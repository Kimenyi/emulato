      /*
        * To change this template, choose Tools | Templates
        * and open the template in the editor.
        */

        package ebm.sdc.rra2;

        import java.awt.Font;
        import java.awt.FontMetrics;
        import java.awt.Graphics;
        import java.awt.PrintJob;
        import java.math.BigDecimal;
        import java.util.Date;
        import java.util.LinkedList; 
        import java.util.Properties; 
        import javax.swing.JFrame; 
        
        /**
        *
        * @author Kimenyi
        */
        public class RRA_PRINT3 extends JFrame {


      public  String    Trade_Name;
     public   String Address, City;
     public   String TIN;//: 000000000
     public   String separateur ="---------------------------------------------------------------------"; 
     public   String welcome ="Welcome to our shop";
     public   String Client_ID ;//: 000000000

     public   double TOTAL ;// 6340.00
      public  double TOTAL_A_EX  ;//1000.00
      public  double TOTAL_B_18 ;//.00% 5340.00
     public   double TOTAL_TAX_B ;// 814.58
     public   double TOTAL_TAX ;// 814.58 
    public    double arr=0.005;
    public    double CASH ;// 6340.00
   public     int ITEMS_NUMBER ;// 3

   public     String SDC_INFORMATION="SDC INFORMATION " ;
   public     String date_time_sdc;//Date: 25/5/2012 Time: 11:07:35
   public     String SDC_ID;//: SDC001000001
    public    String RECEIPT_NUMBER;//: 168/258 NS
   public     String Internal_Data;//:TE68-SLA2-34J5-EAV3-N569-88LJ-Q7
   public     String Receipt_Signature;//:V249-J39C-FJ48-HE2W
  public      String typeSales;
  public      String transaction;
  public      int RECEIPT_NUMBER2 ;//: 152
  public      String date_time_mrc;//DATE: 25/5/2012 TIME: 11:09:32
   public     String MRC;//: AAACC123456
  public  String VERSION;
 public       String tk="THANK YOU";
 public       String comeback="COME BACK AGAIN";
 public       String best="YOUR BEST STORE IN TOWN";
 public       LinkedList <RRA_PRODUCT> getProductListDB; 
 public       LinkedList<String> infoClient;
 public       int margin=15,boldEpson=10,merciBold = 15,title = 13;
 public       int size = 50;
 public        int west = 275;
 
 private Properties properties = new Properties();
 public boolean byempurimwe=false;
 int REFUND_REF=0;
 
public String NUM_AFF="";
public String NOM_CLIENT="";
public String PRENOM_CLIENT="";
public String ASSURANCE="";
public int PERCENTAGE=0;
public String EMPLOYEUR="";
 
public RRA_PRINT3(String Trade_Name, String Address, String City, String TIN, String Client_ID,
double TOTAL, double TOTAL_A_EX, double TOTAL_B_18, double TOTAL_TAX_B, double TOTAL_TAX,
double CASH, String SDC_ID,
String RECEIPT_NUMBER, String Internal_Data, String Receipt_Signature,int RECEIPT_NUMBER2, 
 String MRC,LinkedList <RRA_PRODUCT> getProductListDB,String typeSales,
String transaction,String VERSION,int NEG,int REFUND_REF,LinkedList<String> infoClient,
  String date_time_mrc, String date_time_sdc) 
{
        this.Trade_Name = Trade_Name;
        this.Address = Address;
        this.City = City;
        this.TIN = "TIN: "+TIN;
        this.Client_ID = Client_ID;
        this.TOTAL = TOTAL;
        this.TOTAL_A_EX = TOTAL_A_EX;
        this.TOTAL_B_18 = TOTAL_B_18;
        this.TOTAL_TAX_B = TOTAL_TAX_B;
        this.TOTAL_TAX = TOTAL_TAX;
        this.CASH = CASH; 
        this.date_time_sdc = date_time_sdc;
        this.SDC_ID = SDC_ID;
        this.RECEIPT_NUMBER = RECEIPT_NUMBER;
        this.Internal_Data = Internal_Data;
        this.Receipt_Signature = Receipt_Signature;
        this.RECEIPT_NUMBER2 = RECEIPT_NUMBER2;
        this.date_time_mrc = date_time_mrc;
        this.MRC = MRC;
        this.getProductListDB=getProductListDB;
        this.typeSales=typeSales;
        this.transaction=transaction; 
        this.REFUND_REF=REFUND_REF;
        this.VERSION=VERSION;
        if(infoClient!=null)
        {    for(int i=0;i<infoClient.size();i++)
            {
                this.NUM_AFF=infoClient.get(i);
                this.NOM_CLIENT=infoClient.get(i+1);
                this.PRENOM_CLIENT=infoClient.get(i+2);
                this.ASSURANCE=infoClient.get(i+3);
                this.PERCENTAGE= Integer.parseInt(infoClient.get(i+4)); 
                this.EMPLOYEUR=infoClient.get(i+5);
                break;
            }
        }
        this.infoClient=infoClient;
        this.byempurimwe=PrintRRA(NEG);
}
public RRA_PRINT3(String Trade_Name, String Address, String City, String TIN, String Client_ID,
double TOTAL, double TOTAL_A_EX, double TOTAL_B_18, double TOTAL_TAX_B, double TOTAL_TAX,
double CASH,    String SDC_ID,
String RECEIPT_NUMBER, String Internal_Data, String Receipt_Signature,int RECEIPT_NUMBER2, 
 String MRC, String typeSales,String transaction,String VERSION
,String date_time_mrc, String date_time_sdc) 
{
        this.Trade_Name = Trade_Name;
        this.Address = Address;
        this.City = City;
        this.TIN = "TIN: "+TIN;
        this.Client_ID = Client_ID;
        this.TOTAL = TOTAL;
        this.TOTAL_A_EX = TOTAL_A_EX;
        this.TOTAL_B_18 = TOTAL_B_18;
        this.TOTAL_TAX_B = TOTAL_TAX_B;
        this.TOTAL_TAX = TOTAL_TAX;
        this.CASH = CASH; 
        this.date_time_sdc = date_time_sdc;
        this.SDC_ID = SDC_ID;
        this.RECEIPT_NUMBER = RECEIPT_NUMBER;
        this.Internal_Data = Internal_Data;
        this.Receipt_Signature = Receipt_Signature;
        this.RECEIPT_NUMBER2 = RECEIPT_NUMBER2;
        this.date_time_mrc = date_time_mrc;
        this.MRC = MRC; 
        this.typeSales=typeSales;
        this.transaction=transaction; 
        this.VERSION=VERSION ;
}

public RRA_PRINT3(
        String Trade_Name, String Address, String City, String TIN,String MRC
        ,double tvaRateA, double tvaRateB, double tvaRateC, double tvaRateD,
        String Rtype, String user,String SDC_ID 
        )
{
    PrintJob pjob = getToolkit().getPrintJob(this, "Ishyiga ", properties);
if (pjob != null) {
Graphics pg = pjob.getGraphics(); 
if (pg != null) 
{
    int W=pjob.getPageDimension().width/2;   
    int curHeight = margin+10;
   
Font f = new Font("Arial", Font.BOLD, title );
curHeight= printS  (  pg, Trade_Name, margin,  curHeight, f,"CENTER" );
f = new Font("Arial", Font.PLAIN, boldEpson );
curHeight=printS  (  pg, Address, margin,  curHeight, f ,"CENTER");
curHeight=printS  (  pg, City, margin,  curHeight, f ,"CENTER");
curHeight=printS  (  pg, TIN, margin,  curHeight, f ,"CENTER");
curHeight=printS  (  pg, separateur, margin,  curHeight, f ,"CENTER");
f = new Font("Arial", Font.BOLD, boldEpson ); 
curHeight=printS  (  pg, "TAXE RATE A :", margin,  curHeight, f ,"LEFT");  
printInt(pg, ""+setVirgule(tvaRateA,1,0.005), west, curHeight,f);  
curHeight=printS  (  pg, "TAXE RATE B :", margin,  curHeight, f ,"LEFT"); 
printInt(pg, ""+setVirgule(tvaRateB,1,0.005), west, curHeight,f);  
curHeight=printS  (  pg, "TAXE RATE C :", margin,  curHeight, f ,"LEFT"); 
printInt(pg, ""+setVirgule(tvaRateC,1,0.005), west, curHeight,f);  
curHeight=printS  (  pg, "TAXE RATE D :", margin,  curHeight, f ,"LEFT"); 
printInt(pg, ""+setVirgule(tvaRateD,1,0.005), west, curHeight,f);     
f = new Font("Arial", Font.PLAIN, boldEpson );
curHeight=printS  (  pg, "ISHYIGA POS", margin,  curHeight, f ,"CENTER"); 
curHeight=printS (  pg, " "+VERSION, margin,  curHeight, f ,"LEFT"); 
curHeight=printS  (  pg, "WWW.ISHYIGA.NET", margin,  curHeight, f ,"CENTER");  
f = new Font("Arial", Font.BOLD, boldEpson );
curHeight=printS  (  pg, separateur, margin,  curHeight, f ,"CENTER");
curHeight=printS  (  pg, SDC_INFORMATION, margin,  curHeight, f ,"CENTER");
curHeight=printS  (  pg, "TIME: "+new Date().toLocaleString(), margin,  curHeight, f ,"LEFT");
curHeight=printS  (  pg, "SDC ID: "+SDC_ID, margin,  curHeight, f ,"LEFT");
curHeight=printS  (  pg, "PRINTED BY : "+user, margin,  curHeight, f ,"LEFT");
 
pg.drawRect(margin-5,margin-5, W-25,curHeight);
pg.dispose();
}
pjob.end();
}         
}

public boolean PrintRRA(int NEG) {
    PrintJob pjob = getToolkit().getPrintJob(this, "Ishyiga ", properties);
if (pjob != null) {
Graphics pg = pjob.getGraphics(); 
if (pg != null) 
{
    int W=pjob.getPageDimension().width/2;    
    int H=pjob.getPageDimension().height; 
    int curHeight = margin+10;
   
Font f = new Font("Arial", Font.BOLD, title );
curHeight= printS  (  pg, Trade_Name, margin,  curHeight, f,"CENTER" );
f = new Font("Arial", Font.PLAIN, boldEpson );
curHeight=printS  (  pg, Address, margin,  curHeight, f ,"CENTER");
curHeight=printS  (  pg, City, margin,  curHeight, f ,"CENTER");
curHeight=printS  (  pg, TIN, margin,  curHeight, f ,"CENTER");
curHeight=printS  (  pg, separateur, margin,  curHeight, f ,"CENTER"); 

if(transaction.contains("R"))
{
    curHeight=printS  (  pg, "REFUND", margin,  curHeight, f ,"CENTER");
    curHeight=printS  (  pg,"REF.NORMAL RECEIPT#:" +REFUND_REF, margin,  curHeight, f ,"CENTER"); 
    curHeight=printS  (  pg, separateur, margin,  curHeight, f ,"CENTER"); 
    curHeight=printS  (  pg,"REFUND IS APPROVED ONLY FOR", margin,  curHeight, f ,"CENTER"); 
    curHeight=printS  (  pg,"ORIGINAL SALES RECEIPT", margin,  curHeight, f ,"CENTER"); 
    curHeight=printS  (  pg, "CLIENT ID: "+Client_ID, margin,  curHeight, f ,"CENTER");
    curHeight=printS  (  pg, separateur, margin,  curHeight, f ,"CENTER"); 
}
else if(typeSales.contains("P"))
{
    curHeight=printS  (  pg, welcome, margin,  curHeight, f ,"CENTER");      
    curHeight=printS  (  pg, "Name & Last Name:"+NOM_CLIENT+" ", margin,  curHeight, f ,"CENTER");
    curHeight=printS  (  pg, "Reference:"+ASSURANCE+":"+PRENOM_CLIENT, margin,  curHeight, f ,"CENTER");        
    curHeight=printS  (  pg, "CLIENT ID:"+Client_ID, margin,  curHeight, f ,"CENTER");
    curHeight=printS  (  pg, separateur, margin,  curHeight, f ,"CENTER");   
    
}
else
{
    if(infoClient!=null && !ASSURANCE.equals("CASHNORM") && NOM_CLIENT.length()>2)
    {
        curHeight=printS  (  pg, welcome, margin,  curHeight, f ,"CENTER");        
        curHeight=printS  (  pg, "Affiliate:"+NUM_AFF, margin,  curHeight, f ,"LEFT");
        curHeight=printS  (  pg, "Name & Last Name:"+NOM_CLIENT+" "+PRENOM_CLIENT, margin,  curHeight, f ,"LEFT");
        curHeight=printS  (  pg, "Departement:"+EMPLOYEUR, margin,  curHeight, f ,"LEFT");        
        curHeight=printS  (  pg, "CLIENT ID: "+Client_ID, margin,  curHeight, f ,"LEFT");
        curHeight=printS  (  pg, separateur, margin,  curHeight, f ,"CENTER");         
    }
    else
    {
        curHeight=printS  (  pg, welcome, margin,  curHeight, f ,"CENTER");
        curHeight=printS  (  pg, "CLIENT ID: "+Client_ID, margin,  curHeight, f ,"CENTER");
        curHeight=printS  (  pg, separateur, margin,  curHeight, f ,"CENTER"); 
    }
} 
//////////////////////////////////////////////////////////////////////////////////////////////////////
f = new Font("Arial", Font.PLAIN, 8 );
 FontMetrics fm = pg.getFontMetrics(f); 
     int fontHeight =  fm.getHeight();
for (int i = 0; i <  getProductListDB .size(); i++) 
{
    
RRA_PRODUCT p = (RRA_PRODUCT)  getProductListDB .get(i);

String name=p.productName;
if(name!=null && name.contains("#"))
{ 
    
    String [] sp=name.split("#");    
    
    String newProd=sp[0].substring(0, Math.min(sp[0].length(),size));
    int l=size-newProd.length(); 
    name=newProd+""+getSpaces(l)+setVirgule(p.currentPrice,NEG,0)+"X ";
    curHeight=printS  (  pg, name, margin,  curHeight, f ,"LEFT");
    curHeight -= fontHeight;
    f = new Font("Arial", Font.BOLD, 8 );
    printInt(pg, ""+p.qty, west-110, curHeight,f);
    f = new Font("Arial", Font.PLAIN, 8 );
    printInt(pg, ""+setVirgule((p.originalPrice()),NEG,0)+""+p.classe, west, curHeight,f); 
    curHeight += fontHeight;
    printInt(pg, ""+setVirgule((p.salePrice()+arr),NEG,0)+""+p.classe, west, curHeight,f);    
    curHeight=printS  (  pg, sp[1], margin,  curHeight, f ,"LEFT");
    
}
else if(name!=null)
{    
        String newProd=name.substring(0, Math.min(name.length(),size));
        int l=size-newProd.length();
        name=newProd+""+getSpaces(l)+setVirgule(p.currentPrice,NEG,0)+"X ";
        curHeight=printS  (  pg, name , margin,  curHeight, f ,"LEFT");
      curHeight -= fontHeight;
      f = new Font("Arial", Font.BOLD, 8 );
        printInt(pg, ""+p.qty, west-110, curHeight,f);
        f = new Font("Arial", Font.PLAIN, 8 );
        printInt(pg, ""+setVirgule((p.salePrice()+arr),NEG,0)+""+p.classe, west, curHeight,f); 
      curHeight += fontHeight;
}
else
    System.out.println(name+i);
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////
f = new Font("Arial", Font.PLAIN, boldEpson );

if(typeSales.contains("P")||typeSales.contains("C")||typeSales.contains("T"))
{ 
    curHeight=printS  (  pg, separateur, margin,  curHeight, f ,"CENTER");
    f = new Font("Arial", Font.BOLD, title );
    curHeight=printS  (  pg, "THIS IS NOT AN OFFICIAL RECEIPT ", margin,  curHeight, f ,"CENTER");
}
f = new Font("Arial", Font.PLAIN, boldEpson );
curHeight=printS  (  pg, separateur, margin,  curHeight, f ,"CENTER");
f = new Font("Arial", Font.BOLD, boldEpson ); 
printInt(pg, ""+setVirgule(TOTAL,NEG,0.005), west , curHeight,f);
curHeight=printS (  pg, "TOTAL ", margin,  curHeight, f ,"LEFT");


f = new Font("Arial", Font.PLAIN, boldEpson );
printInt(pg, ""+setVirgule(TOTAL_A_EX,NEG,0.005), west , curHeight,f);
curHeight=printS  (  pg, "TOTAL A-EX " , margin,  curHeight, f ,"LEFT");
printInt(pg, ""+setVirgule(TOTAL_B_18,NEG,0.005), west , curHeight,f);
curHeight=printS  (  pg, "TOTAL B-18.00% " , margin,  curHeight, f ,"LEFT");
printInt(pg, ""+setVirgule(TOTAL_TAX_B,NEG,0.005), west , curHeight,f);
curHeight=printS  (  pg, "TOTAL TAX B " , margin,  curHeight, f ,"LEFT");
printInt(pg, ""+setVirgule(TOTAL_TAX,NEG,0.005), west , curHeight,f);
curHeight=printS  (  pg, "TOTAL TAX " , margin,  curHeight, f ,"LEFT");
  
curHeight=printS  (  pg, separateur, margin,  curHeight, f ,"CENTER"); 
if(typeSales.contains("P"))
{ 
    curHeight=printS  (  pg, "PROFORMA", margin,  curHeight, f ,"CENTER"); 
}
else if(typeSales.contains("C"))
{
    f = new Font("Arial", Font.BOLD, boldEpson );
    curHeight=printS  (  pg, "COPY", margin,  curHeight, f ,"CENTER"); 
    curHeight=printS  (  pg, separateur, margin,  curHeight, f ,"CENTER"); 
}
else if(typeSales.contains("T"))
{
    f = new Font("Arial", Font.BOLD, boldEpson );
    curHeight=printS  (  pg, "TRAINING MODE", margin,  curHeight, f ,"CENTER");
    curHeight=printS  (  pg, separateur, margin,  curHeight, f ,"CENTER"); 
}

if(typeSales.contains("N")||typeSales.contains("C")||typeSales.contains("T"))
{
    f = new Font("Arial", Font.BOLD, boldEpson );
    if(infoClient!=null && !ASSURANCE.equals("CASHNORM") && NOM_CLIENT.length()>2)
    {
        double p = (((CASH * PERCENTAGE) * 0.01));
        curHeight=printS  (  pg, "CASH  "+PERCENTAGE+"% : "+setVirgule(p,NEG,0.005), margin,  curHeight, f ,"CENTER"); 
        curHeight=printS  (  pg, ASSURANCE+" "+(100-PERCENTAGE)+"% : "+setVirgule(CASH-p,NEG,0.005), margin,  curHeight, f ,"CENTER"); 
        curHeight=printS  (  pg, "ITEMS NUMBER: "+getProductListDB .size(), margin,  curHeight, f ,"CENTER");
    }
    else
    {
        curHeight=printS  (  pg, "CASH: "+setVirgule(CASH,NEG,0.005), margin,  curHeight, f ,"CENTER"); 
        curHeight=printS  (  pg, "ITEMS NUMBER: "+getProductListDB .size(), margin,  curHeight, f ,"CENTER");
        
    }
}
f = new Font("Arial", Font.PLAIN, boldEpson );
curHeight=printS  (  pg, separateur, margin,  curHeight, f ,"CENTER");
curHeight=printS  (  pg, SDC_INFORMATION, margin,  curHeight, f ,"CENTER");
curHeight=printS  (  pg, "TIME SDC: "+date_time_sdc, margin,  curHeight, f ,"LEFT");
curHeight=printS  (  pg, "SDC ID: "+SDC_ID, margin,  curHeight, f ,"LEFT");
curHeight=printS  (  pg, "RECEIPT NUMBER: "+RECEIPT_NUMBER, margin,  curHeight, f ,"LEFT");

if(typeSales.contains("N")||typeSales.contains("C"))
{ 
   curHeight=printS  (  pg,"Internal Data:" , margin,  curHeight, f ,"CENTER"); 
   curHeight=printS  (  pg,Internal_Data , margin,  curHeight, f ,"CENTER"); 
   curHeight=printS  (  pg,"Receipt Signature:" , margin,  curHeight, f ,"CENTER"); 
   curHeight=printS  (  pg,set4Virgule(Receipt_Signature) , margin,  curHeight, f ,"CENTER"); 
}

curHeight=printS  (  pg, separateur, margin,  curHeight, f ,"CENTER");
curHeight=printS  (  pg,"INVOICE NUMBER: " +RECEIPT_NUMBER2, margin,  curHeight, f ,"LEFT"); 
curHeight=printS  (  pg,"TIME MRC "+date_time_mrc , margin,  curHeight, f ,"LEFT"); 
curHeight=printS  (  pg,"MRC: "+MRC , margin,  curHeight, f ,"LEFT"); 
       
curHeight=printS  (  pg, separateur, margin,  curHeight, f ,"CENTER"); 

curHeight=printS  (  pg,tk , margin,  curHeight, f ,"CENTER");
curHeight=printS  (  pg,comeback , margin,  curHeight, f ,"CENTER");
curHeight=printS  (  pg,best , margin,  curHeight, f ,"CENTER");
 
pg.drawRect(margin-5,margin-5, W-25,curHeight);
pg.dispose();
}
pjob.end();
return true;
}         
else
    return false;
}
public static String getSpaces(int l)
{String s="";
    for(int i=0;i<l;i++)
    {
        s+=" ";
    }
    return s;
}
public static void printBackward(Graphics g,Font f,String s,int w,int h)
{
int back=0;
for(int i=s.length()-1;i>=0;i--)
  {
char c=s.charAt(i);
FontMetrics fm = g.getFontMetrics(f);
back+=fm.charWidth(c); 
g.drawString(""+c,w-back,h);
}
}
public static String printBackwardBlanc(FontMetrics fm,String s,int withKuzuza)
{
String res="";
int back=0;
char space=' ';
for(int i=0; i<s.length();i++)
{
char c=s.charAt(i); 
back+=fm.charWidth(c);  
}
while(back<withKuzuza)
{ 
res=res+space;
back+=fm.charWidth(space);  
}
return res;
}
int printS  (Graphics pg, String s, int w,int curHeight, Font f,String location )
{
     pg.setFont(f);
     FontMetrics fm = pg.getFontMetrics(f);
        int fontHeight =  fm.getHeight();
        
if(location.equals("CENTER")){ 
      int leCentre=  (west/2); 
       int size=s.length();
        if(size>2)
        {int hagati=(size)/2; 
    printBackward(pg,f,s.substring(0,hagati), leCentre, curHeight);
    pg.drawString(s.substring(hagati, s.length()), leCentre,curHeight);}
       else
        pg.drawString(s, margin, curHeight);
        curHeight += fontHeight; 
    }
        else
        {
            if (s.length() >size) {
        //nextLineE = nextLineE.substring(0, 34) ;
        pg.drawString(s.substring(0, (size - 1)), w, curHeight);
        curHeight += fontHeight;
        s =  s.substring((size - 1), s.length()); 
        }
        pg.drawString(s, margin, curHeight);
        curHeight += fontHeight;
        }
        return curHeight;
}
public void printInt(Graphics pg, String s, int w, int h,Font f) 
{
    pg.setFont(f);
    int back = 0;
    
    for (int i = s.length() - 1; i >= 0; i--) 
    {
        if (s.charAt(i) == ',' || s.charAt(i) == '.')  
        {
            back += 2;
        }
        else
        {
            back += 5;
        }
        pg.drawString("" + s.charAt(i), w - back, h);
    }
}
public static String setSansVirgule(double frw)
    {  
        return setVirgule(frw,1, 0.005).replaceAll(",","");
        
    }

 public static String setVirgule(double frw,int NEG,double arr)
    {  
        
//        System.out.println("frw:"+frw);
        if(arr==0.005)
            frw=frw+0.0055999999999;
//        System.out.println("frw arre:"+frw);
        BigDecimal big=new BigDecimal(frw);
//        big = big.setScale(2, BigDecimal.ROUND_UP);
        String getBigString=big.toString();
//        System.out.println("big:"+getBigString);
  getBigString=getBigString.replace('.', ';');
//  System.out.println("after;"+getBigString);
         String[]sp=getBigString.split(";");  
          
          String decimal="";
          if(sp.length>1)
          {
              decimal=sp[1];
          }
            
            if(decimal.equals("") || ( decimal.length()==1 && decimal.equals("0")) )
            {
                decimal=".00";
            }
            else if (decimal.length()==1 && !decimal.equals("0") )
            {
                decimal="."+decimal+"0";
            }
            else
            {
                decimal="."+decimal.substring(0, 2);
            }
             
            
            String toret=setVirgule(sp[0])+decimal;
            
            if(NEG==-1 && frw>0)
            {
                toret="-"+toret;
            }
            
            return toret;
}

public static String setVirgule(String frw)
   {
       String setString="";
       int k=0;
       for(int i=(frw.length()-1);i>=0;i--)
       {
           k++;
           if((k-1)%3==0 && (k-1)!=0)
           {
               setString+=",";
           }
           setString+=frw.charAt(i);
       }
       return inverse(setString);
   }
   
   static String inverse(String frw)
   {
       String l="";
       for(int i=(frw.length()-1);i>=0;i--)
       {
           l+=frw.charAt(i);
       }
       return l;
   }
   static String set4Virgule(String frw)
   {
       String setString="";
       int k=0;
       for(int i=(frw.length()-1);i>=0;i--)
       {
           k++;
           if((k-1)%4==0 && (k-1)!=0)
           {
               setString+="-";
           }
           setString+=frw.charAt(i);
       }
       return inverse(setString);
   }

        }
