/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ebm.sdc.rra2;
 
import java.awt.Color;
import java.awt.Font;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Date;
import java.util.LinkedList;

/**
 *
 * @author Kimenyi
 */
public class RRA_SDC_SOFT { 
static Font police; 
static String	      messageString = " "; 
 
static public String  MRC, TIN,  SDC,COMPANY,CITY,ADRESS,VERSION;
RRA_SENT_PACKAGE cis=null;
public LinkedList <Byte> getResponse ;
static public Integer received=0;
public Connection conn=null;

static public LinkedList<RRA_COMMAND> queu=new LinkedList<RRA_COMMAND>();
static public  boolean sdcIsConnected=false;
static public int currentQueuId=0;  
static public int timeToSleep=1000;
static public int timeToWait=0;
static public int  busyTime=200; 
RRA_CLIENT rraClient;

public RRA_SDC_SOFT(String TIN, String MRC,String COMPANY,String CITY,String ADRESS,String VERSION,
      Connection conn,  String employe,String serverName)
{
RRA_SDC_SOFT.TIN = TIN;
RRA_SDC_SOFT.MRC = MRC; 
RRA_SDC_SOFT.COMPANY = COMPANY;
RRA_SDC_SOFT.ADRESS = ADRESS;
RRA_SDC_SOFT.CITY = CITY;
RRA_SDC_SOFT.VERSION=VERSION;
rraClient= new RRA_CLIENT(  serverName, employe );
this.cis=new RRA_SENT_PACKAGE( MRC,TIN); 
this.conn=conn; 
System.out.println("SDC CONNECTION:"+sdcIsConnected);
if(sdcIsConnected)
{ 
    RRA_RECEIVED_PACKAGE sd=sendToSDC3("ID REQUEST","",employe,RraSoft.softIP );
    if(sd!=null && sd.data.length()>3)
    {  SDC= sd.data;  }
    else
    { sdcIsConnected=false;  }
}   
}  
public RRA_RECEIVED_PACKAGE sendToSDC3(String choix,String toSend,String employe,String ip)
{ 
    toSend =toSend.replaceAll("#", " ");
System.out.println(choix+" sending    "+toSend); 
String data =  RRA_CLIENT.sendRequestIport2(choix, toSend,RraSoft.PORT_SDC_SOFT,ip);
System.out.println("incoming    "+data); 
RRA_RECEIVED_PACKAGE rec= new RRA_RECEIVED_PACKAGE("P",data);

if(  rec.data==null)
{  RraSoft.textArea.setForeground(Color.red);}
else
{   RraSoft.textArea.setForeground(Color.black); }  
return rec; 
} 

static public RRA_RECEIVED_PACKAGE sendToSDCSOFT
        (String choix,String toSend,
        String employe,int port,String ip)
{ 
toSend =toSend.replaceAll("#", " ");
System.out.println(choix+" sending    "+toSend); 
String data =  RRA_CLIENT.sendRequestIport2(choix, toSend,  RraSoft.PORT_SDC_SOFT,  ip);
System.out.println("incoming    "+data); 
RRA_RECEIVED_PACKAGE rec= new RRA_RECEIVED_PACKAGE("P",data);

if(  rec.data==null)
{  RraSoft.textArea.setForeground(Color.red);}
else
{   RraSoft.textArea.setForeground(Color.black); }  
return rec; 
}

  
boolean insertTrackRRA( RRA_TRACK rt, Connection conn)
 {  
     try
     {  
        PreparedStatement psInsert = conn.prepareStatement("insert into APP.RRA_TRACKING_COMM"
                 + " (SENT_TRACKING,RECEIVED_TRACKING ,KEY_INVOICE,EMPLOYE)  values (?,?,?,?)");
         
         psInsert.setString(1,rt.SENT_TRACKING);
         psInsert.setString(2,rt.RECEIVED_TRACKING);
         psInsert.setString(3,rt.KEY_INVOICE);
         psInsert.setString(4,rt.EMPLOYE);
         psInsert.executeUpdate();
         psInsert.close();
         return true;
    }  
 catch (Throwable e)  
 {
     writeError(e+" KEY "+rt.KEY_INVOICE,"insertTrackRRA FAIL");
     //insertError(e+"","insertTrackRRA");
 } 
 return false;
} 
void writeError(String desi,String ERR)
    {
try {
String  file="ERROR/ rra.txt";
File f = new File(file); 
PrintWriter sorti=new PrintWriter(new FileWriter(f));
sorti.println(desi+ERR);
sorti.close();  
 RraSoft.textArea.append(" writeError "+desi); 
} catch (Throwable ex) {
RraSoft.textArea.append(" writeError "+desi+"  CONTACT ISHYIGA TEAM ");     
//FUNGA FATAL ERROR
}
}
  
}
