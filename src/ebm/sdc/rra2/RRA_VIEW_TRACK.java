/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ebm.sdc.rra2;

import java.awt.Container;
import java.awt.Dimension;
import java.util.LinkedList;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 *
 * @author Kimenyi
 */
public class RRA_VIEW_TRACK extends JFrame { 
    
public RRA_VIEW_TRACK(RRA_DB db) {
       
  
String  Debut = JOptionPane.showInputDialog("DEBUT ","2014-01-01 00:00:00");
String FIN = JOptionPane.showInputDialog("FIN ","2014-12-31 23:59:00");  

String choix = (String) JOptionPane.showInputDialog(null, "options", "", JOptionPane.QUESTION_MESSAGE, null,
new String[]{ "ALL","SEND RECEIPT", "ID REQUEST", "RESPONSE", "COUNTERS REQUEST", "SIGNATURE REQUEST", "DATE AND TIME REQUEST", "PS" }, "ALL");

String where=" where   HEURE_TRACKING<'"+FIN+"' AND HEURE_TRACKING>'"+Debut+"' "
        + " AND KEY_INVOICE='"+choix+"' order by  HEURE_TRACKING " ;    
 if(choix.contains("ALL"))
{ where="WHERE HEURE_TRACKING<'"+FIN+"' AND HEURE_TRACKING>'"+Debut+"' order by  HEURE_TRACKING "; }    
  
String sql="select  * from   APP.RRA_TRACKING_COMM   "+where;

 LinkedList <RRA_TRACK > allRecs=db.getTRACK(sql) ;// INVOICE
   Container content = getContentPane();
westPane(content,makeTRACKJtable(allRecs)); 
this.setSize(1200,750); 
this.setVisible(true);
}
 
//le panel de ventes
public void westPane(  Container content,JTable j )
{
JPanel  productPane  = new JPanel();
productPane.setPreferredSize(new Dimension (1150,700)); 
JScrollPane jTableList =new JScrollPane(j);
jTableList.setPreferredSize(new Dimension(1150,680)); 
productPane.add(jTableList); 
content.add(productPane);
}   
public JTable makeTRACKJtable(LinkedList <RRA_TRACK> reclist) {
     
JTable jTable = new javax.swing.JTable();
int linge=reclist.size();
String [][]s1=new String [linge][19];
for(int i=0;i<linge;i++)
{  
RRA_TRACK C=reclist.get(i);
int j=0;
s1[i][j]=""+C.ID_TRACKING ;  j++;
s1[i][j]=""+C.HEURE_TRACKING ;  j++;
s1[i][j]=C.KEY_INVOICE;  j++;
s1[i][j]=C.EMPLOYE;  j++;
s1[i][j]=stringToByte(C.SENT_TRACKING );  j++;
s1[i][j]=stringToByte(C.RECEIVED_TRACKING);  j++; 
}  
//setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE); 
jTable.setModel(new javax.swing.table.DefaultTableModel(s1,
new String [] {
    "ID", "HEURE",  "CMD", "EMPLOYE",
 "SENT ", "RECEIVED"  } ));
jTable.setAutoResizeMode(jTable.AUTO_RESIZE_ALL_COLUMNS); 
jTable.getColumnModel().getColumn(4).setPreferredWidth(300);//.setWidth(200);
jTable.getColumnModel().getColumn(5).setPreferredWidth(300);//.setWidth(200);
jTable.doLayout();
jTable.validate(); 
return jTable;
}
String stringToByte(String toTransform)
{ 
  String res="" ; 
 String [] splited=toTransform.split(";");  
 
 byte [] byteList=new byte [splited.length];
 
 for(int i=0;i<splited.length;i++)   
{
    try
    {
        byteList[i]=(byte)Integer.parseInt(splited[i]);   
    }
    catch (NumberFormatException e)
    {
    System.err.println(splited[i]+"; "+e);
    
    }
}
 
for(int i=0;i<byteList.length;i++)   
{
  res+= getChar(byteList[i])  ;  
}  

return res;

}
String getChar(byte b)
{
char c='0';
if(b==16)
    return "B";
if(b==46)
    return ".";
if(b==47)
    return "/";
if(b==32)
    return " ";
if(b==44)
    return ",";

 for(int i=0;i<70;i++)
    {
    if((""+c).getBytes()[0]==b)
      return ""+c;
    c++;
    } 
return ""+b;
}
}

