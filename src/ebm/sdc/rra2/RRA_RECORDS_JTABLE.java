package ebm.sdc.rra2;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
/**
*
* @author aimable
*/
public class RRA_RECORDS_JTABLE extends JFrame 
{ 
JMenuBar menu_bar1 = new JMenuBar(); 
JMenu menu1 = new JMenu("OPTION");
LinkedList <RRA_RECORD> recList; 
JMenuItem print1 = new JMenuItem("PRINT SELECTED");
JMenuItem print2 = new JMenuItem("PRINT ALL"); 
JTable recJTable;  
RRA_DB db; 

RRA_RECORDS_JTABLE(RRA_DB db,LinkedList <RRA_RECORD> recList,String title)
{
this.recList=recList; 
this.db=db;
setTitle(title);
this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);    
menu1.add(print1);menu1.add(print2); 

/* Ajouter les menu sur la bar de menu */
menu_bar1.add(menu1);
this.setJMenuBar(menu_bar1);

//on fixe  les composant del'interface
Container content = getContentPane();
print1();print2(); 

recJTable=makeRecJtable(recList);
westPane(content,recJTable); 
this.setSize(1200,750); 
this.setVisible(true);
}
 
//le panel de ventes
public void westPane(  Container content,JTable j )
{
JPanel  productPane  = new JPanel();
productPane.setPreferredSize(new Dimension (1150,700)); 
JScrollPane jTableList =new JScrollPane(j);
jTableList.setPreferredSize(new Dimension(1150,680)); 
productPane.add(jTableList); 
content.add(productPane);
}

void print1(){
print1.addActionListener(new ActionListener()
{
public void actionPerformed(ActionEvent e)
{
 if(e.getSource()==print1 && recJTable.getSelectedRow()!=-1 )
{
 RRA_RECORD rec =  recList.get(recJTable.getSelectedRow());
 if(rec!=null && rec.RTYPE.contains("R"))
         db.photoCopyR(rec );
         else
         db.photoCopyS( rec);

}
}
});
}
void print2(){
print2.addActionListener(new ActionListener()
{
public void actionPerformed(ActionEvent e)
{
 if(e.getSource()==print2 && recJTable.getSelectedRow()!=-1 )
{
    RRA_RECORD rec =  recList.get(recJTable.getSelectedRow());
  
}
}
});
}
 
public static JTable makeRecJtable(LinkedList <RRA_RECORD> reclist) {
     
JTable jTable = new javax.swing.JTable();
int linge=reclist.size();
String [][]s1=new String [linge][19];
for(int i=0;i<linge;i++)
{  
RRA_RECORD C=reclist.get(i);
int j=0;
s1[i][j]=""+C.ID ;  j++;
s1[i][j]=""+C.RTYPE ;  j++;
s1[i][j]=C.NUM_CLIENT;  j++;
s1[i][j]=C.EMPLOYE;  j++;
s1[i][j]=""+C.HEURE ;  j++;
s1[i][j]=""+C.TOTAL;  j++;
s1[i][j]=""+C.TVA;   j++;
s1[i][j]=""+C.TOTAL_A;  j++;
s1[i][j]=""+C.TAXE_A;  j++;
s1[i][j]=""+C.TOTAL_B;  j++;
s1[i][j]=""+C.TAXE_B;  j++;
s1[i][j]=""+C.TOTAL_C ;  j++;
s1[i][j]=""+C.TAXE_C;  j++;
s1[i][j]=""+C.TOTAL_D ;  j++;
s1[i][j]=""+C.TAXE_D;  j++;
s1[i][j]=C.EXTERNAL_DATA;  j++;
s1[i][j]=C.COPY_DATA;    j++;  
s1[i][j]=""+C.HEURE_COPY ;  
}  
//setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE); 
jTable.setModel(new javax.swing.table.DefaultTableModel(s1,
new String [] {
    "ID", "RTYPE",  "NUM_CLIENT", "EMPLOYE",
 "HEURE", "TOTAL", "TVA", "TOTAL_A ", "TAXE_A", "TOTAL_B", "TAXE_B", "TOTAL_C",
 "TAXE_C",      "TOTAL_D ", "TAXE_D", "EXTERNAL_DATA","COPY_DATA", "HEURE_COPY" } ));
jTable.setAutoResizeMode(jTable.AUTO_RESIZE_ALL_COLUMNS); 
jTable.getColumnModel().getColumn(4).setPreferredWidth(100);//.setWidth(200);
jTable.doLayout();
jTable.validate(); 
return jTable;
}  
}