/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ebm.sdc.rra2;

import java.awt.Font;
import java.util.LinkedList;

/**
 *
 * @author Kimenyi
 */
public class RRA_REPORT_PLU {
    
    RRA_DB db ; 
    String hera,geza,tradename,tin,CIS,MRC ;
    double   tvaRateA,   tvaRateB,   tvaRateC,   tvaRateD;

    public RRA_REPORT_PLU(RRA_DB db, String hera, String geza, String tradename, String tin, String CIS, String MRC, double tvaRateA, double tvaRateB, double tvaRateC, double tvaRateD) {
        this.db = db;
        this.hera = hera;
        this.geza = geza;
        this.tradename = tradename;
        this.tin = tin;
        this.CIS = CIS;
        this.MRC = MRC;
        this.tvaRateA = tvaRateA;
        this.tvaRateB = tvaRateB;
        this.tvaRateC = tvaRateC;
        this.tvaRateD = tvaRateD;
    }
 
LinkedList  <RRA_PRODUCT>  PLU()// amount for all sales receipts labeled as NS, including tax;
{
double [] taxes= new double []{tvaRateA/100.0,   tvaRateB/100.0,   tvaRateC/100.0,   tvaRateD/100.0};    
return db.getPLU(hera, geza,taxes);
}
LinkedList <RRA_PRINT2_LINE> toPrint()
{
  //  daily report hera,geza,tradename,tin,CIS,MRC 
    
LinkedList <RRA_PRINT2_LINE> toPrint = new LinkedList();
Font f = new Font("Arial", Font.BOLD, 12 );
 
toPrint.add(new RRA_PRINT2_LINE(" PLU REPORT ", "", f)); 
toPrint.add(new RRA_PRINT2_LINE(" START : "+hera, "", f)); 
toPrint.add(new RRA_PRINT2_LINE(" END : "+geza, "", f)); 
toPrint.add(new RRA_PRINT2_LINE(" TRADE NAME :"+tradename, "", f)); 
toPrint.add(new RRA_PRINT2_LINE(" TIN  : "+tin, "", f)); 
toPrint.add(new RRA_PRINT2_LINE(" MRC : "+MRC, "", f));  
toPrint.add(new RRA_PRINT2_LINE(" CIS  : "+CIS, "", f)); 
toPrint.add(new RRA_PRINT2_LINE("   ", "", f));
toPrint.add(new RRA_PRINT2_LINE(" -------------------------  ", "", f));
toPrint.add(new RRA_PRINT2_LINE(" TAXE RATE A :"+RRA_PRINT2.setVirgule(tvaRateA,1,0.005), "", f)); 
toPrint.add(new RRA_PRINT2_LINE(" TAXE RATE B :"+RRA_PRINT2.setVirgule(tvaRateB,1,0.005), "", f)); 
toPrint.add(new RRA_PRINT2_LINE(" TAXE RATE C :"+RRA_PRINT2.setVirgule(tvaRateC,1,0.005), "", f)); 
toPrint.add(new RRA_PRINT2_LINE(" TAXE RATE D :"+RRA_PRINT2.setVirgule(tvaRateD,1,0.005), "", f));  
toPrint.add(new RRA_PRINT2_LINE(" -------------------------  ", "", f));
toPrint.add(new RRA_PRINT2_LINE("   ", "", f));   
f = new Font("Arial", Font.PLAIN, 10 );
 

 
return toPrint;
}

}
