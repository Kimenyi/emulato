/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ebm.sdc.rra2;
  
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.Socket; 
import javax.swing.JOptionPane; 

/**
 *
 * @author Kimenyi
 */ 
 

 public class RRA_CLIENT {

   int port_sdc = 7779;   
   String employe="";
   String serverName="";  
  
 public RRA_CLIENT(String serverName,String employe ) {
     this.employe=employe; 
     this.serverName=serverName;   
     
 }

public String getSaleString4(RRA_PACK rp) 
{
    String inv=""+rp.id_invoice;
    String tin;
    if(inv.equals("0"))
        inv="&ID_INVOICE&"; 
    
    if(rp.ClientTin!=null && rp.ClientTin.length()==9)
    {
        tin=","+rp.ClientTin;
    }
    else
        tin="";
    
   return rp.rtype+rp.ttype+rp.MRC+","+rp.TIN+",&TIME&,"+inv+","+
RRA_PRINT2.setSansVirgule(rp.tvaRateA)+","+ RRA_PRINT2.setSansVirgule(rp.tvaRateB)+","+ RRA_PRINT2.setSansVirgule(rp.tvaRateC)+","+
           RRA_PRINT2.setSansVirgule(rp.tvaRateD)+","+RRA_PRINT2.setSansVirgule(rp.totalpriceA)+","+ RRA_PRINT2.setSansVirgule(rp.totalpriceB)+","+ 
           RRA_PRINT2.setSansVirgule(rp.totalpriceC)+","+ RRA_PRINT2.setSansVirgule(rp.totalpriceD)+","+
RRA_PRINT2.setSansVirgule(rp.tvaPriceA)+","+ RRA_PRINT2.setSansVirgule(rp.tvaPriceB)+","+ RRA_PRINT2.setSansVirgule(rp.tvaPriceC)+","+RRA_PRINT2.setSansVirgule(rp.tvaPriceD)+""+tin;
} 
public String getSaleString2(RRA_PACK rp) 
{ 
    String tin; 
   
    if(rp.ClientTin!=null && rp.ClientTin.length()==9)
    {
        tin=","+rp.ClientTin;
    }
    else
        tin="";
//SEND RECEIPT#NRALG01000001,101587911,,24,0.00,18.00,0.00,0.00,1.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00#vtc#Tue Jan 28 11:46:57 EET 2014AdminAlgorithmBeta/10.18.97.129@@ @N@R@24
    
    System.out.println("rtype:"+rp.rtype);
    System.out.println("ttype:"+rp.ttype);
    System.out.println("mrc:"+rp.MRC);
    System.out.println("tin:"+rp.TIN);
   return rp.rtype+rp.ttype+rp.MRC+","+rp.TIN+",&TIME&,&ID_INVOICE&,"+
RRA_PRINT2.setSansVirgule(rp.tvaRateA)+","+ RRA_PRINT2.setSansVirgule(rp.tvaRateB)+","+ RRA_PRINT2.setSansVirgule(rp.tvaRateC)+","+
RRA_PRINT2.setSansVirgule(rp.tvaRateD)+","+RRA_PRINT2.setSansVirgule(rp.totalpriceA)+","+ RRA_PRINT2.setSansVirgule(rp.totalpriceB)+","+ 
RRA_PRINT2.setSansVirgule(rp.totalpriceC)+","+ RRA_PRINT2.setSansVirgule(rp.totalpriceD)+","+
RRA_PRINT2.setSansVirgule(rp.tvaPriceA)+","+ RRA_PRINT2.setSansVirgule(rp.tvaPriceB)+","+ RRA_PRINT2.setSansVirgule(rp.tvaPriceC)+","+RRA_PRINT2.setSansVirgule(rp.tvaPriceD)+""+tin;
} 

 static public String sendRequestIport2(String rule,String toSend,int port,String ip) { 
 
     
 String dataTosend=rule+"#"+toSend;
     
 
     try { 
        
     System.out.println( "  Data to Send  "+dataTosend);
      System.out.println(ip+"  serverName "+ip );
       System.out.println("  port_sdc  "+port);
     
     
     String answer ;

         InetAddress serveur = InetAddress.getByName(ip);
         Socket socket = new Socket(serveur, port);
         BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
         PrintStream out = new PrintStream(socket.getOutputStream());
         out.println(dataTosend);
         answer=in.readLine();
         System.out.println("Answer from SDC   "+answer); 
 
         
     if(answer.contains("AN ERROR MSG"))
     {
         String [] msg=answer.split("#");
         JOptionPane.showMessageDialog(null, ""+msg[0], ""+msg[1], JOptionPane.WARNING_MESSAGE);
         return answer;  
     }
     
     if(answer.contains("ERROR"))
     { JOptionPane.showMessageDialog(null, ""+answer, "ATTENTION", JOptionPane.WARNING_MESSAGE);
         return answer;  
     }
 
 return answer;
 } 
 catch (  IOException e) 
 {
      System.out.println("error:"+e); 
 return null;
 } 
 }
 
 public String sendRequestPort(String rule,String toSend,String mesg,String isaha,
         String tin_cl,String Rtype,String Ttype,int id_invoice,int port_sdc ) { 
 
     
 String dataTosend=rule+"#"+toSend+"#"+employe+"#"+mesg+"@"+isaha+"@"
         +tin_cl+"@"+Rtype+"@"+Ttype+"@"+id_invoice;
  
   
     try { 
        
     System.out.println(port_sdc+" Data to Send  "+dataTosend);
     String answer ;

         InetAddress serveur = InetAddress.getByName(serverName);
         Socket socket = new Socket(serveur, port_sdc);
         BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
         PrintStream out = new PrintStream(socket.getOutputStream());
         out.println(dataTosend);
         answer=in.readLine();
         System.out.println("Answer from SDC   "+answer); 
 
         
     if(answer.contains("AN ERROR MSG"))
     {
         String [] msg=answer.split("#");
         JOptionPane.showMessageDialog(null, ""+msg[0], ""+msg[1], JOptionPane.WARNING_MESSAGE);
         return answer;  
     }
     
     if(answer.contains("ERROR"))
     { JOptionPane.showMessageDialog(null, ""+answer, "ATTENTION", JOptionPane.WARNING_MESSAGE);
         return answer;  
     }
 
 return answer;
 } 
 catch (  IOException e) 
 {
      System.out.println("error:"+e); 
 return null;
 }
      
     
 }
 
 }
