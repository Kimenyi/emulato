/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ebm.sdc.rra2;

import java.util.LinkedList;

/**
 *
 * @author Kimenyi
 */
public class RRA_RECEIVED_PACKAGE {
   
//Messages from the SDC to the CIS
//<01><LEN><SEQ><CMD><DATA><04><STATUS><05><BCC><03>
 public String recMsg="";
 public String data="";
 public String status="";
 
public RRA_RECEIVED_PACKAGE(String recMsg,String data,String status)
{
    this.recMsg=recMsg;
    this.data=data;
    this.status=status;    
}
public RRA_RECEIVED_PACKAGE(String stat,String res)
{ 
 this.data=res;
 this.status=stat;
 
if(status!=null && status.contains("E"))
data= errorHandling(res); 

}
 
public String errorHandling( String E)
{
//    00 – no error;
//ii. 11 – internal memory full;
//iii. 12 – internal data corrupted;
//iv. 13 – internal memory error;
//v. 20 – Real Time Clock error;
//vi. 30 – wrong command code;
//vii. 31 – wrong data format in the CIS request data;
//viii. 32 – wrong TIN in the CIS request data;
//ix. 33 – wrong tax rate in the CIS request data;
//x. 34 – invalid receipt number int the CIS request data;
         
   if( E.contains("E00"))
  return  "no error#SDC RECEIVE AN ERROR MSG";
   else if(  E.contains("E01"))
  return  "TIME OUT 1000mlls#SDC RECEIVE AN ERROR MSG";
   else if(  E.contains("E11"))
  return  "internal memory full#SDC RECEIVE AN ERROR MSG";
   else if( E.contains("E12"))
  return  "internal data corrupted#SDC RECEIVE AN ERROR MSG";
   else if(  E.contains("E13"))
  return  "internal memory error#SDC RECEIVE AN ERROR MSG";
   else if(  E.contains("E20"))
  return  "Real Time Clock error#SDC RECEIVE AN ERROR MSG";
   else if(  E.contains("E30"))
  return  "wrong command code#SDC RECEIVE AN ERROR MSG";
   else if(  E.contains("E31") )
  return  "wrong data format in the CIS request data#SDC RECEIVE AN ERROR MSG";
  else if(  E.contains("E33"))
  return  "wrong tax rate in the CIS request data#SDC RECEIVE AN ERROR MSG";
   else if(  E.contains("E32"))
  return  "wrong TIN in the CIS request data#SDC RECEIVE AN ERROR MSG";
   else if(  E.contains("E34"))
  return  "invalid receipt number int the CIS request data#SDC RECEIVE AN ERROR MSG";
  else if( E.contains("E40"))
  return  "SDC not activated#SDC RECEIVE AN ERROR MSG";
   else if(  E.contains("E41") )
  return  "SDC already activated#SDC RECEIVE AN ERROR MSG";
   else if(  E.contains("E90"))
  return  "SIM card error#SDC RECEIVE AN ERROR MSG";
   else if(   E.contains("E91"))
  return  "GPRS modem error#SDC RECEIVE AN ERROR MSG";
   else if(  E.contains("E99"))
  return  "hardware intervention is necessary#SDC RECEIVE AN ERROR MSG";
  else 
  return  "ERROR NOT SUPPORTED#SDC RECEIVE AN ERROR MSG";
  
//    if(b==00 || E.contains("E00"))
//  JOptionPane.showMessageDialog(null, "no error", "SDC RECEIVE AN ERROR MSG", JOptionPane.WARNING_MESSAGE);
//   else if(b==01|| E.contains("E01"))
//  JOptionPane.showMessageDialog(null, "TIME OUT 1000mlls", "SDC RECEIVE AN ERROR MSG", JOptionPane.WARNING_MESSAGE);
//   else if(b==11|| E.contains("E11"))
//  JOptionPane.showMessageDialog(null, "internal memory full", "SDC RECEIVE AN ERROR MSG", JOptionPane.WARNING_MESSAGE);
//   else if(b==12|| E.contains("E12"))
//  JOptionPane.showMessageDialog(null, "internal data corrupted", "SDC RECEIVE AN ERROR MSG", JOptionPane.WARNING_MESSAGE);
//   else if(b==13|| E.contains("E13"))
//  JOptionPane.showMessageDialog(null, "internal memory error", "SDC RECEIVE AN ERROR MSG", JOptionPane.WARNING_MESSAGE);
//   else if(b==20|| E.contains("E20"))
//  JOptionPane.showMessageDialog(null, "Real Time Clock error", "SDC RECEIVE AN ERROR MSG", JOptionPane.WARNING_MESSAGE);
//   else if(b==30 || E.contains("E30"))
//  JOptionPane.showMessageDialog(null, "wrong command code", "SDC RECEIVE AN ERROR MSG", JOptionPane.WARNING_MESSAGE);
//   else if(b==31 || E.contains("E31") )
//  JOptionPane.showMessageDialog(null, "wrong data format in the CIS request data", "SDC RECEIVE AN ERROR MSG", JOptionPane.WARNING_MESSAGE);
//  else if(b==33 || E.contains("E33"))
//  JOptionPane.showMessageDialog(null, "wrong tax rate in the CIS request data", "SDC RECEIVE AN ERROR MSG", JOptionPane.WARNING_MESSAGE);
//   else if(b==32 || E.contains("E32"))
//  JOptionPane.showMessageDialog(null, "wrong TIN in the CIS request data", "SDC RECEIVE AN ERROR MSG", JOptionPane.WARNING_MESSAGE);
//   else if(b==34 || E.contains("E34"))
//  JOptionPane.showMessageDialog(null, "invalid receipt number int the CIS request data", "SDC RECEIVE AN ERROR MSG", JOptionPane.WARNING_MESSAGE);
//  else if(b==40 || E.contains("E40"))
//  JOptionPane.showMessageDialog(null, "SDC not activated", "SDC RECEIVE AN ERROR MSG", JOptionPane.WARNING_MESSAGE);
//   else if(b==41 || E.contains("E41") )
//  JOptionPane.showMessageDialog(null, "SDC already activated", "SDC RECEIVE AN ERROR MSG", JOptionPane.WARNING_MESSAGE);
//   else if(b==90 || E.contains("E90"))
//  JOptionPane.showMessageDialog(null, "SIM card error", "SDC RECEIVE AN ERROR MSG", JOptionPane.WARNING_MESSAGE);
//   else if(b==91 || E.contains("E91"))
//  JOptionPane.showMessageDialog(null, "GPRS modem error", "SDC RECEIVE AN ERROR MSG", JOptionPane.WARNING_MESSAGE);
//   else if(b==99 || E.contains("E99"))
//  JOptionPane.showMessageDialog(null, "hardware intervention is necessary", "SDC RECEIVE AN ERROR MSG", JOptionPane.WARNING_MESSAGE);
//  else 
//  JOptionPane.showMessageDialog(null, "ERROR NOT SUPPORTED "+E, "SDC RECEIVE AN ERROR MSG", JOptionPane.WARNING_MESSAGE);
//  
       
}
}
