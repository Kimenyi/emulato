/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ebm.sdc.rra2;

import java.awt.Container;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author Kimenyi
 */
public class RRA_CHECK_EBM {
    
String path;
Container content;
RRA_DB db;
    public RRA_CHECK_EBM(String path, String debut, String fin,RRA_DB db,Container content ) {
        this.path = path; 
        this.content=content; 
        this.db=db;
        LinkedList<RRA_INVOICE> ISHYIGA=db.getRRA_INVOICE(  debut,   fin);
        System.out.println(ISHYIGA.size());
        LinkedList<RRA_INVOICE> EBM=getEBMXlsx(ISHYIGA.getLast());
        System.out.println(EBM.size());
        EBM=reba(ISHYIGA,EBM);
        getFailInvoice( EBM);
    }
    private LinkedList<RRA_INVOICE>  reba(LinkedList<RRA_INVOICE> ISHYIGA,LinkedList<RRA_INVOICE> EBM)
    {
    
        for(int i=0;i<ISHYIGA.size();i++)
        {
            RRA_INVOICE ishyiga=ISHYIGA.get(i);
            boolean find=false;
             for(int j=0;j<EBM.size();j++)
             {
              RRA_INVOICE ebm=EBM.get(j);             
               if(ishyiga.isEqual(ebm))
                {
                    find=true;
                    EBM.remove(j);
                    break;
                }             
             }             
             if(!find)
             {
                  System.out.println(" ----------------  "+ishyiga.toString2());
             }
             } 
          for(int j=0;j<EBM.size();j++)
             {
                ISHYIGA.add(EBM.get(j));
             }
        
         return ISHYIGA;
    }
 private void getFailInvoice(LinkedList<RRA_INVOICE> EBM)
    {
        String [][] s=new String [EBM.size()][8];
        double total=0;
    for(int j=0;j<EBM.size();j++)
{
 RRA_INVOICE ebm=EBM.get(j);    
 System.out.println( "  ++++  "+ebm.toString2()); 
 
 s[j][0]=""+db.getIdInvoiceFailed(""+(ebm.SDC+","+(ebm.id_invoice-1)+","), 
         ""+(ebm.SDC+","+(ebm.id_invoice+1)+","));
 s[j][1]= ""+ebm.id_invoice;
 s[j][2]= ""+ebm.SDC;
 s[j][3]= ""+RRA_PRINT2.setVirgule(ebm.amountSDC, 1, 0.005);
 s[j][4]= ""+ebm.indi.id_invoice;
 s[j][5]= ""+ebm.indi.SDC;
 s[j][6]= ""+RRA_PRINT2.setVirgule(ebm.indi.amountSDC, 1, 0.005);
 
 
 total+=ebm.amountSDC;
}
 JTable   jTable =new JTable();
 String [] header=new String [] {
    "ID ISHYIGA DB ", "ID ISHYIGA ",  "SDC ", " AMOUNT " ,"ID EBM ",  "SDC ", " AMOUNT " };
jTable.setModel(new javax.swing.table.DefaultTableModel(s ,header
 ));
jTable.setAutoResizeMode(jTable.AUTO_RESIZE_ALL_COLUMNS); 
//jTable.getColumnModel().getColumn(4).setPreferredWidth(300);//.setWidth(200);
//jTable.getColumnModel().getColumn(5).setPreferredWidth(300);//.setWidth(200);
jTable.doLayout();
jTable.validate(); 

writeFaile(  header,  s);  

JOptionPane.showConfirmDialog(content,jTable ," TOTAL FAIL "+RRA_PRINT2.setVirgule(total, 1, 0.005),JOptionPane.OK_CANCEL_OPTION);
    
    
}
    
      private LinkedList<RRA_INVOICE> getEBMXlsx(RRA_INVOICE last)
    { 
        LinkedList<RRA_INVOICE>vars=new LinkedList(); 
          String [][] n=  getXls();
         
          
            System.out.println("size rra  "+n.length);
             System.out.println(last.toString2());
            
            for(int i=0;i<n.length;i++)
            {   
                //03/01/2014;SDC002000229/6.880;;0,00;0,00;;0,00;;0,00;;0,00;4910,00;4910,00;;0,00;0,00;;0,00
                  //04/04/2014;SDC002000934/1;0;7100;350;0;1030
  // 1  41759.91711805556  6  Not Identified  8  Different Items  11  SDC002000934/1.104  12  0.0  13  0.0  15  0.0  17  0.0  19  0.0  20  46960.0  21  2100.0  23  0.0  24  44860.0  26  6843.05null  5562
         if(n[i][11]!=null &&  n[i][20]!=null  )
         { String [] sp=n[i][11].split("/");
         
         if(sp !=null &&  sp.length==2  )
         {
          String sdc=sp[0] ;
          String invoicesdc=sp[1].replaceAll("\\.", ""); 
          String ammountsdc=n[i][20]; 
    RRA_INVOICE ri=new RRA_INVOICE(Integer.parseInt(invoicesdc), sdc,  Double.parseDouble(ammountsdc));
               System.out.println(ri.toString2());
                if(ri.SDC.equals(last.SDC))
               {
                   vars.add(ri);
               }
         }
         
         }
          
         
         
            } 
        
      return vars;  
    }
      String [][] getXls()
 {
     String [][] arrayReport=new String [10000][100];
            try
        {
             JFileChooser chooser = new JFileChooser();
                chooser.setCurrentDirectory(new File(path));
                int returnVal = chooser.showOpenDialog(content);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    System.out.println("You chose to open this file: "
                            + chooser.getSelectedFile().getName());
                }
                String s = "" + chooser.getCurrentDirectory() + '\\' + chooser.getSelectedFile().getName();
              
                System.out.println("fiiiillllleeeee    "+s);
            
            FileInputStream file = new FileInputStream(new File(s));
 
            //Create Workbook instance holding reference to .xlsx file
            XSSFWorkbook workbook = new XSSFWorkbook(file);
 
            //Get first/desired sheet from the workbook
            XSSFSheet sheet = workbook.getSheetAt(0);
 
            //Iterate through each rows one by one
            Iterator<Row> rowIterator = sheet.iterator();
            int i=0; int j=0;
            while (rowIterator.hasNext()) 
            {
                Row row = rowIterator.next();
                //For each row, iterate through all the columns
                Iterator<Cell> cellIterator = row.cellIterator();
                  
                while (cellIterator.hasNext()) 
                {
                    Cell cell = cellIterator.next();
                    //Check the cell type and format accordingly
                    j++;
                    switch (cell.getCellType()) 
                    {
                        case Cell.CELL_TYPE_NUMERIC:
//                            System.out.print (  "  "+j+"  "+cell.getNumericCellValue());
                            if(j<100)
                           arrayReport[i][j]=""+(cell.getNumericCellValue());
                            break;
                        case Cell.CELL_TYPE_STRING:
//                             System.out.print ( "  "+j+"  "+cell.getStringCellValue());
                             if(j<100)
                            arrayReport[i][j]=(cell.getStringCellValue() );
                            break;
                    }
                    
                }
                j=0;
//                System.out.println(arrayReport[i][0]+"  "+i);
               i++;
            }
            file.close();
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
            return arrayReport;
      }    
LinkedList <String>getFichier(File productFile)throws FileNotFoundException, IOException
{
    LinkedList<String> give= new LinkedList();

       // = new File(fichier);
        String  ligne =null;
        BufferedReader entree=new BufferedReader(new FileReader(productFile));
        try{ ligne=entree.readLine(); }
        catch (IOException e){ System.out.println(e);}
        int i=0;
        while(ligne!=null)
        {
        // System.out.println(i+ligne);
        give.add(ligne);
        i++;
        try{ ligne =entree.readLine();}
        catch (IOException e){ System.out.println(e);}
        }
        entree.close();
return give;
} 
void writeFaile(String [] header,String [] [] s)
{
//Blank workbook
        XSSFWorkbook workbook = new XSSFWorkbook(); 
         
        //Create a blank sheet
        XSSFSheet sheet = workbook.createSheet("FAIL EBM INVOICE ISHYIGA");
          
        //This data needs to be written (Object[])
        Map<String, Object[]> data = new TreeMap<String, Object[]>();
        
        data.put("1", header);
        for(int i=0;i<s.length;i++)
        {data.put(""+(i+1), s[i]);
        } 
        //Iterate over data and write to sheet
        Set<String> keyset = data.keySet();
        int rownum = 0;
        for (String key : keyset)
        {
            Row row = sheet.createRow(rownum++);
            Object [] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr)
            {
               Cell cell = row.createCell(cellnum++);
               if(obj instanceof String)
                    cell.setCellValue((String)obj);
                else if(obj instanceof Integer)
                    cell.setCellValue((Integer)obj);
            }
        }
        try
        {
            String  file="FAILED/"+( (new Date().toLocaleString()) );
            file=file.replace(':', '-');
            //Write the workbook in file system
            FileOutputStream out = new FileOutputStream(new File(file+".xlsx"));
            workbook.write(out);
            out.close();
            System.out.println("howtodoinjava_demo.xlsx written successfully on disk.");
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }


}

}
