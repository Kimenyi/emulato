/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ebm.sdc.rra;

/**
 *
 * @author toshiba
 */
public class RRA_SDC_FAGITURE {

    String NUM_CLIENT;
    String MRC="";
    String TIN="";
    String DATE;
    int ID_INVOICE;
    double TOTAL;
    String EMPLOYE;
    double TVA;
    double CASH;
    double CREDIT;
    String KEY_INVOICE;
    String TOSEND;

    double TOTAL_A;
    double TOTAL_B;
    double TOTAL_C;
    double TOTAL_D;
    
    double TAX_A;
    double TAX_B;
    double TAX_C;
    double TAX_D;
    
    
    public RRA_SDC_FAGITURE(String NUM_CLIENT, String DATE, double TOTAL, String EMPLOYE, double TVA, double CASH, double CREDIT, String KEY_INVOICE,String TOSENDD) {
        this.NUM_CLIENT = NUM_CLIENT;
        this.DATE = DATE;
        this.TOTAL = TOTAL;
        this.EMPLOYE = EMPLOYE;
        this.TVA = TVA;
        this.CASH = CASH;
        this.CREDIT = CREDIT;
        this.KEY_INVOICE = KEY_INVOICE;
        this.TOSEND=TOSENDD;
        this.ID_INVOICE=0;
        if(TOSEND==null)
        {
            if(TVA==0)
            {
                TOTAL_A=TOTAL;
                TOTAL_B=0;
                TOTAL_C=0;
                TOTAL_D=0;
                
                TAX_A=0;
                TAX_B=0;
                TAX_C=0;
                TAX_D=0; 
            }
            else
            {
//                double ht=TVA/0.18;
//                double totb=ht*1.18;
//                TOTAL_A=
            }
            
        }
        else if(TOSEND!=null)
        {
            String [] data=TOSEND.split("#");
            //NSALG01000107,101587911,&TIME&,&ID_INVOICE&,0.00,18.00,0.00,0.00,806.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00
            String [] total=data[1].split(",");
            TIN=""+total[1];
            MRC=""+total[0].substring(2,13);
            TOTAL_A=Double.parseDouble(total[8]);
            TOTAL_B=Double.parseDouble(total[9]);
            TOTAL_C=Double.parseDouble(total[10]);
            TOTAL_D=Double.parseDouble(total[11]);
            
            TAX_A=Double.parseDouble(total[12]);
            TAX_B=Double.parseDouble(total[13]);
            TAX_C=Double.parseDouble(total[14]);
            TAX_D=Double.parseDouble(total[15]); 
        }
    }
    
    public RRA_SDC_FAGITURE(int ID_INVOICE,double TOTAL_A,double TOTAL_B,double TOTAL_C,double TOTAL_D,double TAX_A,double TAX_B,double TAX_C,double TAX_D,String KEY_INVOICE)
    {
        this.ID_INVOICE=ID_INVOICE;
        this.TOTAL_A=TOTAL_A;
        this.TOTAL_B=TOTAL_B;
        this.TOTAL_C=TOTAL_C;
        this.TOTAL_D=TOTAL_D; 
        this.TAX_A=TAX_A;
        this.TAX_B=TAX_B;
        this.TAX_C=TAX_C;
        this.TAX_D=TAX_D;
        this.KEY_INVOICE=KEY_INVOICE;
        TOTAL=TOTAL_A+TOTAL_B+TOTAL_C+TOTAL_D;
        TVA=TAX_A+TAX_B+TAX_C+TAX_D;
    }
    
    
    
    
}
