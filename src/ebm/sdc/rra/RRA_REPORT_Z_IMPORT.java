/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ebm.sdc.rra;

import java.awt.Font;
import java.util.LinkedList;

/**
 *
 * @author Kimenyi
 */
public class RRA_REPORT_Z_IMPORT {
    
    RRA_DB db ; 
    String hera,geza,tradename,tin,CIS,MRC ;
    double   tvaRateA,   tvaRateB,   tvaRateC,   tvaRateD;
    
    public RRA_REPORT_Z_IMPORT(RRA_DB db, String hera, String geza, String tradename, 
            String tin, String CIS, String MRC,double tvaRateA, double tvaRateB, double tvaRateC, double tvaRateD ) {
        this.db = db;
        this.hera = hera;
        this.geza = geza;
        this.tradename = tradename;
        this.tin = tin;
        this.CIS = CIS;
        this.MRC = MRC;
        this.tvaRateA = tvaRateA;
        this.tvaRateB = tvaRateB;
        this.tvaRateC = tvaRateC;
        this.tvaRateD = tvaRateD;
    }

    
double totalSalesNS() 
{
return db.sum("invoice", "total", "EXTERNAL_DATA like '%NS%'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");
} 
double totalSalesNSGoupe(String groupe)
{
return db.sum("invoice", "total", "EXTERNAL_DATA like '%NS%' and TARIF='"+groupe+"'",
        "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");
}
int numberSalesNS()
{return db.count("invoice", "ID_INVOICE", "EXTERNAL_DATA like '%NS%'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");}
double totalRefundNR()// including tax;
{
return db.sum("REFUND", "total", "EXTERNAL_DATA like '%NR%'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");
} 
int  numberRefundNR()
{return db.count("invoice", "ID_INVOICE", "EXTERNAL_DATA like '%NR%'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");}
       
double taxableAmounts(String rate)// per applicable tax rates divided between sales (NS) and refunds (NR);
{
return db.sum("invoice", rate, "EXTERNAL_DATA like '%NS%' ", "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");
}
double taxAmounts(String rate) //per applicable tax rates divided between sales (NS) and refunds(NR);
{
return db.sum("invoice", rate, "EXTERNAL_DATA like '%NS%' ", "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");
}
double  openingDeposit()
{ 
return db.sum("LOGIN", "ARGENT", "IN_OUT= 'IN' ", "LOG >'"+hera+"' AND LOG <'"+geza+"'");
}
int  numberItemsSold()
{double [] taxes= new double []{1,   1,   1,   1};    
return db.getPLU(hera, geza,taxes).size();
}
int numberCS() 
{return db.count("invoice", "ID_INVOICE", "COPY_DATA like '%CS%'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");}

int numberCR()
{return db.count("REFUND", "ID_REFUND", "COPY_DATA like '%CR%'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");}

double totalCS()
{
return db.sum("invoice", "total", "COPY_DATA like '%CS%'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");
} 
double totalCR()
{
return db.sum("REFUND", "total", "COPY_DATA like '%CR%'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");
}  
int numberTS ()
{return db.count("invoice", "ID_invoice", "EXTERNAL_DATA like '%TS%'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");}
 int numberTR ()
{return db.count("REFUND", "ID_REFUND", "EXTERNAL_DATA like '%TR%'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");}
double totalTS ()
{
return db.sum("invoice", "total", "EXTERNAL_DATA like '%TS%'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");
} 
double totalTR ()
{
return db.sum("REFUND", "total", "EXTERNAL_DATA like '%TR%'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");
} 
int numberPS ()
{
return db.count("BON_PROFORMA", "ID_PROFORMA", "EXTERNAL_DATA like '%PS%'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");
}
double totalPS ()
{
return db.sum("BON_PROFORMA", "total", "EXTERNAL_DATA like '%PS%'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");
}  
double totalSalesCash()// amount for all sales receipts labeled as NS, including tax;
{
return db.sum("invoice", "total", "EXTERNAL_DATA like '%NS%' and MODE='FACTURE CASH'",
  "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");
} 
double totalSalesCredit()// amount for all sales receipts labeled as NS, including tax;
{
return db.sum("invoice", "total", "EXTERNAL_DATA like '%NS%' and MODE='FACTURE CREDIT '",
  "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");
}
double totalRefundNRCash()// amount for all sales receipts labeled as NS, including tax;
{
return db.sum("REFUND", "total", "EXTERNAL_DATA like '%NR%' and MODE='FACTURE CASH'",
  "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");
} 
double totalRefundNRCredit()// amount for all sales receipts labeled as NS, including tax;
{
return db.sum("REFUND", "total", "EXTERNAL_DATA like '%NR%' and MODE='FACTURE CREDIT '",
  "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");
} 
double totaDiscounts()  
{
return db.sum("invoice", "REDUCTION", " HEURE >'"+hera+"' ",
  "  HEURE <'"+geza+"'");
}  
//xix. other registrations that have reduced the day's sales and their amount;
int numberIncompleteSales()
{return
db.count("invoice", "ID_INVOICE", "EXTERNAL_DATA = 'EMPTY'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"'")
        ;
}
String setVirguleDouble(double dou)
{
return " "+dou;
}
String setVirguleInt(int in)
{
return " "+in;
}
LinkedList <RRA_PRINT2_LINE> toPrint()
{
  //  daily report hera,geza,tradename,tin,CIS,MRC 
    
LinkedList <RRA_PRINT2_LINE> toPrint = new LinkedList();
Font f = new Font("Arial", Font.BOLD, 12 );
 
toPrint.add(new RRA_PRINT2_LINE(" Z-REPORT ", "", f)); 
toPrint.add(new RRA_PRINT2_LINE(" START : "+hera, "", f)); 
toPrint.add(new RRA_PRINT2_LINE(" END : "+geza, "", f)); 
toPrint.add(new RRA_PRINT2_LINE(" TRADE NAME :"+tradename, "", f)); 
toPrint.add(new RRA_PRINT2_LINE(" TIN  : "+tin, "", f)); 
toPrint.add(new RRA_PRINT2_LINE(" MRC : "+MRC, "", f));  
toPrint.add(new RRA_PRINT2_LINE(" CIS  : "+CIS, "", f)); 
toPrint.add(new RRA_PRINT2_LINE(" -------------------------  ", "", f));
toPrint.add(new RRA_PRINT2_LINE(" TAXE RATE A :"+RRA_PRINT2.setVirgule(tvaRateA,1,0.005), "", f)); 
toPrint.add(new RRA_PRINT2_LINE(" TAXE RATE B :"+RRA_PRINT2.setVirgule(tvaRateB,1,0.005), "", f)); 
toPrint.add(new RRA_PRINT2_LINE(" TAXE RATE C :"+RRA_PRINT2.setVirgule(tvaRateC,1,0.005), "", f)); 
toPrint.add(new RRA_PRINT2_LINE(" TAXE RATE D :"+RRA_PRINT2.setVirgule(tvaRateD,1,0.005), "", f));  
toPrint.add(new RRA_PRINT2_LINE("   ", "", f));
f = new Font("Arial", Font.PLAIN, 10 );
toPrint.add(new RRA_PRINT2_LINE(" -------- SALES -----------   ", "", f));
toPrint.add(new RRA_PRINT2_LINE("   ", "", f));
toPrint.add(new RRA_PRINT2_LINE("TOTAL SALES NS ", setVirguleDouble(totalSalesNS()), f)); 
LinkedList<String> clients=db.clients ();
for(int i=0;i<clients.size();i++)
{
    String clie=clients.get(i);
    double tot=totalSalesNSGoupe(clie);
    if(tot!=0)
    {toPrint.add(new RRA_PRINT2_LINE("TOTAL GROUPE NS "+clie,
            setVirguleDouble(tot), f));}
}
 
toPrint.add(new RRA_PRINT2_LINE("NUMBER SALES NS ", setVirguleInt(numberSalesNS()), f));
toPrint.add(new RRA_PRINT2_LINE("TOTAL COPY SALE ", setVirguleDouble(totalCS()), f));
toPrint.add(new RRA_PRINT2_LINE("NUMBER COPY SALE ", setVirguleInt(numberCS()), f));
toPrint.add(new RRA_PRINT2_LINE("TOTAL SALES CASH ", setVirguleDouble(totalSalesCash()), f));
toPrint.add(new RRA_PRINT2_LINE("TOTAL SALES CREDIT ", setVirguleDouble(totalSalesCredit()), f));
toPrint.add(new RRA_PRINT2_LINE("TOTAL TRAINING SALES ", setVirguleDouble(totalTS ()), f));
toPrint.add(new RRA_PRINT2_LINE("NUMBER TRAINING SALES ", setVirguleInt(numberTS ()), f)); 
toPrint.add(new RRA_PRINT2_LINE("TOTAL PROFORMA SALES", setVirguleDouble(totalPS ()), f));
toPrint.add(new RRA_PRINT2_LINE("NUMBER PROFORMA SALES ", setVirguleInt(numberPS ()), f)); 
toPrint.add(new RRA_PRINT2_LINE("   ", "", f));
toPrint.add(new RRA_PRINT2_LINE("-------- REFUND -----------   ", "", f));
toPrint.add(new RRA_PRINT2_LINE("   ", "", f));
toPrint.add(new RRA_PRINT2_LINE("TOTAL REFUND NR ", setVirguleDouble(totalRefundNR()), f)); 
toPrint.add(new RRA_PRINT2_LINE("NUMBER REFUND NR ", setVirguleInt(numberRefundNR()), f));
toPrint.add(new RRA_PRINT2_LINE("TOTAL COPY REFUND ", setVirguleDouble(totalCR()), f));
toPrint.add(new RRA_PRINT2_LINE("NUMBER COPY REFUND ", setVirguleInt(numberCR()), f));
toPrint.add(new RRA_PRINT2_LINE("TOTAL REFUND CASH ", setVirguleDouble(totalRefundNRCash()), f));
toPrint.add(new RRA_PRINT2_LINE("TOTAL REFUND CREDIT ", setVirguleDouble(totalRefundNRCredit()), f));  
toPrint.add(new RRA_PRINT2_LINE("TOTAL TRAINING REFUND ", setVirguleDouble(totalTR ()), f));
toPrint.add(new RRA_PRINT2_LINE("NUMBER TRAINING REFUND ", setVirguleInt(numberTR ()), f));
 
toPrint.add(new RRA_PRINT2_LINE("   ", "", f));
toPrint.add(new RRA_PRINT2_LINE("--------- TAXE ----------   ", "", f));
toPrint.add(new RRA_PRINT2_LINE("   ", "", f));
toPrint.add(new RRA_PRINT2_LINE("TOTAL TAXABLE  A ", setVirguleDouble(taxableAmounts("TOTAL_A")), f));   
toPrint.add(new RRA_PRINT2_LINE("TOTAL TAXABLE  B ", setVirguleDouble(taxableAmounts("TOTAL_B")), f));   
toPrint.add(new RRA_PRINT2_LINE("TOTAL TAXABLE  C ", setVirguleDouble(taxableAmounts("TOTAL_C")), f));   
toPrint.add(new RRA_PRINT2_LINE("TOTAL TAXABLE  D ", setVirguleDouble(taxableAmounts("TOTAL_D")), f));   
 
toPrint.add(new RRA_PRINT2_LINE("TOTAL TAXE  A ", setVirguleDouble(taxAmounts("TAXE_A")), f));   
toPrint.add(new RRA_PRINT2_LINE("TOTAL TAXE  B ", setVirguleDouble(taxAmounts("TAXE_B")), f));   
toPrint.add(new RRA_PRINT2_LINE("TOTAL TAXE  C ", setVirguleDouble(taxAmounts("TAXE_C")), f));   
toPrint.add(new RRA_PRINT2_LINE("TOTAL TAXE  D ", setVirguleDouble(taxAmounts("TAXE_D")), f));   
 toPrint.add(new RRA_PRINT2_LINE("   ", "", f));
toPrint.add(new RRA_PRINT2_LINE(" --------- # --------------  ", "", f));
toPrint.add(new RRA_PRINT2_LINE("   ", "", f));
toPrint.add(new RRA_PRINT2_LINE("OPENING DEPOSIT ", setVirguleDouble(openingDeposit()), f)); 
toPrint.add(new RRA_PRINT2_LINE("NUMBER OF ITEM SOLD ", setVirguleInt(numberItemsSold()), f));
toPrint.add(new RRA_PRINT2_LINE("TOTAL DISCOUNT ", setVirguleDouble(totaDiscounts() ), f)); 
toPrint.add(new RRA_PRINT2_LINE("NUMBER INCOMPLETE SALES ", setVirguleInt(numberIncompleteSales()), f)); 
 
return toPrint;
}

}
