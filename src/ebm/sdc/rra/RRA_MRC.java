/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ebm.sdc.rra;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.InetAddress;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Kimenyi
 */
public class RRA_MRC extends JFrame{
 String MRC,MACHINE_NAME,COMPANY_NAME ,MAIN_USER,DPT,MAC,SDC_IPL ;   
 JTextField MRC_T,MACHINE_NAME_T,COMPANY_NAME_T ,MAIN_USER_T,DPT_T,MAC_T,SDC_IP ;
 JButton ADD,DELETE,UPDATE,PRINT;
 RRA_DB db;
    public RRA_MRC(String MRC,String MAC, String MACHINE_NAME, String COMPANY_NAME, String MAIN_USER, String DPT,String SDC_IP) {
        this.MRC = MRC;
        this.MAC = MAC;
        this.MACHINE_NAME = MACHINE_NAME;
        this.COMPANY_NAME = COMPANY_NAME;
        this.MAIN_USER = MAIN_USER;
        this.DPT = DPT;
        this.SDC_IPL=SDC_IP;
    }
 
public  RRA_MRC(   RRA_DB db )
{
this.db=db;  

String  CHOIX=  (String) JOptionPane.showInputDialog(null, "CHOIX", " ",
JOptionPane.QUESTION_MESSAGE, null, new String[]{  "SELECT","MRC","MAC","NAME", "NEW" }, "MAC");

String sql=""; 
if(CHOIX.equals("MRC"))
{ 
String MRC=JOptionPane.showInputDialog(rootPane, "MRC");
sql="where APP.SDC_MRC.MRC= '"+MRC+"'";}
else if (CHOIX.equals("MAC"))
{ 
sql="where APP.SDC_MRC.MAC_ADDRESS= '"+getMac()+"'";}
else if (CHOIX.equals("NAME"))
{ String name="";   
try { 
 name=""+InetAddress.getLocalHost();
} catch (Exception ex) {
    Logger.getLogger(RRA_DB.class.getName()).log(Level.SEVERE, null, ex);
}
sql="where APP.SDC_MRC.MACHINE_NAME= '"+name+"'";}
else if (CHOIX.equals("NEW") || CHOIX.equals("SELECT"))
{ sql=CHOIX;}

makeFrame( db.getRRA_MRC (sql));
send(); 
update  ();delete();print();
}
 public void makeFrame( RRA_MRC m)
{
 
  
        this.MRC_T = new JTextField(m.MRC);
	this.MACHINE_NAME_T  = new JTextField(m.MACHINE_NAME);
	this.COMPANY_NAME_T  = new JTextField(m.COMPANY_NAME);
	this.MAIN_USER_T = new JTextField(m.MAIN_USER); 
        this.DPT_T = new JTextField(m.DPT); 
        this.MAC_T = new JTextField(m.MAC);
        this.SDC_IP=new JTextField(m.SDC_IPL);
        
        JPanel Pane  = new JPanel(); 
JPanel panel = new JPanel();
panel.setLayout(new GridLayout(7,2,5,20));//
panel.setPreferredSize(new Dimension(450,300)); //400=largeur350=longeur
 

panel.add(new JLabel("MRC_T "));panel.add(MRC_T);
panel.add(new JLabel("MAC_T "));panel.add(MAC_T);
panel.add(new JLabel("MACHINE_NAME_T "));panel.add(MACHINE_NAME_T);
panel.add(new JLabel("COMPANY_NAME_T "));panel.add(COMPANY_NAME_T);
panel.add(new JLabel("MAIN_USER_T "));panel.add(MAIN_USER_T); 
panel.add(new JLabel("DPT_T "));panel.add(DPT_T);
panel.add(new JLabel("SDC_IP "));panel.add(SDC_IP);
Pane.add(panel,BorderLayout.CENTER);

    this.ADD=new JButton("ADD");
    ADD.setBackground(Color.green);
    this.DELETE=new JButton("DELETE");
    DELETE.setBackground(Color.red);
    this.UPDATE=new JButton("UPDATE");
    UPDATE.setBackground(Color.yellow);
    this.PRINT=new JButton("PRINT"); 
    
    JPanel face = new JPanel();
    face.setLayout(new GridLayout(1,4));
    face.setPreferredSize(new Dimension(450,70)); 
    face.add(ADD);face.add(UPDATE);face.add(DELETE);face.add(PRINT);
 Container content = getContentPane();
    JPanel mainInsurance = new JPanel();
mainInsurance.add(panel);
mainInsurance.add(face);
content.add(mainInsurance);

     setTitle("MRC MGT Ishyiga"); 
    this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);  
      
     setSize(500,450);
     setVisible(true); 

}
 /**********************************************************************************************
AJOUT Mannuel
**********************************************************************************************/
public void send()
{ ADD.addActionListener(new ActionListener() {
 public void actionPerformed(ActionEvent ae)
 { if(ae.getSource()==ADD  )
  {
        
        String MRC= MRC_T.getText();
	String MACHINE_NAME = MACHINE_NAME_T.getText();
	String COMPANY_NAME = COMPANY_NAME_T.getText(); 
	String MAIN_USER = MAIN_USER_T.getText();
        String DPT = DPT_T.getText();
        String SDC_IPP=SDC_IP.getText();
        
String MAC = MAC_T.getText();
RRA_MRC m = new RRA_MRC  (  MRC,MAC,   MACHINE_NAME,   COMPANY_NAME,   MAIN_USER, DPT,SDC_IPP) ;
 db.insertRRA_MRC(m);
 }
 }});   
} 
public void update()
{ UPDATE.addActionListener(new ActionListener() {
 public void actionPerformed(ActionEvent ae)
 { if(ae.getSource()==UPDATE )
  {
        
        String MRC= MRC_T.getText();
	String MACHINE_NAME = MACHINE_NAME_T.getText();
	String COMPANY_NAME = COMPANY_NAME_T.getText(); 
	String MAIN_USER = MAIN_USER_T.getText();
        String MAC = MAC_T.getText();
        String DPT = DPT_T.getText();
        String SDC_IPP=SDC_IP.getText();
RRA_MRC m = new RRA_MRC  (  MRC,MAC,   MACHINE_NAME,   COMPANY_NAME,   MAIN_USER, DPT,SDC_IPP) ;
 
db.upDateRRA_MRC(m); 
 }
 }});   
} 
public void delete()
{ DELETE.addActionListener(new ActionListener() {
 public void actionPerformed(ActionEvent ae)
 { if(ae.getSource()==DELETE )
  {
 String MRC= MRC_T.getText(); 
db.deleteRRA_MRC(MRC); 
 }
 }});   
} 
public void print()
{ 
PRINT.addActionListener(new ActionListener() {
public void actionPerformed(ActionEvent ae)
{
if(ae.getSource()==PRINT  )
{
LinkedList <RRA_PRINT2_LINE> toPrint = new LinkedList();
Font f = new Font("Arial", Font.BOLD, 12 ); 
toPrint.add(new RRA_PRINT2_LINE(" MRC : "+MRC_T.getText(), "", f)); 
toPrint.add(new RRA_PRINT2_LINE(" MAC : "+MAC_T.getText()     , "", f)); 
toPrint.add(new RRA_PRINT2_LINE(" © "+COMPANY_NAME_T.getText(), "", f)); 
new  RRA_PRINT2 (  toPrint,"C:/Users/Kimenyi/Desktop/FRSS/Google Drive/PharmaRama4.0.2/final.gif");
}
 }}); 
}  
@Override
protected void processWindowEvent(WindowEvent evt)
{
   if(WindowEvent.WINDOW_CLOSING == evt.getID())
   {
	   this.dispose();
 }

} 
    
    
static String getMac()
   {
       try {
                Process p = Runtime.getRuntime().exec("getmac /fo csv /nh");
                java.io.BufferedReader in = new java.io.BufferedReader(new  java.io.InputStreamReader(p.getInputStream()));
                String line;
                line = in.readLine();        
                String[] result = line.split(",");
                String res=result[0].replace('"', ' ').trim();
                System.out.println(res);
                return res;
        } catch (IOException ex) {
            System.out.println("getmac:"+ex);
        }
       return "Disabled";
       
   }    
    @Override
   public String toString()
   {return MRC+" "+MACHINE_NAME ;}
   
}
