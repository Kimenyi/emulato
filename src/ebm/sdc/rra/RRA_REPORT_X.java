/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ebm.sdc.rra;

import java.awt.Font;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.StringTokenizer;

/**
 *
 * @author Kimenyi
 */
public class RRA_REPORT_X {
    
    RRA_DB db ; 
    String hera,geza,tradename,tin,CIS,MRC ;
    double   tvaRateA,   tvaRateB,   tvaRateC,   tvaRateD;
    
    public RRA_REPORT_X(RRA_DB db, String hera, String geza, String tradename, 
            String tin, String CIS, String MRC,double tvaRateA, double tvaRateB, double tvaRateC, double tvaRateD) {
        this.db = db;
        this.hera = hera;
        this.geza = geza;
        this.tradename = tradename;
        this.tin = tin;
        this.CIS = CIS;
        this.MRC = MRC;
        this.tvaRateA = tvaRateA;
        this.tvaRateB = tvaRateB;
        this.tvaRateC = tvaRateC;
        this.tvaRateD = tvaRateD;
    }

    
double totalSalesNS() 
{
return db.sum("invoice", "total", "EXTERNAL_DATA like '%,NS,%'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");
} 
double totalSalesNSGoupe(String groupe)
{
return db.sum("invoice", "total", "EXTERNAL_DATA like '%,NS,%' and num_client='"+groupe+"'",
        "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");
}
int numberSalesNS()
{return db.count("invoice", "ID_INVOICE", "EXTERNAL_DATA like '%,NS,%'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");}
double totalRefundNR()// including tax;
{
return db.sum("REFUND", "total", "EXTERNAL_DATA like '%,NR,%'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");
} 
int  numberRefundNR()
{return db.count("REFUND", "ID_REFUND", "EXTERNAL_DATA like '%,NR,%'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");}
       
double taxableAmounts(String rate)// per applicable tax rates divided between sales (NS) and refunds (NR);
{
return db.sum("invoice", rate, "EXTERNAL_DATA like '%,NS,%' ", "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");
}
double taxAmounts(String rate) //per applicable tax rates divided between sales (NS) and refunds(NR);
{
return db.sum("invoice", rate, "EXTERNAL_DATA like '%,NS,%' ", "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");
}
double  login(String in)
{ 
return db.sum("LOGIN", "ARGENT", "IN_OUT= '"+in+"' ", "LOG >'"+hera+"' AND LOG <'"+geza+"'");
}
int  numberItemsSold()
{double [] taxes= new double []{1,   1,   1,   1};    
return db.getPLU(hera, geza,taxes).size();
}
int numberCS() 
{return db.count("invoice", "ID_INVOICE", "COPY_DATA like '%,CS,%'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");}

int numberCR()
{return db.count("REFUND", "ID_REFUND", "COPY_DATA like '%,CR,%'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");}

double totalCS()
{
return db.sum("invoice", "total", "COPY_DATA like '%,CS,%'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");
} 
double totalCR()
{
return db.sum("REFUND", "total", "COPY_DATA like '%,CR,%'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");
}  
int numberTS ()
{return db.count("invoice", "ID_invoice", "EXTERNAL_DATA like '%,TS,%'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");}
 int numberTR ()
{return db.count("REFUND", "ID_REFUND", "EXTERNAL_DATA like '%,TR,%'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");}
double totalTS ()
{
return db.sum("invoice", "total", "EXTERNAL_DATA like '%,TS,%'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");
} 
double totalTR ()
{
return db.sum("REFUND", "total", "EXTERNAL_DATA like '%,TR,%'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");
} 
int numberPS ()
{
return db.count("BON_PROFORMA", "ID_PROFORMA", "EXTERNAL_DATA like '%,PS,%'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");
}
double totalPS ()
{
return db.sum("BON_PROFORMA", "total", "EXTERNAL_DATA like '%,PS,%'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");
}  
double totalSalesCash()// amount for all sales receipts labeled as NS, including tax;
{
return db.sum("invoice", "cash", "EXTERNAL_DATA like '%,NS,%' ",
  "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");
} 
double totalSalesCredit()// amount for all sales receipts labeled as NS, including tax;
{
return db.sum("invoice", "CREDIT", "EXTERNAL_DATA like '%,NS,%' ",
  "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");
}
double totalRefundNRCash()// amount for all sales receipts labeled as NS, including tax;
{
return db.sum("REFUND", "cash", "EXTERNAL_DATA like '%,NR,%' ",
  "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");
} 
double totalRefundNRCredit()// amount for all sales receipts labeled as NS, including tax;
{
return db.sum("REFUND", "CREDIT", "EXTERNAL_DATA like '%,NR,%' ",
  "HEURE >'"+hera+"' AND HEURE <'"+geza+"'");
} 
double totaDiscounts()  
{
return db.sum("invoice", "REDUCTION", " HEURE >'"+hera+"' ",
  "  HEURE <'"+geza+"'");
}  
//xix. other registrations that have reduced the day's sales and their amount;
int numberIncompleteSales()
{return
db.count("invoice", "ID_INVOICE", "EXTERNAL_DATA = 'EMPTY'", "HEURE >'"+hera+"' AND HEURE <'"+geza+"'")
        ;
}
 String setVirgule(double frw)
    {
        String setString="";
        if(frw>99999)
        {
            BigDecimal big=new BigDecimal(frw);
            int value=big.intValue();
            float dec=(float)(frw-value);
            
            StringTokenizer st1=new StringTokenizer (""+dec);
            st1.nextToken(".");
            
            String decimal=st1.nextToken("").replace(".", "");
            
            if(decimal.length()==1 && decimal.equals("0") )
                decimal=".00";
            else if (decimal.length()==1 && !decimal.equals("0") )
                decimal="."+decimal+"0";
            else
                decimal="."+decimal.substring(0, 2);
            
            setString = ""+value;  
            
            String toret=setVirgule(setString)+decimal;
            return toret;
        }
        else
        {
            StringTokenizer st1=new StringTokenizer (""+frw);
            String entier =st1.nextToken(".");
            //System.out.println(frw);
            String decimal=st1.nextToken("").replace(".", "");
            
            if(decimal.length()==1 && decimal.equals("0") )    
                decimal=".00";
                    
            else if (decimal.length()==1 && !decimal.equals("0") )
                decimal="."+decimal+"0";
            else
                decimal="."+decimal.substring(0, 2);
            String toret=setVirgule(""+entier)+decimal; 
            return toret;
        }
         
}

 String setVirgule(String frw)
   {
       String setString="";
       int k=0;
       for(int i=(frw.length()-1);i>=0;i--)
       {
           k++;
           if((k-1)%3==0 && (k-1)!=0)
           {
               setString+=",";
           }
           setString+=frw.charAt(i);
       }
       return inverse(setString);
   }
   
   String inverse(String frw)
   {
       String l="";
       for(int i=(frw.length()-1);i>=0;i--)
       {
           l+=frw.charAt(i);
       }
       return l;
   }


String setVirguleDouble1(double dou)
{
return " "+dou;
}
String setVirguleInt(int in)
{
return " "+in;
}
LinkedList <RRA_PRINT2_LINE> toPrint()
{
  //  daily report hera,geza,tradename,tin,CIS,MRC 
    
LinkedList <RRA_PRINT2_LINE> toPrint = new LinkedList();
Font f = new Font("Arial", Font.BOLD, 12 );
 
toPrint.add(new RRA_PRINT2_LINE(" X DAILY REPORT ", "", f)); 
toPrint.add(new RRA_PRINT2_LINE(" DATE : "+hera.substring(0, 11), "", f));  
toPrint.add(new RRA_PRINT2_LINE(" TRADE NAME :"+tradename, "", f)); 
toPrint.add(new RRA_PRINT2_LINE(" TIN  : "+tin, "", f)); 
toPrint.add(new RRA_PRINT2_LINE(" MRC : "+MRC, "", f));  
toPrint.add(new RRA_PRINT2_LINE(" CIS  : "+CIS, "", f));  
toPrint.add(new RRA_PRINT2_LINE(" TAXE RATE A :"+RRA_PRINT2.setVirgule(tvaRateA,1,0.005), "", f)); 
toPrint.add(new RRA_PRINT2_LINE(" TAXE RATE B :"+RRA_PRINT2.setVirgule(tvaRateB,1,0.005), "", f)); 
toPrint.add(new RRA_PRINT2_LINE(" TAXE RATE C :"+RRA_PRINT2.setVirgule(tvaRateC,1,0.005), "", f)); 
toPrint.add(new RRA_PRINT2_LINE(" TAXE RATE D :"+RRA_PRINT2.setVirgule(tvaRateD,1,0.005), "", f)); 
f = new Font("Arial", Font.PLAIN, 10 );
toPrint.add(new RRA_PRINT2_LINE(" -------- SALES -----------   ", "", f));
toPrint.add(new RRA_PRINT2_LINE("   ", "", f));
toPrint.add(new RRA_PRINT2_LINE("TOTAL SALES NS ", setVirgule(totalSalesNS()), f)); 
LinkedList<String> clients=db.clients ();
for(int i=0;i<clients.size();i++)
{
    String clie=clients.get(i);
    double tot=totalSalesNSGoupe(clie);
    if(tot!=0)
    {toPrint.add(new RRA_PRINT2_LINE("TOTAL GROUPE NS "+clie,
            setVirgule(tot), f));}
}
 
toPrint.add(new RRA_PRINT2_LINE("NUMBER SALES NS ", setVirguleInt(numberSalesNS()), f));
toPrint.add(new RRA_PRINT2_LINE("TOTAL COPY SALE ", setVirgule(totalCS()), f));
toPrint.add(new RRA_PRINT2_LINE("NUMBER COPY SALE ", setVirguleInt(numberCS()), f));
toPrint.add(new RRA_PRINT2_LINE("TOTAL SALES CASH ", setVirgule(totalSalesCash()), f));
toPrint.add(new RRA_PRINT2_LINE("TOTAL SALES CREDIT ", setVirgule(totalSalesCredit()), f));
toPrint.add(new RRA_PRINT2_LINE("TOTAL TRAINING SALES ", setVirgule(totalTS ()), f));
toPrint.add(new RRA_PRINT2_LINE("NUMBER TRAINING SALES ", setVirguleInt(numberTS ()), f)); 
toPrint.add(new RRA_PRINT2_LINE("TOTAL PROFORMA SALES", setVirgule(totalPS ()), f));
toPrint.add(new RRA_PRINT2_LINE("NUMBER PROFORMA SALES ", setVirguleInt(numberPS ()), f)); 
toPrint.add(new RRA_PRINT2_LINE("   ", "", f));
toPrint.add(new RRA_PRINT2_LINE("-------- REFUND -----------   ", "", f));
toPrint.add(new RRA_PRINT2_LINE("   ", "", f));
toPrint.add(new RRA_PRINT2_LINE("TOTAL REFUND NR ", setVirgule(totalRefundNR()), f)); 
toPrint.add(new RRA_PRINT2_LINE("NUMBER REFUND NR ", setVirguleInt(numberRefundNR()), f));
toPrint.add(new RRA_PRINT2_LINE("TOTAL COPY REFUND ", setVirgule(totalCR()), f));
toPrint.add(new RRA_PRINT2_LINE("NUMBER COPY REFUND ", setVirguleInt(numberCR()), f));
toPrint.add(new RRA_PRINT2_LINE("TOTAL REFUND CASH ", setVirgule(totalRefundNRCash()), f));
toPrint.add(new RRA_PRINT2_LINE("TOTAL REFUND CREDIT ", setVirgule(totalRefundNRCredit()), f));  
toPrint.add(new RRA_PRINT2_LINE("TOTAL TRAINING REFUND ", setVirgule(totalTR ()), f));
toPrint.add(new RRA_PRINT2_LINE("NUMBER TRAINING REFUND ", setVirguleInt(numberTR ()), f));
 
toPrint.add(new RRA_PRINT2_LINE("   ", "", f));
toPrint.add(new RRA_PRINT2_LINE("--------- TAXE ----------   ", "", f));
toPrint.add(new RRA_PRINT2_LINE("   ", "", f));
toPrint.add(new RRA_PRINT2_LINE("TOTAL TAXABLE  A ", setVirgule(taxableAmounts("TOTAL_A")), f));   
toPrint.add(new RRA_PRINT2_LINE("TOTAL TAXABLE  B ", setVirgule(taxableAmounts("TOTAL_B")), f));   
toPrint.add(new RRA_PRINT2_LINE("TOTAL TAXABLE  C ", setVirgule(taxableAmounts("TOTAL_C")), f));   
toPrint.add(new RRA_PRINT2_LINE("TOTAL TAXABLE  D ", setVirgule(taxableAmounts("TOTAL_D")), f));   
 
toPrint.add(new RRA_PRINT2_LINE("TOTAL TAXE  A ", setVirgule(taxAmounts("TAXE_A")), f));   
toPrint.add(new RRA_PRINT2_LINE("TOTAL TAXE  B ", setVirgule(taxAmounts("TAXE_B")), f));   
toPrint.add(new RRA_PRINT2_LINE("TOTAL TAXE  C ", setVirgule(taxAmounts("TAXE_C")), f));   
toPrint.add(new RRA_PRINT2_LINE("TOTAL TAXE  D ", setVirgule(taxAmounts("TAXE_D")), f));   
 toPrint.add(new RRA_PRINT2_LINE("   ", "", f));
toPrint.add(new RRA_PRINT2_LINE(" --------- # --------------  ", "", f));
toPrint.add(new RRA_PRINT2_LINE("   ", "", f));
toPrint.add(new RRA_PRINT2_LINE("OPENING DEPOSIT ", setVirgule(login("IN")), f)); 
toPrint.add(new RRA_PRINT2_LINE("DEPOSIT ", setVirgule(login("DEPOSIT")), f)); 
toPrint.add(new RRA_PRINT2_LINE("WITHDRAW ", setVirgule(login("RETRAIT")), f)); 
toPrint.add(new RRA_PRINT2_LINE("CLOSING ", setVirgule(login("OUT")), f)); 
toPrint.add(new RRA_PRINT2_LINE("NUMBER OF ITEM SOLD ", setVirguleInt(numberItemsSold()), f));
toPrint.add(new RRA_PRINT2_LINE("TOTAL DISCOUNT ", setVirgule(totaDiscounts() ), f)); 
toPrint.add(new RRA_PRINT2_LINE("NUMBER INCOMPLETE SALES ", setVirguleInt(numberIncompleteSales()), f)); 
 
return toPrint;
}

}
