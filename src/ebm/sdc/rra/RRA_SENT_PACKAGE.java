/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ebm.sdc.rra;

import java.util.StringTokenizer;

/**
 *
 * @author Kimenyi
 */
    public class RRA_SENT_PACKAGE {

//Messages from the CIS to the SDC
//01,LEN,SEQ,CMD,DATA,05,BCC,03,   
//String S01,len,seq,cmd,data,S05,bcc,S03; KUMENEKA
byte [] tosent; 
String client_TIN;   
String  MRC,TIN  ; 
public RRA_SENT_PACKAGE(String MRC,String TIN) 
{
this.MRC = MRC;
this.TIN = TIN;   
}  
    public byte [] transform( byte seq,byte cmd,byte [] data)
    {
        int sizeData=data.length;
         
        int sizeArray=10+sizeData;
        byte [] resultat = new byte [sizeArray];
        resultat [0]=0x01; 
        resultat [1]=(byte)(0x23 + (new Integer (sizeData+1)).byteValue());
        resultat [2]=seq;
        resultat [3]=cmd;
        System.arraycopy(data, 0, resultat, 4, sizeData);
        resultat [4+sizeData]=0x05; 
        System.arraycopy(bcc(resultat,4+sizeData+1), 0, resultat, 5+sizeData, 4);
        resultat [sizeArray-1]=0x03; 
       // System.err.println(resultat.length+"size:"+resultat [sizeArray-1]);
        return resultat;
    }
  
    int HexDec(String hex)
    {
        if(hex!=null && hex.length()==2)
        {
            int uno=0;
            int dos=0;
            
            if(hex.charAt(0)=='A')
            uno=10;
            else if(hex.charAt(0)=='B')
            uno=11;
            else if(hex.charAt(0)=='C')
            uno=12;
            else if(hex.charAt(0)=='D')
            uno=13;
            else if(hex.charAt(0)=='E')
            uno=14;
            else if(hex.charAt(0)=='F')
            uno=15;
            else
            uno=Integer.parseInt(""+hex.charAt(0));
            
            if(hex.charAt(1)=='A')
            dos=10;
            else if(hex.charAt(1)=='B')
            dos=11;
            else if(hex.charAt(1)=='C')
            dos=12;
            else if(hex.charAt(1)=='D')
            dos=13;
            else if(hex.charAt(1)=='E')
            dos=14;
            else if(hex.charAt(1)=='F')
            dos=15;
             else
                dos=Integer.parseInt(""+hex.charAt(1));
                
            
            return (uno*16)+(dos);
        }
         else
        {
            System.err.println("Fatal Error HexDec "+hex);
            return -1;
        }
         
        
    }
        
public byte [] bcc( byte []b, int stop)
{
    int sum=0;
    
    for(int i=1;i<stop;i++)
    {
//       System.out.print("  ["+b[i] +"!"+String.format("%02x", b[i]&0xff)+"#"+
//               HexDec((String.format("%02x", b[i]&0xff)).toUpperCase()) +" ]");   
//       
       sum+= HexDec((String.format("%02x", b[i]&0xff)).toUpperCase()); 
    }
    //System.out.println("sum:"+String.format("%04x", sum&0xffff));
    String sss=String.format("%04x", sum&0xffff).toUpperCase();
byte one=0x30;
byte two=0x30;
byte tre=0x30;
byte four=0x30;

     if(sss.charAt(0)=='A')
         one=0x3A;
     else if(sss.charAt(0)=='B')
         one=0x3B;
     else if(sss.charAt(0)=='C')
         one=0x3C;
     else if(sss.charAt(0)=='D')
         one=0x3D;
     else if(sss.charAt(0)=='E')
         one=0x3E;
     else if(sss.charAt(0)=='F')
         one=0x3F;     
     else if(sss.charAt(0)=='1')
         one=0x31;
     else if(sss.charAt(0)=='2')
         one=0x32;
     else if(sss.charAt(0)=='3')
         one=0x33;
     else if(sss.charAt(0)=='4')
         one=0x34;
     else if(sss.charAt(0)=='5')
         one=0x35;
     else if(sss.charAt(0)=='6')
         one=0x36;
     else if(sss.charAt(0)=='7')
         one=0x37;
     else if(sss.charAt(0)=='8')
         one=0x38;
     else if(sss.charAt(0)=='9')
         one=0x39; 
     else
         one=0x30;
     
     if(sss.charAt(1)=='A')
         two=0x3A;
     else if(sss.charAt(1)=='B')
         two=0x3B;
     else if(sss.charAt(1)=='C')
         two=0x3C;
     else if(sss.charAt(1)=='D')
         two=0x3D;
     else if(sss.charAt(1)=='E')
         two=0x3E;
     else if(sss.charAt(1)=='F')
         two=0x3F;     
     else if(sss.charAt(1)=='1')
         two=0x31;
     else if(sss.charAt(1)=='2')
         two=0x32;
     else if(sss.charAt(1)=='3')
         two=0x33;
     else if(sss.charAt(1)=='4')
         two=0x34;
     else if(sss.charAt(1)=='5')
         two=0x35;
     else if(sss.charAt(1)=='6')
         two=0x36;
     else if(sss.charAt(1)=='7')
         two=0x37;
     else if(sss.charAt(1)=='8')
         two=0x38;
     else if(sss.charAt(1)=='9')
         two=0x39; 
     else
         two=0x30;
     
     
     if(sss.charAt(2)=='A')
         tre=0x3A;
     else if(sss.charAt(2)=='B')
         tre=0x3B;
     else if(sss.charAt(2)=='C')
         tre=0x3C;
     else if(sss.charAt(2)=='D')
         tre=0x3D;
     else if(sss.charAt(2)=='E')
         tre=0x3E;
     else if(sss.charAt(2)=='F')
         tre=0x3F;     
     else if(sss.charAt(2)=='1')
         tre=0x31;
     else if(sss.charAt(2)=='2')
         tre=0x32;
     else if(sss.charAt(2)=='3')
         tre=0x33;
     else if(sss.charAt(2)=='4')
         tre=0x34;
     else if(sss.charAt(2)=='5')
         tre=0x35;
     else if(sss.charAt(2)=='6')
         tre=0x36;
     else if(sss.charAt(2)=='7')
         tre=0x37;
     else if(sss.charAt(2)=='8')
         tre=0x38;
     else if(sss.charAt(2)=='9')
         tre=0x39; 
     else
         tre=0x30;
     
     if(sss.charAt(3)=='A')
         four=0x3A;
     else if(sss.charAt(3)=='B')
         four=0x3B;
     else if(sss.charAt(3)=='C')
         four=0x3C;
     else if(sss.charAt(3)=='D')
         four=0x3D;
     else if(sss.charAt(3)=='E')
         four=0x3E;
     else if(sss.charAt(3)=='F')
         four=0x3F;     
     else if(sss.charAt(3)=='1')
         four=0x31;
     else if(sss.charAt(3)=='2')
         four=0x32;
     else if(sss.charAt(3)=='3')
         four=0x33;
     else if(sss.charAt(3)=='4')
         four=0x34;
     else if(sss.charAt(3)=='5')
         four=0x35;
     else if(sss.charAt(3)=='6')
         four=0x36;
     else if(sss.charAt(3)=='7')
         four=0x37;
     else if(sss.charAt(3)=='8')
         four=0x38;
     else if(sss.charAt(3)=='9')
         four=0x39; 
     else
         four=0x30;  
     
return new byte []{one,two,tre,four};
}
 public byte [] convertCis( byte seq, byte cmd, byte [] data ) { 
  return transform( seq, cmd,data);
 }  
//<Rtype><TType><MRC>,<TIN>,<Date><Space><Time>,<RNum>,<
//TaxRate1>,<
//TaxRate2>,<TaxRate3>,<TaxRate4>,<Amount1>,<Amount2>,<Amo
//unt3>,<Amount4>,<Tax1>,<Tax2>,<Tax3>,<Tax4>[,<ClientsTin>]


public byte [] rulestoSend( byte seq, String CMD,String toSend) {
    
//229 (E5h) SDC ID REQUEST NO DATA
if(CMD.equals("ID REQUEST"))
{
return transform(seq, (byte)0xE5, new byte []{});
}
//198 (C6h) RECEIPT DATA  VIEW GETSALE
else if(CMD.equals("SEND RECEIPT"))
{
//  return transform(seq, (byte)0xC6, getSaleString().getBytes()); 
return transform(seq, (byte)0xC6, toSend.getBytes());
}
//200 (C8h) SDC RESPONSE  <RNumber>
else  if(CMD.equals("RESPONSE"))
{
//    return transform(seq, (byte)0xC8,(""+rp.id_invoice).getBytes());
return transform(seq, (byte)0xC8,toSend.getBytes());
}
//199 (C7h) SIGNATURE REQUEST  <Type><RNumber>
else if(CMD.equals("SIGNATURE REQUEST"))
{
return transform(seq, (byte)0xC7, new byte []{0x31,0x31});
}
//201 (C9h) COUNTERS REQUEST <RNumber>
else if(CMD.equals("COUNTERS REQUEST"))
{
return transform(seq, (byte)0xC9, new byte []{0x31});
}
//238 (EEh) EJ DATA <LType><String>
else if(CMD.equals("EJ DATA"))
{
return transform(seq, (byte)0xEE, toSend.getBytes());
} 
//62 (3Eh) SDC DATE AND TIME REQUEST NO DATA
else if(CMD.equals("DATE AND TIME REQUEST"))
{
return transform(seq, (byte)0x3E, new byte []{});
}
else
return null;
}

//public byte [] convert( byte seq) { 
//    ///  return ""+S01+","+len+","+seq+","+cmd+","+data+","+S05+","+bcc+","+S03+", ";  
//    //return "01,03,20h,3Eh,,05,30h,03"; 
//  
////     return new byte [] {0x01,0x25,0x23,(byte)0xC6,0x31,0x05,0x30,0x31,0x34,0x37,0x03}; //CIS INVOICE REQUEST
//    // return new byte [] {0x01,0x24,0x20,(byte)0x3E,0x05,0x30,0x30,0x38,0x37,0x03}; //DATE AND TIME REQUEST
////     return new byte [] {0x01,0x25,0x27,(byte)0xC8,0x31,0x05,0x30,0x31,0x34,0x3A,0x03}; //SDC RESPONSE
//     //return new byte [] {0x01,0x26,0x28,(byte)0xC7,0x52,0x31,0x05,0x30,0x31,0x39,0x3D,0x03}; //SDC SIGNATURE
//     
//   //return new byte [] {0x01,0x24,0x20,(byte)0xE5,0x05,0x30,0x31,0x32,0x3e,0x03}; //SDC SERIAL NUMBER
//   // return new byte [] {0x01,0x25,0x20,(byte)0xC8,0x32,0x05,0x30,0x31,0x32,0x3e,0x03}; 
//   
//     
////byte [] bite= new byte []   
////{
//// 0x01,(byte)0x90,0x22,(byte)0xc6,(byte)0x54,0x53, 0x54, 0x45, 0x53, 0x30, 0x31, 0x30, 0x31, 0x32, 0x33, 0x34, 
////0x35, 0x2C, 0x31, 0x30, 0x30, 0x36, 0x30, 0x30, 0x35, 0x37, 0x30, 0x2C, 0x31, 0x37, 0x2F, 0x30, 
////0x37, 0x2F, 0x32, 0x30, 0x31, 0x33, 0x20, 0x30, 0x39, 0x3A, 0x32, 0x39, 0x3A, 0x33, 0x37, 0x2C, 
////0x31, 0x2C, 0x30, 0x2E, 0x30, 0x30, 0x2C, 0x31, 0x38, 0x2E, 0x30, 0x30, 0x2C, 0x30, 0x2E, 0x30, 
////0x30, 0x2C, 0x30, 0x2E, 0x30, 0x30, 0x2C, 0x31, 0x31, 0x2E, 0x30, 0x30, 0x2C, 0x31, 0x32, 0x2E, 
////0x30, 0x30, 0x2C, 0x30, 0x2E, 0x30, 0x30, 0x2C, 0x30, 0x2E, 0x30, 0x30, 0x2C, 0x30, 0x2E, 0x30, 
////0x30, 0x2C, 0x31, 0x2E, 0x38, 0x33, 0x2C, 0x30, 0x2E, 0x30, 0x30, 0x2C, 0x30, 0x2E, 0x30, 0x30, 
////0x05, 0x31, 0x36, 0x37, 0x3F, 0x03
////
//////0X01, (byte)0X82, 0X23, (byte)0XC8, 0X53, 0X44, 0X43, 0X30, 0X30, 0X31, 0X30, 0X30, 0X30, 0X30, 0X34, 0X34
//////, 0X2C, 0X32, 0X38, 0X34, 0X2C, 0X33, 0X33, 0X31, 0X2C, 0X4E, 0X53, 0X2C, 0X31, 0X37, 0X2F, 0X30
//////, 0X37, 0X2F, 0X32, 0X30, 0X31, 0X33, 0X20, 0X30, 0X39, 0X3A, 0X33, 0X30, 0X3A, 0X34, 0X38, 0X2C
//////, 0X53, 0X4B, 0X41, 0X41, 0X53, 0X4B, 0X34, 0X51, 0X4F, 0X50, 0X4D, 0X33, 0X55, 0X36, 0X42, 0X59
//////, 0X2C, 0X55, 0X43, 0X33, 0X4A, 0X4F, 0X58, 0X51, 0X59, 0X49, 0X4C, 0X4F, 0X59, 0X52, 0X54, 0X4D
//////, 0X41, 0X47, 0X54, 0X4C, 0X34, 0X57, 0X4E, 0X48, 0X37, 0X4B, 0X49, 0X04, 0X50, 0X30, 0X30, 0X30
//////, 0X30, 0X30, 0X05, 0X31, 0X38, 0X31, 0X38, 0X03
////};
//// 
//byte [] igisum=convertCIS(seq);
//
//// System.out.println(bite.length+" * "+igisum.length);
////for(int i=0;i<bite.length;i++)
////{
////    if(igisum[i]==bite[i])
////    System.out.print(i+" * "+igisum[i]+"  "+bite[i]);
////    else
////    System.out.print(i+" ----------------------- "+igisum[i]+"  "+bite[i]);    
////    
////    System.out.println();
////}
//
////System.out.println(bite.length+" * "+igisum.length);
////for(int i=0;i<igisum.length;i++)
////{ 
////    System.out.print(i+" -------- "+igisum[i]+"---------"+String.format("%02x", igisum[i]&0xffff).toUpperCase());    
////    
////    System.out.println();
////}
//
//return igisum;   
//}



String bty(byte [] a)
{
    String ret="";
    for(int i=0;i<a.length;i++)
    {
      ret+=String.format("%02x ", a[i]&0xff);  
    }
    return ret;
    
}
        
 
}
