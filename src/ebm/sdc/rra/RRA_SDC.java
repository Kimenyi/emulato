/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ebm.sdc.rra;

import gnu.io.CommPortIdentifier;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.UnsupportedCommOperationException;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;  
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Date;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;    
 
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingWorker;

/**
 *
 * @author Kimenyi
 */
public class RRA_SDC {
static Enumeration	      portList;
static Font police;
static CommPortIdentifier portId;
static String	      messageString = " ";
static SerialPort	      serialPort;
static OutputStream       outputStream;
InputStream inputStream;
static boolean	      outputBufferEmptyFlag = false;
static String  defaultPort="COM21"; 
public final String softwareCode="001";
 
static public String  MRC, TIN,  SDC,COMPANY,CITY,ADRESS,VERSION;
RRA_SENT_PACKAGE cis=null;
public LinkedList <Byte> getResponse ;
static public Integer received=0;
public Connection conn=null;

static public LinkedList<RRA_COMMAND> queu=new LinkedList<RRA_COMMAND>();
static public  boolean sdcIsConnected=false;
static public int currentQueuId=0;  
static public int timeToSleep=1000;
static public int timeToWait=0;
static public int  busyTime=200;
static public int bits=115200;
public RRA_SDC(String TIN, String MRC,String COMPANY,String CITY,String ADRESS,String VERSION,
      Connection conn,
        String employe,String defaultPort)
{
RRA_SDC.TIN = TIN;
RRA_SDC.MRC = MRC; 
RRA_SDC.COMPANY = COMPANY;
RRA_SDC.ADRESS = ADRESS;
RRA_SDC.CITY = CITY;
RRA_SDC.VERSION=VERSION;

this.cis=new RRA_SENT_PACKAGE( MRC,TIN); 
this.conn=conn;
this.defaultPort=defaultPort;
sdcIsConnected=portConnection();
System.out.println("SDC CONNECTION:"+sdcIsConnected);
if(sdcIsConnected)
{
   
    RRA_RECEIVED_PACKAGE sd=sendToSDC3((byte)0x27,"ID REQUEST","",employe );
    if(sd!=null && sd.data.length()>3)
    {
        SDC= sd.data;
    }
    else
    {
        sdcIsConnected=false;
    }
}  
 
}  
public RRA_RECEIVED_PACKAGE sendToSDC3(byte seq,String choix,String toSend,String employe)
{ 

byte [] packToSent=cis.rulestoSend(seq,choix,toSend);
//for(int i=0;i<packToSent.length;i++)

//System.out.print (" " +packToSent[i] );

LinkedList <Byte> comm=comm(packToSent,choix,employe,conn);
System.out.println((new Date()).getTime());
RRA_RECEIVED_PACKAGE rec= new RRA_RECEIVED_PACKAGE(comm);
if(rec==null || rec.data==null)
Rra.textArea.setForeground(Color.red);
else
Rra.textArea.setForeground(Color.black);

return rec;
}
public RRA_RECEIVED_PACKAGE sendToSDC4(String choix,String toSend,String employe,byte seq)
{ 

byte [] packToSent=cis.rulestoSend(seq,choix,toSend);
//System.out.println((new Date()).getTime());
LinkedList <Byte> comm=comm2(packToSent,choix,employe,conn);
//System.out.println((new Date()).getTime());
RRA_RECEIVED_PACKAGE rec= new RRA_RECEIVED_PACKAGE(comm);
if(rec==null || rec.data==null)
Rra.textArea.setForeground(Color.red);
else
Rra.textArea.setForeground(Color.black);

return rec;
}

public LinkedList <Byte> comm(byte [] sendIt,String choix,String employe,Connection conn)
{
    getResponse = new LinkedList(); 
    received=0;
 
    if(serialPort!=null)
    { try { 
        
    outputStream.write(sendIt); 
    Thread t = new Thread(); 
    System.out.println("receiving ") ;
    inputStream = serialPort.getInputStream();
    
//////////////////////////////////////////////////////////////////////////////////////////////////////   

/** Create a SwingWorker instance to run a compute-intensive task */
final SwingWorker<LinkedList <Byte>, String> worker = new SwingWorker<LinkedList <Byte>, String>() 
{
/** Schedule a compute-intensive task in a background thread */
@Override
protected LinkedList <Byte> doInBackground() throws Exception {
    
//    System.out.println("IN  "+(new Date()).getTime());
while(received!=03)
{  
received=inputStream.read();
getResponse.add(received.byteValue()); 
if(received==22)
{ timeToWait+=busyTime; }

  //System.out.print(" kk  "  +received); 
} 
//    System.out.println("OUT  "+(new Date()).getTime());
return getResponse;
} 
/** Run in event-dispatching thread after doInBackground() completes */
@Override
protected void done() {
try { 
 inputStream.close();
} catch (IOException ex) {
 Logger.getLogger(RRA_SDC.class.getName()).log(Level.SEVERE, null, ex);
}
} 
/** Run in event-dispatching thread to process intermediate results
send from publish(). */
@Override
protected void process(java.util.List<String> chunks) {
// Get the latest result from the list
String latestResult = chunks.get(chunks.size() - 1);
System.out.println("Result is " + latestResult); 
}
};

worker.execute();  // start the worker thread
System.out.println("Running " );
/** Event handler for the PropertyChangeEvent of property "progress" */
worker.addPropertyChangeListener(new PropertyChangeListener() {
@Override
public void propertyChange(PropertyChangeEvent evt) {
if (evt.getPropertyName().equals("progress")) {  // check the property name
//pbWorker.setValue((Integer)evt.getNewValue());  // update progress bar
}
}
});
 
    //////////////////////////////
System.out.println(received+"-------------------------------------------timeToSleep "+timeToSleep) ;
t.sleep(timeToSleep);

if(received!=03 && timeToWait>0 )
{
insertTrackRRA(new RRA_TRACK(0, "", byteArrayToString(sendIt), 
              byteListToString(getResponse), " BUSY "+timeToWait, employe),conn);    
t.sleep(timeToWait); 
}

System.out.println(received+"-------------------------------------------timeToSleep "+timeToSleep) ;
timeToWait=0;
if(received!=03)
{
//     for(int i=0;i<getResponse.size();i++) 
//System.out.println (" - " +getResponse.get(i) );
//     
getResponse = new LinkedList();
received=01;

getResponse.add(received.byteValue()); 
worker.cancel(true);
return getResponse;
}

if(!choix.equals("EJ DATA") || !choix.equals("ID REQUEST"))
{insertTrackRRA(new RRA_TRACK(0, "", byteArrayToString(sendIt), 
              byteListToString(getResponse), choix, employe),conn);}
    return getResponse;
} 
        catch (Throwable ex) {
//            Rra.textArea.setFont(font);
            Rra.textArea.setForeground(Color.red);
             Rra.textArea.append("\n comm() "+ex);             
             sdcIsConnected=false;
//             serialPort.close();
//             Thread t = new Thread();
//             while(!sdcIsConnected)
//             {
//                   System.out.println("BACK:"+sdcIsConnected);
//                 sdcIsConnected=portReConnection();
//                 System.out.println("BACK:"+sdcIsConnected);
//                    try {
//                        t.sleep(timeToSleep);
//                        if(sdcIsConnected)
//                    return comm(sendIt, choix, employe, conn);
//                    } catch (InterruptedException ex1) {
//                        Logger.getLogger(RRA_SDC.class.getName()).log(Level.SEVERE, null, ex1);
//                    }
//                 
//             }
             return null;
           // JOptionPane.showMessageDialog(null, ""+ex, "MESSAGE",JOptionPane.ERROR_MESSAGE);
        }
        }  
    else
     {sdcIsConnected=false;}
    
    return null;
} 
 
public LinkedList <Byte> comm2(byte [] sendIt,String choix,String employe,Connection conn)
{
    getResponse = new LinkedList(); 
    received=0;
 // Date d1=new Date();
    if(serialPort!=null)
    { try { 
        
    outputStream.write(sendIt);  
//    System.out.println("receiving ") ;
    inputStream = serialPort.getInputStream();
    while(received!=03)
{  
received=inputStream.read();
getResponse.add(received.byteValue()); 
 //System.out.println(" "+String.format("%02x ", received&0xff)); 
}
     inputStream.close();
} catch (IOException ex) {
 Logger.getLogger(RRA_SDC.class.getName()).log(Level.SEVERE, null, ex);
}
} 
// Date d2=new Date();
// long ti=d2.getTime()-d1.getTime();
//  System.out.println(d1+" "+d2+" "+ti); 
  return getResponse; 
} 
 
boolean insertTrackRRA( RRA_TRACK rt, Connection conn)
 {  
     try
     {  
        PreparedStatement psInsert = conn.prepareStatement("insert into APP.RRA_TRACKING_COMM"
                 + " (SENT_TRACKING,RECEIVED_TRACKING ,KEY_INVOICE,EMPLOYE)  values (?,?,?,?)");
         
         psInsert.setString(1,rt.SENT_TRACKING);
         psInsert.setString(2,rt.RECEIVED_TRACKING);
         psInsert.setString(3,rt.KEY_INVOICE);
         psInsert.setString(4,rt.EMPLOYE);
         psInsert.executeUpdate();
         psInsert.close();
         return true;
    }  
 catch (Throwable e)  
 {
     writeError(e+" KEY "+rt.KEY_INVOICE,"insertTrackRRA FAIL");
     //insertError(e+"","insertTrackRRA");
 } 
 return false;
} 
void writeError(String desi,String ERR)
    {
try {
String  file="ERROR/ rra.txt";
File f = new File(file); 
PrintWriter sorti=new PrintWriter(new FileWriter(f));
sorti.println(desi+ERR);
sorti.close();  
 Rra.textArea.append(" writeError "+desi); 
} catch (Throwable ex) {
Rra.textArea.append(" writeError "+desi+"  CONTACT ISHYIGA TEAM ");     
//FUNGA FATAL ERROR
}
}
String byteArrayToString(byte [] bite)
{
String res="";
for(int i=0;i<bite.length;i++)
{
res+=";"+bite[i];
}
return res;

}
String byteListToString(LinkedList <Byte> bite)
{
String res="";
for(int i=0;i<bite.size();i++)
{
res+=";"+bite.get(i);
}
return res;

}
boolean portConnection()
{
    
boolean portFound = false; 
 LinkedList<String> port=new LinkedList<String>();
serialPort=null;
outputStream=null;
inputStream=null;
portList = CommPortIdentifier.getPortIdentifiers();
//System.out.println(portList.hasMoreElements());
while (portList.hasMoreElements()) { 
portId = (CommPortIdentifier) portList.nextElement(); 
System.out.println("LIST : " + portId.getName()  );
if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL) 
{
                port.add(portId.getName());
System.out.println("LIST : " + portId.getName()  );
if (portId.getName().equals(defaultPort)) {
System.out.println("Found port " + defaultPort);
  Rra.textArea.setForeground(Color.green);
Rra.textArea.append("\n Found port " + defaultPort);
  Rra.textArea.setForeground(Color.black);
portFound = true; 
try {
serialPort = 
(SerialPort) portId.open("SDC", 2000);
Rra.textArea.setForeground(Color.black);
} catch (PortInUseException e) {
System.out.println("Port in use.");
Rra.textArea.append("\n Port in use " + defaultPort);
 Rra.textArea.setForeground(Color.orange);
continue;
}   
try {outputStream = serialPort.getOutputStream();
} catch (IOException e) {
    Rra.textArea.append(" PORT IO "+e);
    Rra.textArea.setForeground(Color.red);
}
try {
serialPort.setSerialPortParams(bits, 
SerialPort.DATABITS_8, 
SerialPort.STOPBITS_1, 
SerialPort.PARITY_NONE);
} catch (UnsupportedCommOperationException e) {} 
try {
serialPort.notifyOnOutputEmpty(true);
} catch (Exception e) {
     Rra.textArea.append(" PORT Exception "+e+" Error setting event notification ");
 Rra.textArea.setForeground(Color.red);
}  	  
		} 
	    } 

	} 
	if (!portFound) 
        {
             JPanel pane=new JPanel(); 
             LinkedList < JRadioButton> jcheck=new LinkedList(); 
             pane.setLayout(new GridLayout(port.size(),1)); 
             for(int i=0;i<port.size();i++)
             {
                 String oneport=port.get(i);
                 JRadioButton now=new JRadioButton(oneport);
                 jcheck.add(now);
                 pane.add(now); 
             }
	    System.out.println("port " + defaultPort + " not found.");
              Rra.textArea.setForeground(Color.red);
            Rra.textArea.append("\n port " + defaultPort + " not found. RECONNECTING");
 JOptionPane.showMessageDialog(null, pane,"Port " + defaultPort + " not found.",JOptionPane.WARNING_MESSAGE);  
    
//      Rra.textArea.setForeground(Color.black);
            for(int i=0;i<port.size();i++)
            {
                if(jcheck.get(i).isSelected())
                { 
                    defaultPort=port.get(i);
                    return portConnection();
                }
            }
//             JOptionPane.showMessageDialog(null, "PORT " + defaultPort + " not found.", "", JOptionPane.WARNING_MESSAGE);
			
	}
	 
return portFound;         
}

boolean portReConnection()
{
boolean portFound = false; 
 LinkedList<String> port=new LinkedList<String>();

	 portList = CommPortIdentifier.getPortIdentifiers();
//System.out.println(portList.hasMoreElements());
	while (portList.hasMoreElements()) { 
	    portId = (CommPortIdentifier) portList.nextElement(); 
	    if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL) 
            {
                port.add(portId.getName());
System.out.println("LIST : " + portId.getName()  );
if (portId.getName().equals(defaultPort)) {
System.out.println("Found port " + defaultPort);
  Rra.textArea.setForeground(Color.green);
Rra.textArea.append("\n Found port " + defaultPort);
  Rra.textArea.setForeground(Color.black);
  

try {
    
serialPort = 
(SerialPort) portId.open("SDC", 2000);
Rra.textArea.setForeground(Color.black);
portFound = true; 
} catch (PortInUseException e) {
System.out.println("Port in use.");
Rra.textArea.append("\n Port in use " + defaultPort);
 Rra.textArea.setForeground(Color.orange);
continue;
}   
try {outputStream = serialPort.getOutputStream();
} catch (IOException e) {
    Rra.textArea.append(" PORT IO "+e);
    Rra.textArea.setForeground(Color.red);
}
try {
serialPort.setSerialPortParams(bits, 
SerialPort.DATABITS_8, 
SerialPort.STOPBITS_1, 
SerialPort.PARITY_NONE);
} catch (UnsupportedCommOperationException e) {} 
try {
serialPort.notifyOnOutputEmpty(true);
} catch (Exception e) {
     Rra.textArea.append(" PORT Exception "+e+" Error setting event notification ");
 Rra.textArea.setForeground(Color.red);
}  	  
		} 
	    } 
	} 
	if (!portFound) 
        {
             JPanel pane=new JPanel(); 
             LinkedList < JRadioButton> jcheck=new LinkedList(); 
             pane.setLayout(new GridLayout(port.size(),1)); 
             for(int i=0;i<port.size();i++)
             {
                 String oneport=port.get(i);
                 JRadioButton now=new JRadioButton(oneport);
                 jcheck.add(now);
                 pane.add(now); 
             }
	    System.out.println("port " + defaultPort + " not found.");
              Rra.textArea.setForeground(Color.red);
            Rra.textArea.append("\n port " + defaultPort + " not found. RECONNECTING");
 JOptionPane.showMessageDialog(null, pane,"Port " + defaultPort + " not found.",JOptionPane.WARNING_MESSAGE);  
    
//      Rra.textArea.setForeground(Color.black);
            for(int i=0;i<port.size();i++)
            {
                if(jcheck.get(i).isSelected())
                { 
                    defaultPort=port.get(i);
                    return portReConnection();
                }
            }
//             JOptionPane.showMessageDialog(null, "PORT " + defaultPort + " not found.", "", JOptionPane.WARNING_MESSAGE);
			
	}
	 
return portFound;         
}
static void printByteArray(byte []ba)
{
    for(int i=0;i<ba.length;i++)
    {
      System.out.print(" "+ba[i]+" ");
    }
}
public static void main(String[] arghs) {
    
//RRA_SDC rra=  new RRA_SDC(1585555, "2323", "BBBCCNNNNNN",null,"");
//  String aim="AIMABLE/:";
//  printByteArray(aim.getBytes());
//  System.out.println();
//   aim="aimable";
// printByteArray(aim.getBytes());
//    
//  char c='0';
// for(int i=0;i<70;i++)
//    {
//      System.out.println(c+" "+(""+c).getBytes()[0] ); 
//      c++;
//    } 
    
//System.err.println((0x24+0x20+0xE5+0x05)+" "+(0x012E)); 
//System.err.println((0x30+0x31+0x32+0x3E)+" "+(0x012E)); 
}

}
