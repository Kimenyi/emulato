/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ebm.sdc.rra;

import ebm.sdc.rra2.RRA_SDC_SOFT;
import ebm.sdc.rra2.RraSoft;
import ebm.sdc.xml.BUILD_XML;
import static ebm.sdc.xml.BUILD_XML.doXmlItem;
import static ebm.sdc.xml.BUILD_XML.doXmlReceipt;
import static ebm.sdc.xml.BUILD_XML.doXmlReceiptItem;
import static ebm.sdc.xml.BUILD_XML.receipt;
import static ebm.sdc.xml.BUILD_XML.receiptRefund;
import ebm.sdc.xml.ITEM;
import ebm.sdc.xml.RECEIPT;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.CallableStatement;
import java.sql.SQLException;
import java.util.Date;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingWorker;

/**
 *
 * @author Kimenyi
 */
public class Rra extends JFrame {

private int PORT_SDC = 9632;
JButton connectButton,resetVarsButton,startIshyigaButton,clearButton; 
 
 static public int connNBR=0;
static public JTextArea textArea = new JTextArea(); 
RRA_DB db=null; 
RRA_SDC sdc=null;
String user="";
String defaultPort="COM27";
static public int Tosleep=1000;
static public int EjTosleep=0;
static public int busyTime=100; 
static public String version="ISHYIGA";
static public String bcpFile="F:/BCP/";
static public String bitsRate="9600";
RRA_COMMAND rCmd2=null;
static public String softIP="192.168.1.92";
static public  int portSoft=7779;
String []sp2=null;
boolean kora=true;
int LOAD=5;


public Rra(RRA_DB db,String user,String defaultPort) 
    {
    this.user=user; 
    this.db=db; 
    Rra.version=db.getRRA("version");
    this.defaultPort=defaultPort; 
    if(db.dataBase.toUpperCase().contains("TIGO"))
     { String portS=JOptionPane.showInputDialog(null, "ENTER NEXT OPEN PORT", ""+PORT_SDC);
       PORT_SDC=Integer.parseInt(portS); }
  //  testEJ(3);
    draw();
    System.out.println  ("Lancement SDC QUEU  SIZE :"  ); 
    connSdc();
    RRAvars();
    clear();
    SyncroSdc();
    rebaSDC();
    }
 public void draw()
{
    setTitle("Ishyiga RRA SDC CONTROLER"); 
    this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE); 
    
    //on fixe  les composant del'interface
    Container content = getContentPane(); 
     westPane(content);

     setSize(1150,400);
     setVisible(true);

}
   
public void westPane(Container content )
{ 

JPanel panel = new JPanel();
panel.setLayout(new GridLayout(4,1 ));//
panel.setPreferredSize(new Dimension(250,350)); //400=largeur350=longeur

connectButton=new JButton("CONNECT");
connectButton.setBackground(new Color(102, 255, 153));
resetVarsButton=new JButton("RESET RRA VARS");
resetVarsButton.setBackground(new Color(255, 255, 153));
startIshyigaButton =new JButton("SYNC VSDC");
startIshyigaButton.setBackground(new Color(51, 204, 255));
clearButton=new JButton("CLEAR");
clearButton.setBackground(new Color(255, 204, 102));
textArea=new JTextArea(20, 5);
 panel.add(connectButton); 
 panel.add(resetVarsButton );  
 panel.add(startIshyigaButton ); 
panel.add(clearButton ); 
JPanel MAIN = new JPanel();
//MAIN.setLayout(new GridLayout(1,2));
MAIN.setPreferredSize(new Dimension(1050,350));
JScrollPane jt=new JScrollPane(textArea);
jt.setPreferredSize(new Dimension(750,350));
MAIN.add(jt);  MAIN.add(panel);
 
content.add(MAIN); 
 
}   
  
public void RRAvars()
{

resetVarsButton.addActionListener(new ActionListener() {
public void actionPerformed(ActionEvent ae)
{
    if(ae.getSource()==resetVarsButton  )
    { 
        if(db!=null)
        { LinkedList<RRA_VARIABLE> vars = db.getV();
      
        if(sdc!=null && vars.size()>0 )
        {
            new RRA_INTERFACE(db, vars, user, sdc);
        }
        else if( vars.size()>0 )
        {
        JOptionPane.showMessageDialog(rootPane, " SDC IS NOT CONNECTED");
        new RRA_INTERFACE(db, vars, user, sdc);
        
        }
        
        
        
        }
        else
            JOptionPane.showMessageDialog(rootPane, "NO CURRENT CONNECTION");
    }
 }});
        
}  

void autoSyncSdc()
{
 BUILD_XML.buildItem(db.STATEMENT);
// ebm.sdc.rra2.RRA_RECEIVED_PACKAGE recItem= RRA_SDC_SOFT.sendToSDCSOFT
//        ("MAXIDITEM", "0>>0"  ,user,portSoft,softIP); 
//textArea.append(" \n MAXIDITEM " +recItem.data);    
//for (int i= Integer.parseInt(recItem.data); i< BUILD_XML.lesItems.length;i++) {
//ITEM lesItem = BUILD_XML.lesItems[i];
//if(lesItem!=null)
//{String item = doXmlItem(lesItem);
//recItem=RRA_SDC_SOFT.sendToSDCSOFT("ITEM",lesItem.ID_PRODUCT+">>"+item ,user ,portSoft,softIP); 
//textArea.append(" \n ITEM " +recItem.data);  } 
//} 

LinkedList <Integer> laListe= db.getIdInvoiceUnPushedReceipt("INVOICE"); 
for(int i=0;i<laListe.size();i++)// 
{   
    RECEIPT rece= receipt(laListe.get(i),  db.STATEMENT);
    ebm.sdc.rra2.RRA_RECEIVED_PACKAGE getDareceipt=
          RRA_SDC_SOFT.sendToSDCSOFT("TRNEXTDATA",rece.ID_INVOICE+">>S" ,user ,portSoft,softIP); 
    
    rece.extData= getDareceipt.data  ;
    
    String invoice=    (doXmlReceipt(rece, "","S",rece.REFERENCE ))   ;
    
    ebm.sdc.rra2.RRA_RECEIVED_PACKAGE rec=
          RRA_SDC_SOFT.sendToSDCSOFT("TRNRECEIPT",rece.ID_INVOICE+">>"+invoice ,user ,portSoft,softIP); 
    
    textArea.append(" \n TRNRECEIPT INVOICE " +rec.data);
    
     
    String list=  doXmlReceiptItem (rece); 
     rec=RRA_SDC_SOFT.sendToSDCSOFT("TRNRECEIPTITEM",rece.ID_INVOICE+">>"+list ,user ,portSoft,softIP); 
    textArea.append(" \n TRNRECEIPTITEM INVOICE " +rec.data);
    
    if(rec.data.equals("true"))
    {db.updatePUSHEDReceipt(""+ rece.ID_INVOICE,"INVOICE");}
    
 } 
laListe= db.getIdInvoiceUnPushedReceipt("REFUND");
for(int i=0;i<laListe.size();i++)// 
{    
    RECEIPT rece= receiptRefund(laListe.get(i),  db.STATEMENT);
    String invoice=   (doXmlReceipt(rece, ""+rece.REDUCTION,"R",rece.REFERENCE))   ;
    ebm.sdc.rra2.RRA_RECEIVED_PACKAGE rec=
            RRA_SDC_SOFT.sendToSDCSOFT("TRNRECEIPTREFUND",rece.ID_INVOICE+">>"+invoice ,user ,portSoft,softIP); 
    textArea.append(" \n TRNRECEIPT  REFUND " +rec.data);
    if(rec.data.equals("true"))
    {db.updatePUSHEDReceipt(""+ rece.ID_INVOICE,"REFUND");} 
    String list=  doXmlReceiptItem (rece)   ; 
    rec=RRA_SDC_SOFT.sendToSDCSOFT("TRNRECEIPTITEMREFUND",rece.ID_INVOICE+">>"+list ,user ,portSoft,softIP); 
    textArea.append(" \n TRNRECEIPTITEM REFUND " +rec.data);
 }            
   ebm.sdc.rra2.RRA_RECEIVED_PACKAGE rec=RRA_SDC_SOFT.sendToSDCSOFT
        ("DELETESTOCK", "0>>0"  ,user,portSoft,softIP );
    textArea.append(" \n DELETESTOCK " +rec.data);
    
if(rec.data.equals("true"))
    {boolean stock = BUILD_XML.buildStock(db.STATEMENT,user,portSoft); 
                    textArea.append(" \n STOCK " +stock);     }   
         
}
public void SyncroSdc() {
startIshyigaButton.addActionListener(new ActionListener() {

    public void actionPerformed(ActionEvent ae) {
        if (ae.getSource() == startIshyigaButton) {
             

            autoSyncSdc();
            
        }
    }
});
}

public String getSaleString2(RRA_PACK rp) 
{
    String inv=""+rp.id_invoice;
    String tin="";
   
    if(rp.ClientTin!=null && rp.ClientTin.length()==9)
    {
        tin=","+rp.ClientTin;
    }
    else
    {
        tin="";
    }
    
   return rp.rtype+rp.ttype+rp.MRC+","+rp.TIN+",&TIME&,&ID_INVOICE&,"+
RRA_PRINT2.setSansVirgule(rp.tvaRateA)+","+ RRA_PRINT2.setSansVirgule(rp.tvaRateB)+","+ RRA_PRINT2.setSansVirgule(rp.tvaRateC)+","+
           RRA_PRINT2.setSansVirgule(rp.tvaRateD)+","+RRA_PRINT2.setSansVirgule(rp.totalpriceA)+","+ RRA_PRINT2.setSansVirgule(rp.totalpriceB)+","+ 
           RRA_PRINT2.setSansVirgule(rp.totalpriceC)+","+ RRA_PRINT2.setSansVirgule(rp.totalpriceD)+","+
RRA_PRINT2.setSansVirgule(rp.tvaPriceA)+","+ RRA_PRINT2.setSansVirgule(rp.tvaPriceB)+","+ RRA_PRINT2.setSansVirgule(rp.tvaPriceC)+","+RRA_PRINT2.setSansVirgule(rp.tvaPriceD)+""+tin;
}

String stringToByte(String toTransform)
{ 
  String res="" ; 
 String [] splited=toTransform.split(";");    
 byte [] byteList=new byte [splited.length];
 
 for(int i=0;i<splited.length;i++)   
{
    try
    {
 byteList[i]=(byte)Integer.parseInt(splited[i]);   
    }
    catch (NumberFormatException e)
    {
    System.err.println(splited[i]+"; "+e);
    
    }
    }
 
for(int i=0;i<byteList.length;i++)   
{
  res+= getChar(byteList[i])  ;  
}  

return res;

}

String getChar(byte b)
{
char c='0';
if(b==16)
    return "B";
if(b==46)
    return ".";
if(b==47)
    return "/";
if(b==32)
    return " ";
if(b==44)
    return ",";

 for(int i=0;i<70;i++)
    {
    if((""+c).getBytes()[0]==b)
      return ""+c;
    c++;
    } 
return ""+b;
}
 
public void connSdc()
{
connectButton.addActionListener(new ActionListener() {
public void actionPerformed(ActionEvent ae)
{
if(ae.getSource()==connectButton  )
{ 
 String  rule=  (String) JOptionPane.showInputDialog(null, "WHERE TO CONNECT", " ",
JOptionPane.QUESTION_MESSAGE, null, new String[]{"PORT SDC","PORT SERVER RRA"}, "PORT SERVER RRA");
         
if(rule.equals("PORT SDC"))
 rebaSDC();
else if(rule.equals("PORT SERVER RRA"))
rebaNet();

    }
 }});
        
} 
public void clear()
{ 
clearButton.addActionListener(new ActionListener() {
public void actionPerformed(ActionEvent ae)
{
    if(ae.getSource()==clearButton  )
    { 
 String text=textArea.getText();
  String dateD=""+new Date(); 
    dateD=dateD.replaceAll(":", "-");  
    String  file=bcpFile+"TRACK/TRACK_"+dateD+".txt";
    System.out.println(file);
    db.andika(text,file);
    textArea.setText("");
    koraBCP();
    
    }
 }});
        
} 

public void actionPerformed(ActionEvent e) { 
  
textArea.append(e.getActionCommand());
System.out.println(e.getActionCommand());   
}
 
 void rebaNet()
 {
      textArea.append("Lancement SDC Controler  " );
 
/** Create a SwingWorker instance to run a compute-intensive task */
final SwingWorker<String , String> worker = new SwingWorker<String, String>() 
{
/** Schedule a compute-intensive task in a background thread */
         @Override
protected String doInBackground() throws Exception {
  
  defaultPort= JOptionPane.showInputDialog(null, "String", defaultPort);  
  sdc =   new RRA_SDC(db.getV ("tin"),db.getV("mrc"), db.getV("company"), db.getV("City"), db.getV("version"),
          db.getString("bp"),db.conn,user,defaultPort);
  RRA_SDC.busyTime=busyTime;
  
  try{
  RRA_SDC.bits= Integer.parseInt(bitsRate);
  }
  catch(Exception e)
  {
      RRA_SDC.bits=9600;
  }
  koraBCP();
try {
    
 ServerSocket socketServeur = new ServerSocket(PORT_SDC);
 System.out.println("Lancement du serveur");
 textArea.append("\n Lancement SDC Controler");
 textArea.append("\n Server :"+db.server );
 textArea.append("\n DB : "+db.dataBase);
 textArea.append("\n PORT SDC : "+defaultPort);
 textArea.append("\n PORT ISHYIGA CIS: "+PORT_SDC);
 textArea.append("\n PORT VSDC : "+softIP);
 textArea.append("\n IP VSDC : "+portSoft);
 while (true) {
 Socket socketClient = socketServeur.accept();
 RRA_SERVER t = new RRA_SERVER(socketClient ); 
 t.start();
 }
 } catch (Exception e) { 
  textArea.append("SERVER SOCKET "+e);   
 }
    
 return  " "; 
}
 
         /** Run in event-dispatching thread after doInBackground() completes */
@Override
protected void done() {
 
 textArea.append("  \n Result is...");                 
  
} 
 @Override
         protected void process(java.util.List<String> chunks) {
            // Get the latest result from the list
            String latestResult = chunks.get(chunks.size() - 1); 
            textArea.append("  \n Result is..."+latestResult);
         }
      };
             worker.execute();  // start the worker thread 
            textArea.append("  Running...");
      /** Event handler for the PropertyChangeEvent of property "progress" */
      
worker.addPropertyChangeListener(new PropertyChangeListener() {
@Override
public void propertyChange(PropertyChangeEvent evt) {
if (evt.getPropertyName().equals("progress")) {  // check the property name
//pbWorker.setValue((Integer)evt.getNewValue());  // update progress bar
}
}
}); 
}
 void koraBCP()
 {
      textArea.append("Ngiye gukora bcp  " );
 
/** Create a SwingWorker instance to run a compute-intensive task */
final SwingWorker<String , String> worker = new SwingWorker<String, String>() 
{
/** Schedule a compute-intensive task in a background thread */
         @Override
protected String doInBackground() {
        try {
            
            int f= (int)(5*Math.random());
            String backupdirectory=bcpFile+f;
            System.out.println(backupdirectory);
            CallableStatement cs = db.connBcp.prepareCall("CALL SYSCS_UTIL.SYSCS_BACKUP_DATABASE(?)");
            cs.setString(1, backupdirectory);
            cs.execute();
            cs.close();             
                     
          return  "OK";
        } catch (Throwable ex) { 
            
      JOptionPane.showMessageDialog(null, ex);
        }
         return  "SHIDA";
}
/** Run in event-dispatching thread after doInBackground() completes */
@Override
protected void done() {
 
 textArea.append("  \n Result is BCP");                 
  
} 
 @Override
         protected void process(java.util.List<String> chunks) {
            // Get the latest result from the list
            String latestResult = chunks.get(chunks.size() - 1); 
            textArea.append("  \n Result is BCP"+latestResult);
         }
      };
             worker.execute();  // start the worker thread 
            textArea.append("  Running...");
      /** Event handler for the PropertyChangeEvent of property "progress" */
      
worker.addPropertyChangeListener(new PropertyChangeListener() {
@Override
public void propertyChange(PropertyChangeEvent evt) {
if (evt.getPropertyName().equals("progress")) {  // check the property name
//pbWorker.setValue((Integer)evt.getNewValue());  // update progress bar
}
}
}); 
}
  void rebaSDC()
{
    System.out.println("Lancement SDC QUEU  SIZE :"+sdc.queu.size()+" Processing :"+sdc.currentQueuId );
 
textArea.append("\n Lancement SDC QUEU  SIZE :"+sdc.queu.size()+" Processing :"+sdc.currentQueuId );
 
/** Create a SwingWorker instance to run a compute-intensive task */
final SwingWorker<String , String> worker = new SwingWorker<String, String>() 
{
/** Schedule a compute-intensive task in a background thread */
         @Override
protected String doInBackground() throws Exception {  
             
try { 
Thread t = new Thread();
 byte [] arr2= new byte []{(byte)0x27,(byte)0x26,(byte)0x25}; int gd=0;
while (kora) 
{
if(RRA_SDC.queu.size()>(RRA_SDC.currentQueuId))
{ 
    if(RRA_SDC.currentQueuId% 5 == 0)
    {autoSyncSdc();}
    
 if(gd==1)
gd=0;
else
gd=1; 
          
textArea.append("\n SCD QUEU  SIZE :"+RRA_SDC.queu.size()+" Processing :"+sdc.currentQueuId );
RRA_COMMAND rCmd=RRA_SDC.queu.get(RRA_SDC.currentQueuId);
    
     
 if(RRA_SDC.sdcIsConnected && rCmd.CMD!=null && rCmd.CMD.equals("ID REQUEST"))
     {
      rCmd.replyClient(sdc.SDC);        
      Rra.textArea.append("\n ID REQUEST"); 
     }
 else if(rCmd.CMD!=null && rCmd.CMD.equals("ID REQUEST") )
     {
             RRA_RECEIVED_PACKAGE rec=sdc.sendToSDC3(arr2[gd],rCmd.CMD,rCmd.DATA ,rCmd.SENDER );
             //RRA_RECEIVED_PACKAGE rec=new RRA_RECEIVED_PACKAGE("", "SDC0010001", "P");
            
            if(rec!=null)
            {
                Rra.textArea.append("\n SDC RESPONCE  : "+rec.data);
                RRA_SDC.sdcIsConnected=true;
                sdc.SDC=rec.data; 
                rCmd.replyClient(sdc.SDC);        
                Rra.textArea.append("\n ID REQUEST");
            }
            else
            {
                Rra.textArea.append("\n SDC RESPONCE  : "+rec.data);
                RRA_SDC.sdcIsConnected=false;
                rCmd.replyClient("ERROR SDC NOT CONNECTED");   
            } 
            
//            RRA_SDC_SOFT.sendToSDCSOFT
//        (rCmd.CMD,rCmd.DATA ,rCmd.SENDER,portSoft,softIP);
            
     }
else
     {
      if(rCmd.CMD!=null && rCmd.CMD.equals("SEND RECEIPT") )
         {
            RRA_RECEIVED_PACKAGE rec=null;
            RRA_INVOICE ri=null;
                String MRC=rCmd.DATA.substring(2, 13);
                if((rCmd.R_TYPE.equals("P") && rCmd.T_TYPE.equals("S")))
                    {
                        ri=  db.insertProforma3(rCmd.KEY_INVOICE); 
                       rCmd.ID_INVOICE=ri.id_invoice;  
                       rCmd.DATA=rCmd.DATA.replaceAll("&ID_INVOICE&", ""+rCmd.ID_INVOICE);
                       rCmd.DATA=rCmd.DATA.replaceAll("&TIME&", ""+db.getNowTime2(ri.heure));
                       System.err.println(rCmd.DATA);
                       rec=sdc.sendToSDC3(arr2[gd],rCmd.CMD,rCmd.DATA,rCmd.SENDER );
                      
                    }  
                else if((rCmd.R_TYPE.equals("N") && rCmd.T_TYPE.equals("S")) || 
                        (rCmd.R_TYPE.equals("T") && rCmd.T_TYPE.equals("S"))    )
                    {
                        ri=  db.insertInvoice3(rCmd.KEY_INVOICE); 
                        rCmd.ID_INVOICE=ri.id_invoice;
                        System.err.println("data------"+rCmd.DATA+" "+rCmd.ID_INVOICE+" "+db.getNowTime2(ri.heure));
                        
                        rCmd.DATA=rCmd.DATA.replaceAll("&ID_INVOICE&", ""+rCmd.ID_INVOICE); 
                        rCmd.DATA=rCmd.DATA.replaceAll("&TIME&", ""+db.getNowTime2(ri.heure));
               /////////NDABICOMMENTINZE        
                       System.err.println("data------"+rCmd.DATA+" "+rCmd.ID_INVOICE+" "+db.getNowTime2(ri.heure));
                       rec=sdc.sendToSDC3(arr2[gd],rCmd.CMD,rCmd.DATA,rCmd.SENDER );
                       //rec=new RRA_RECEIVED_PACKAGE("","","");
                       //rec.data="P";
                       Rra.textArea.append(rCmd.CMD+" : "+rCmd.DATA);
                    }
                
                else if((rCmd.R_TYPE.equals("N") && rCmd.T_TYPE.equals("R")) || 
                        (rCmd.R_TYPE.equals("T") && rCmd.T_TYPE.equals("R"))    )
                    {
                         ri=  db.insertAnnule3(rCmd.KEY_INVOICE);  
                         ri.idInvRef=rCmd.ID_INVOICE;
                       rCmd.ID_INVOICE=ri.id_invoice;  
                       rCmd.DATA=rCmd.DATA.replaceAll("&ID_INVOICE&", ""+rCmd.ID_INVOICE);
                       rCmd.DATA=rCmd.DATA.replaceAll("&TIME&", ""+db.getNowTime2(ri.heure)); 
                       System.err.println(rCmd.DATA);
                       rec=sdc.sendToSDC3(arr2[gd],rCmd.CMD,rCmd.DATA ,rCmd.SENDER );
                       Rra.textArea.append(rCmd.CMD+" : "+rCmd.DATA);
                    }
                  else if((rCmd.R_TYPE.equals("C") && rCmd.T_TYPE.equals("S")))
                {
                    db.updateheureCopyInvoice(rCmd.ID_INVOICE ); 
                        ri=  db.getIdheureInvoice(rCmd.ID_INVOICE);  
                       rCmd.ID_INVOICE=ri.id_invoice;   
                       rCmd.DATA=rCmd.DATA.replaceAll("&TIME&", ""+db.getNowTime2(ri.heure)); 
                       System.err.println(rCmd.DATA);
                       rec=sdc.sendToSDC3(arr2[gd],rCmd.CMD,rCmd.DATA ,rCmd.SENDER );
                       Rra.textArea.append(rCmd.CMD+" : "+rCmd.DATA);
                }
              else if((rCmd.R_TYPE.equals("C") && rCmd.T_TYPE.equals("R")))
                {
                    db.updateheureCopyAnnule(rCmd.ID_INVOICE ); 
                       ri=  db.getIdheureRefund(rCmd.ID_INVOICE);  
                       rCmd.ID_INVOICE=ri.id_invoice;   
                       rCmd.DATA=rCmd.DATA.replaceAll("&TIME&", ""+RRA_DB.getNowTime2(ri.heure)); 
                       System.err.println(rCmd.DATA);
                       rec=sdc.sendToSDC3(arr2[gd],rCmd.CMD,rCmd.DATA ,rCmd.SENDER );
                       Rra.textArea.append(rCmd.CMD+" : "+rCmd.DATA);
                }      
                else
                    { 
                       Rra.textArea.append(rCmd.CMD+" : "+rCmd.DATA);
                       rec=sdc.sendToSDC3(arr2[gd],rCmd.CMD,rCmd.DATA,rCmd.SENDER );
                    }
                

          ////////////////////////////////////////////////////////////////////////////////////////////      
                    if( rec!=null && rec.data!=null && rec.data.contains("P"))
                          {
                            Rra.textArea.append("\n   RESPONCE  : "+rec.data);
                            RRA_SDC.sdcIsConnected=true;
                           rec=sdc.sendToSDC3(arr2[gd],"RESPONSE",""+rCmd.ID_INVOICE,rCmd.SENDER ); 
                           //rec.data="SDC001000638,461,739,NS,08/04/2014 15:44:22,PL4PIHHAKIEBBPA5,S7V6BAHPUWW2C2ZORJNT6HEKLY";
                            String []sp=null;
                              if(rec!=null && rec.data!=null)
                              {
                               rec.data=rec.data.replaceAll(",,", ",#,#");  
                               sp=rec.data.split(","); 
                              }       
                                   if(sp!=null && sp.length>6 )
                                   { 
                                      sp2=sp;    
                                      rCmd2=rCmd;
                                     if((rCmd.R_TYPE.equals("P") && rCmd.T_TYPE.equals("S")))
                                       {
                                           db.updateExternalProforma(rCmd.KEY_INVOICE,rec.data); 
                                       }
                                     else if((rCmd.R_TYPE.equals("N") && rCmd.T_TYPE.equals("S")) || 
                                             (rCmd.R_TYPE.equals("T") && rCmd.T_TYPE.equals("S")))
                                       {
                                           db.updateExternalInvoice(rCmd.KEY_INVOICE,rec.data); 
                                       }
                                     else if((rCmd.R_TYPE.equals("N") && rCmd.T_TYPE.equals("R")) || 
                                             (rCmd.R_TYPE.equals("T") && rCmd.T_TYPE.equals("R")))
                                       {
                                           db.updateExternalAnnule(rCmd.KEY_INVOICE,rec.data );
                                       }    
                                     else if((rCmd.R_TYPE.equals("C") && rCmd.T_TYPE.equals("S")))
                                       {
                                           db.updateExternalCopyInvoice(rCmd.ID_INVOICE,rec.data); 
                                       }
                                     else if((rCmd.R_TYPE.equals("C") && rCmd.T_TYPE.equals("R")))
                                       {
                                           db.updateExternalCopyAnnule(rCmd.ID_INVOICE,rec.data); 
                                       }    
                                 
                                          
                                           if(rCmd.R_TYPE.equals("N"))
                                              {  
//                                                  System.out.println("in backgr:");
//                                                final SwingWorker<String, String> worker = new SwingWorker< String, String>() 
//                                                {
//                                                    @Override protected  String  doInBackground() throws Exception 
//                                                      {
                                              //SDC001000638,461,739,NS,17/12/2013 17:44:22,PL4PIHHAKIEBBPA5,S7V6BAHPUWW2C2ZORJNT6HEKLY
                                                        LinkedList <String> ejList=makeEj(rCmd2,sp2[0],sp2[4],sp2[5],sp2[6],sp2[1]+"/"+sp2[2]+" "+sp2[3],MRC,ri)  ;
                                                        String instruction="B"; 
                                                        byte [] arr= new byte []{(byte)0x24,(byte)0x25,(byte)0x26}; int g=0;
                                                        
                                                        System.out.println("ej length:"+ejList.size());
                                                        //RRA_RECEIVED_PACKAGE rec=new RRA_RECEIVED_PACKAGE(null);
                                                        rec.data="P"; 
String ej="";
                                                            for(int j=0;j<ejList.size();j++)
                                                            { 
                                                                String line=ejList.get(j);
//                                                                System.out.println("j:"+j);
                                                                if(j+1==ejList.size())
                                                                {instruction="E"; }
                                                                else if(j>0 && j<ejList.size())
                                                                {instruction="N"; } 
                                                           ej+=instruction+line;      
                                                                if( rec!=null && rec.data!=null && rec.data.contains("P"))
                                                                {
                                                                   System.out.println(g+"   "+instruction +""+line);
                                                                   rec=sdc.sendToSDC4("EJ DATA",instruction+line,rCmd2.SENDER,arr[g]);
         
                                                                    if(g==1)
                                                                       g=0;
                                                                   else
                                                                       g=1;
                                                                }
                                                                else
                                                                { 
                                                                if(rec==null) { rCmd2.replyClient("ERROR IN COMM INSERTING EJ DATA"); }
                                                                else { rCmd2.replyClient("ERROR HAPPENED INSERTING EJ DATA "+rec.data); }  
                                                                rCmd2=null;
                                                                } 
                                                            } 
                                                            
                                                             db.updateEJInvoice(rCmd.KEY_INVOICE,ej); 
//                                     

                                               t.sleep(EjTosleep);
                                               Rra.textArea.append("---SLEEPT-EjTosleep "+EjTosleep); 
                                              }
                                           
                                                    if(rCmd2!=null)                                       
                                                    {
                                                        rCmd.replyClient(""+rCmd.ID_INVOICE+"#"+rCmd.KEY_INVOICE+"#"+  rec.data );
                                                    } 

                                    }////END RESPONSE YA SEND DATA
                                   else ///HABAYE IKOSA MURI RESPONSE
                                    {
                                    if(rec==null) { rCmd.replyClient("ERROR IN COMM LOOKING FOR RESPONSE"); }
                                    else { rCmd.replyClient("ERROR HAPPENED GETTING RESPONSE "+rec.data);}   
                                    //FATAL
                                    //EMPTY QUEU AND CLOSE SERVER
                                    sdc.currentQueuId++;
                                    kora=false;
                                    for( ;sdc.currentQueuId<sdc.queu.size();sdc.currentQueuId++)
                                    {    
                                        rCmd=sdc.queu.get(sdc.currentQueuId); 
                                        rCmd.replyClient(" ERROR IN COMM LOOKING FOR RESPONSE SHUT DOWN SERVER AUTOMATICALY CONTACT ISHYIGA TEAM ");
                                    }
                                    
                                    }
                        }//END SEND DATA
                        else //NTABWO HASUBIJWE P KURI SEND DATA
                        { 
                        if(rec==null)
                        { rCmd.replyClient("ERROR IN COMM SENDING RECEIPT");  }
                        else { rCmd.replyClient("ERROR IN DATA GETTING RECEIPT "+rec.data); }
                        }
          }// END SEND DATA
      else // OTHER COMMAND
          {
            Rra.textArea.append("\n"+rCmd.CMD+" : "+rCmd.DATA);
            if(rCmd.CMD!=null && rCmd.CMD.equals("ID REQUEST END INVOICE") )
            { rCmd.CMD="ID REQUEST"; }

            RRA_RECEIVED_PACKAGE rec=sdc.sendToSDC3(arr2[gd],rCmd.CMD,rCmd.DATA ,rCmd.SENDER );
            
            if(rec!=null)
            {
                Rra.textArea.append("\n SDC RESPONCE  : "+rec.data);
                RRA_SDC.sdcIsConnected=true;
                rCmd.replyClient(rec.data);  
            }
            else
            {
                Rra.textArea.append("\n SDC RESPONCE  : "+rec.data);
                RRA_SDC.sdcIsConnected=false;
                rCmd.replyClient("ERROR SDC NOT CONNECTED");   
            }
         } // END OTHER COMMAND
      }
 
 
 System.out.println(" hahaha");
     ebm.sdc.rra2.RRA_RECEIVED_PACKAGE rec= RRA_SDC_SOFT.sendToSDCSOFT
        (rCmd.CMD,rCmd.DATA ,rCmd.SENDER,portSoft,softIP); 
  System.out.println(" hahaha"+rec);    
     
if(rec==null || rec.data==null)
{
    int n = JOptionPane.showConfirmDialog(null, "EXIT ISHYIGA SDC CONTROLER \n VSDC NOT ON PLEASE START IT"
                                              , "ATTENTION", JOptionPane.YES_NO_OPTION);
    
            if (n == 0)
            { System.exit(0);}   
      
}
     RRA_SDC.timeToSleep=Tosleep;
     rCmd.socket.close();
     sdc.currentQueuId++;
     
}// iyo hari ikintu gitegereje
else // IYO NTAKINTU GITEGEREGE RYAMA T0SLEEP MILLISECOND
{
  t.sleep(Tosleep);
}
}// END WHILE
}// END TRY

catch (Exception e) 
{
    Rra.textArea.append("\n SDC RESPONCE  : "+e); 
    e.printStackTrace(); 
} 
return  " "; 
}
@Override protected void done() { } 
@Override protected void process(java.util.List<String> chunks) { }
};
worker.execute();  // start the worker thread 
worker.addPropertyChangeListener(new PropertyChangeListener() { 
@Override public void propertyChange(PropertyChangeEvent evt) {
if (evt.getPropertyName().equals("progress")) { }}}); 

}
  
public static int getQuantity(String s, String s1, int i) {
        int entier = -1;

        String in = JOptionPane.showInputDialog(null, s + s1);
        boolean done = true;

        while (in != null && i < 3 && done == true) {
            try {
                if (in.equals("")) {
                    i++;
                    in = JOptionPane.showInputDialog(null, s + " Enter a number ", "1");
                } else {
                    Integer in_int = new Integer(in);
                    entier = in_int.intValue();
                    done = false;
                    if (entier < 0 || entier == 0) {
                        entier = -1;
                    }
                }

            } catch (NumberFormatException e) {
                i++;
                in = JOptionPane.showInputDialog(null, s + " = " + in + "= Not a number; Enter a number ");
                done = true;
            }
        }
        if (i == 3) {
            JOptionPane.showMessageDialog(null, " Too many try ", " Sorry we have to Exit ", JOptionPane.PLAIN_MESSAGE);
        }

        return entier;
    }
 @Override
    protected void processWindowEvent(WindowEvent evt) {
        if (WindowEvent.WINDOW_CLOSING == evt.getID()) {
            //default icon, custom title
int n = JOptionPane.showConfirmDialog(null, "EXIT ISHYIGA SDC CONTROLER \n BCP WILL TAKE 2-5MIN"
                    , "ATTENTION", JOptionPane.YES_NO_OPTION);
            if (n == 0) {
                int f= (int)(10*Math.random());
                try {
                

    
  if(sdc.queu.size()>(sdc.currentQueuId))
     {
    for(int i=sdc.currentQueuId;i<sdc.queu.size();i++)
    {
       
       RRA_COMMAND rCmd=sdc.queu.get(i);
     
     rCmd.replyClient("ERROR SDC IS CLOSING WITHOUT PROCESSING YOUR REQUEST ");        
     Rra.textArea.append("\n ERROR SDC IS CLOSING WITHOUT PROCESSING YOUR REQUEST "); 
    }
    } 
    String text=textArea.getText();
                               String backupdirectory=bcpFile+f;
                               System.out.println(backupdirectory);
                               CallableStatement cs = db.connBcp.prepareCall("CALL SYSCS_UTIL.SYSCS_BACKUP_DATABASE(?)");
                               cs.setString(1, backupdirectory);
                               cs.execute();
                               cs.close();  
                       text+="bcp succeful"+backupdirectory;
    String dateD=""+new Date(); 
    dateD=dateD.replaceAll(":", "-");  
                       String  file=bcpFile+"TRACK/TRACK_"+dateD+".txt";
    db.andika(text,file);
    textArea.setText("");
                        
                } catch (SQLException ex) {
                    
String text=textArea.getText(); 
String backupdirectory=bcpFile+f;
text+="bcp fail"+backupdirectory;
String dateD=""+new Date(); 
dateD=dateD.replaceAll(":", "-");  
String  file=bcpFile+"TRACK/TRACK_"+dateD+".txt";
db.andika(text,file);
textArea.setText("");

}
     System.exit(0);
            }
        }
    }
 RRA_PACK getRpack( LinkedList <RRA_PRODUCT> getProductList )
{
double totalpriceA, totalpriceB, totalpriceC, totalpriceD,
          tvaPriceA, tvaPriceB, tvaPriceC, tvaPriceD;
     double tvaRateA, tvaRateB, tvaRateC, tvaRateD;
     totalpriceA=0; totalpriceB=0; totalpriceC=0; totalpriceD=0;
          tvaPriceA=0; tvaPriceB=0; tvaPriceC=0; tvaPriceD=0;
       tvaRateA=0;tvaRateB=0; tvaRateC=0; tvaRateD=0;
       
   tvaRateA= db.getTvaRate("A");
   tvaRateB= db.getTvaRate("B");
   tvaRateC= db.getTvaRate("C");
   tvaRateD= db.getTvaRate("D"); 
   
   for (int i = 0; i < getProductList.size(); i++) {

                RRA_PRODUCT p2 =  getProductList.get(i); 
                
                double taxable =((p2.salePrice()) / (1 + p2.tva));
                 
                if(p2.tva==tvaRateA)
                {tvaPriceA+=(taxable * p2.tva);
                totalpriceA+=p2.salePrice();
                getProductList.get(i).classe="A-EX";
                }
                
                else if(p2.tva==tvaRateD)
                {tvaPriceD+=(taxable * p2.tva);
                totalpriceD+=p2.salePrice();
                getProductList.get(i).classe="D";
                }
                else if(p2.tva==tvaRateB)
                {tvaPriceB+=(taxable * p2.tva);
                totalpriceB+=p2.salePrice();
                 getProductList.get(i).classe="B";
                }
                else if(p2.tva==tvaRateC)
                {tvaPriceC+=(taxable * p2.tva);
                totalpriceC+=p2.salePrice();
                 getProductList.get(i).classe="C";
                }
                else
                 System.err.println(totalpriceB+" FATAL    "+taxable);
            }
   
  return new RRA_PACK( 
          totalpriceA, totalpriceB, totalpriceC, totalpriceD,
          tvaPriceA, tvaPriceB, tvaPriceC, tvaPriceD,
          tvaRateA, tvaRateB, tvaRateC, tvaRateD );
}
 
 void testEJ(int id_invoice)
 {
RRA_INVOICE ri=db.getIdREFUNDID (id_invoice);

System.out.println(ri);
System.out.println(ri.key);
 String [] sp3= ri.data.split(","); 
 
 RRA_COMMAND rcmd=new RRA_COMMAND
 ("","","","" ,null );
          
     rcmd.TIN="1212121212";
     rcmd.R_TYPE="N";
     rcmd.T_TYPE="R";
     rcmd.ID_INVOICE=id_invoice;
     rcmd.KEY_INVOICE=ri.key;
    RRA_SDC.TIN= db.getV ("tin");
RRA_SDC.MRC=db.getV("mrc");
RRA_SDC.COMPANY=db.getV("company");
RRA_SDC.CITY=db.getV("City");
RRA_SDC.VERSION=db.getV("version");
RRA_SDC.ADRESS=db.getString("bp");
 System.out.println(RRA_SDC.VERSION);
 LinkedList <String> ejList=makeEj 
(rcmd,sp3[0],sp3[4],sp3[5],sp3[6],sp3[1]+"/"+sp3[2]+" "+sp3[3],"ALG01000001",ri)  ;
for(int j=0;j<ejList.size();j++)
{  
     System.out.println(ejList.get(j));                                                       
}
}
 
LinkedList <String > makeEj(RRA_COMMAND rCmd2, String sdc,String date_time_sdc,
        String signature,String Internal_Data,String receipt2,String MRC,RRA_INVOICE ri2)
{ 
     int NEG=1; int id_ref=0;
 LinkedList <RRA_PRODUCT> prodList=new LinkedList();
 if(rCmd2.T_TYPE.equals("S"))
 {
     prodList=db.getUrutonde(rCmd2.KEY_INVOICE);
     id_ref=ri2.id_invoice;
 }
 else if(rCmd2.T_TYPE.equals("R"))
 {
     prodList=db.getListREFUND(rCmd2.KEY_INVOICE  );
     NEG=-1; 
     id_ref=ri2.idInvRef;
 } 
 RRA_PACK rp= getRpack( prodList);  
//   JOptionPane.showMessageDialog(null, "NDAHAGEZE id_ref "+id_ref);
 LinkedList<String> infoClient=db.Leclient(db.getClient2 (rCmd2.KEY_INVOICE));
 
return (new RRA_PRINT2(RRA_SDC.COMPANY, RRA_SDC.ADRESS, RRA_SDC.CITY, RRA_SDC.TIN, rCmd2.TIN  ,
rp.getTotal(),(rp.totalpriceA ), (rp.totalpriceB ), rp.tvaPriceB, rp.getTotalTaxes(),
rp.getTotal(), sdc,""+receipt2,   Internal_Data, signature,rCmd2.ID_INVOICE,
 MRC,prodList,rCmd2.R_TYPE,rCmd2.T_TYPE,RRA_SDC.ADRESS, NEG, id_ref, infoClient,
RRA_DB.getNowTime2(ri2.heure), date_time_sdc,"#REFERENCE#") ).ejList;
      
 }    
    public static void main(String[] args) { 
        StartServer d;
        BufferedReader entree = null;
         try {
            d = new StartServer();
           int id = getQuantity("USER ID ", " : CHIFFRE", 0);
//              int id =1;
            if (id != -1) {

                JPanel pw = new JPanel();
                JPasswordField passwordField = new JPasswordField(10);
                JLabel label = new JLabel("PASSWORD: ");
                pw.add(label);
                pw.add(passwordField);
                passwordField.setText("");
                JOptionPane.showConfirmDialog(null, pw);
                Integer in_int = new Integer(passwordField.getText());
                int carte = in_int.intValue();  
//                int carte =2;
                
                File productFile = new File("log.txt");
                String ligne = null; 
                LinkedList<String> give = new LinkedList();
                entree = new BufferedReader(new FileReader(productFile));
                try {
                    ligne = entree.readLine();
                } catch (IOException e) {
                    System.out.println(e);
                }
                int i = 0;
                 try {
                while (ligne != null) {
                    // System.out.println(i+ligne);
                    give.add(ligne);
                    i++;

                   
                        ligne = entree.readLine();
                 
                }
                entree.close();
 
 
                String dbName = give.get(2);
                String server = give.get(1);
                String defaultPort = give.get(7);
                String tosleep=give.get(8);
                String ejsleep=give.get(9);
                String busytime=give.get(10);
                bcpFile= give.get(11);
                bitsRate= give.get(12);
                softIP= give.get(13);
                RraSoft.softIP=softIP;
                RRA_DB db = new RRA_DB(server, dbName); 
                String user=db.emp(id, carte);
                if ( user!=null && !user.equals("") ) {
                      Tosleep=Integer.parseInt(tosleep);
                      EjTosleep=Integer.parseInt(ejsleep);
                    busyTime=Integer.parseInt(busytime);
                    Rra rra = new Rra(db,user,defaultPort);
                    } else {
                        JOptionPane.showMessageDialog(null, "  MOT DE PASSE INCORRECTE ", " IKIBAZO ", JOptionPane.PLAIN_MESSAGE);
                    }
          } catch (IOException e) {
                        JOptionPane.showMessageDialog(null, "QUESTION", " reba log ", JOptionPane.PLAIN_MESSAGE);
                    }
    }
}
        catch (FileNotFoundException ex) {
            Logger.getLogger(Rra.class.getName()).log(Level.SEVERE, null, ex);
        }
         catch (Throwable t) {
            JOptionPane.showMessageDialog(null, " SERVEUR ETEINT " + t, " IKIBAZO ", JOptionPane.PLAIN_MESSAGE);
        }
         finally {
            try {
                entree.close();
            } catch (IOException ex) {
                Logger.getLogger(Rra.class.getName()).log(Level.SEVERE, null, ex);
            }
            
         }
         
    }
        }