/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ebm.sdc.rra;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Kimenyi
 */
public class RRA_DB {
String driver = "org.apache.derby.jdbc.NetWorkDriver";
String connectionURL ;  
ResultSet outResult ;
Connection conn = null;
public Connection connBcp=null;
Statement STATEMENT; 
PreparedStatement psInsert; 
String server,  dataBase;
double tvaRateA, tvaRateB, tvaRateC, tvaRateD; 
 RRA_DB(String server,String dataBase)
{   
this.server=server;
this.dataBase=dataBase;
connectionURL = "jdbc:derby://"+server+":1527/" + dataBase  ;
read(); 
tvaRateA= getTvaRate("A");
tvaRateB= getTvaRate("B");
tvaRateC= getTvaRate("C");
tvaRateD= getTvaRate("D");  
}
 
 static public String derbyPwd=";user=sa;password=ish";
 public  void read()
{ 
 try {
    conn = DriverManager.getConnection(connectionURL+derbyPwd); 
    connBcp = DriverManager.getConnection(connectionURL+derbyPwd);  
     conn.setSchema("APP"); 
      connBcp.setSchema("APP"); 
    STATEMENT = conn.createStatement();
    
    System.out.println( "connection etablie TO  "+connectionURL);  
    }
    catch (SQLException e){ 
  JOptionPane.showMessageDialog(null, "CONNECTION FAIL TO Auto "+connectionURL);
          System.out.println("Connection Fail " + e);
          System.exit(1);
            
} 
   
} 
 public void upDateRRA_VARIABLE(RRA_VARIABLE c,String nom,boolean b)
 {
        try 
        { 
            Statement sU = conn.createStatement();
            String createString1 = " update VARIABLES set VALUE_VARIABLE='" + c.value + "',FAMILLE_VARIABLE='" + c.famille + "' ," +
                    "VALUE2_VARIABLE='" + c.value2+ "'  where NOM_VARIABLE='"+c.nom+"'";
            sU.execute(createString1);
           // JOptionPane.showMessageDialog(null," VARIABLE UPDATED SUCCESSFULLY "," MURAKOZE ",JOptionPane.PLAIN_MESSAGE);

        } catch (SQLException ex) {

            JOptionPane.showMessageDialog(null,"  "+ex," UpDate Client Fail " ,JOptionPane.ERROR_MESSAGE);
        insertError(ex+"","update");     }


}
 
 LinkedList<RRA_VARIABLE> getV()
 {
      LinkedList<RRA_VARIABLE>vars=new LinkedList();
 try
 {
    
    outResult = STATEMENT.executeQuery("select  * from APP.VARIABLES where  APP.variables.famille_variable= 'RRA' and nom_variable not like '%version%'  ");

        while (outResult.next())
        {
          //  System.out.println(outResult.getString(2));
                String num =outResult.getString(1);
                String value =outResult.getString(2);
                String fam =outResult.getString(3);
                String value2 =outResult.getString(4);
                vars.add(new RRA_VARIABLE(num,value,fam,value2));
        }
        
      
         
 }
 catch(SQLException e)
 {
     System.out.println(e);
 }
 return vars;
 }
 int getMaxInvoice(String table)
 {
       
 try
 {   
    outResult = STATEMENT.executeQuery("select  max(id_"+table+") from APP."+table+" ");
    
        while (outResult.next())
        {
                 return outResult.getInt(1); 
        } 
 }
 catch(SQLException e)
 {
    insertError("getMAx sdc", " "+e);
 }
 return -1;
 }
 int getMaxProfo()
 { 
 try
 {   
outResult = STATEMENT.executeQuery("select  max(id_proforma) from APP.proforma "); 
while (outResult.next())
{
         return outResult.getInt(1); 
} 
 }
 catch(SQLException e)
 {
    insertError("getMAx profo sdc", " "+e);
 }
 return -1;
 }
 String  emp(int id,int carte)
{ 
    try
  { 
System.out.println("select nom_employe from APP.EMPLOYE where id_employe="+id+" AND catre_indetite="+carte);
    outResult = STATEMENT.executeQuery("select nom_employe from APP.EMPLOYE where id_employe="+id+"AND catre_indetite="+carte);
    
   while (outResult.next())
    {
    return outResult.getString(1);
    }

    //  Close the resultSet
    outResult.close();



    //  Beginning of the primary catch block: uses errorPrint method
    }  catch (Throwable e)  {
   insertError(e+"","employe");
    }


return "";
}
 static void andika(String kwandika, String file) {

        File f = new File(file);
        try {
            PrintWriter sorti = new PrintWriter(new FileWriter(f));
            sorti.println(kwandika);
            sorti.close();
        } catch (Throwable e) {
            JOptionPane.showConfirmDialog(null, "ECRIRE DANS " + file + " IMPOSSIBLE \n" + e);
        }
    }
void insertError(String desi,String ERR)
    {
        try {
            psInsert = conn.prepareStatement("insert into APP.ERROR(DESI,USERNAME)  values (?,?)");

            if(desi.length()>160)
                desi=desi.substring(0, 155);
             if(ERR.length()>160)
                ERR=ERR.substring(0, 155);

            psInsert.setString(1,desi );
            psInsert.setString(2, ERR);



            psInsert.executeUpdate();
            psInsert.close();
            JOptionPane.showMessageDialog(null,"  "+desi," Ikibazo ",JOptionPane.PLAIN_MESSAGE);



        } catch (Throwable ex) {
         JOptionPane.showMessageDialog(null,"  "+ex," Ikibazo ",JOptionPane.PLAIN_MESSAGE);   ;
        }


          }
 
LinkedList <RRA_COMMAND  > getNotOfficial()
{ 
    LinkedList <RRA_COMMAND  > listToSyncro=new LinkedList();
    String toSyncro="";
    try
  {
String sql="select * from APP.INVOICE WHERE NUM_CLIENT IS NULL AND EXTERNAL_DATA !='EMPTY' AND EXTERNAL_DATA LIKE'%SDC%' AND TOTAL IS NULL ORDER BY HEURE"; 
System.out.println(sql);
outResult = STATEMENT.executeQuery(sql);


   while (outResult.next())
    {
        toSyncro+="\n "+outResult.getInt(1)+"  "+outResult.getString(2) ; 
        RRA_COMMAND D=new RRA_COMMAND( "", "", outResult.getString("EMPLOYE"), "",  null);
        D.KEY_INVOICE=outResult.getString("KEY_INVOICE");
        D.ID_INVOICE=outResult.getInt(1);
        listToSyncro.add(D ); 
    }
    //  Close the resultSet
    outResult.close(); 
    //  Beginning of the primary catch block: uses errorPrint method
    }  catch (Throwable e)  {
   insertError(e+"","employe");
    }
JOptionPane.showMessageDialog(null, toSyncro,
        "INVOICE TO SYNC", JOptionPane.WARNING_MESSAGE);
 
return listToSyncro;
}
public void updateExternal(int id,String data)
{ 
    
    try
     {
         STATEMENT.execute("update app.invoice set external_data='"+data+"' where id_invoice="+id+"");
     }
     catch(SQLException e)
     {
         insertError(e+" ","external data");
     } 
}
public void updateExternalInvoice(String keyIdInvoice,  String data)
{  
    try
     {
         STATEMENT.execute("update app.invoice set external_data='"+data+"' where KEY_INVOICE='"+keyIdInvoice+"'");
         //s.execute("delete from app.sdc_fagiture where KEY_INVOICE='"+keyIdInvoice+"'  ");
     }
     catch(SQLException e)
     {
         insertError(e+" ","external data invoice");
     }     
}
public void updateExternalAnnule(String keyIdInvoice,String data)
{  
    try
     {
        STATEMENT.execute("update app.refund set external_data='"+data+"' where   KEY_INVOICE='"+keyIdInvoice+"'");
        
        
     }
    catch(SQLException e)
    {
        insertError(e+" ","external data annule");
    }     
}
public void updateExternalProforma(String keyIdInvoice,String data)
{  
    try
     {
        STATEMENT.execute("update app.proforma set external_data='"+data+"' where   KEY_INVOICE='"+keyIdInvoice+"'");
 }
    catch(SQLException e)
    {
        insertError(e+" ","external data annule");
    }     
}
public void updateEJInvoice(String keyIdInvoice,  String data)
{  
    try
     {
//         System.out.println("------------------TYTYTYTYTYT----------------- where KEY_INVOICE='"+keyIdInvoice+"'");
//           System.out.println("----connectionURL   "+connectionURL);
//            STATEMENT.execute("update app.invoice set NAME_CLIENT='NAHAGEZE' where KEY_INVOICE='"+keyIdInvoice+"'");
//        
//          outResult = STATEMENT.executeQuery("select  ID_INVOICE,heure from APP.INVOICE WHERE KEY_INVOICE='"+keyIdInvoice+"'");
//          while (outResult.next())
//          {
//              System.out.println(outResult.getInt(1)+" up ICE='  "+outResult.getString(2)+"    '"); 
//          }  outResult.close();
//             
//            
//             System.out.println("update app.invoice set NAME_CLIENT='NAHAGEZE' where KEY_INVOICE='"+keyIdInvoice+"'");
           STATEMENT.execute("update app.invoice set export='"+data+"' where KEY_INVOICE='"+keyIdInvoice+"'");
//           
//        JOptionPane.showMessageDialog(null, "NDAHAGEZE");
         
       //  s.execute("update app.invoice set export='"+data+"' where KEY_INVOICE='"+keyIdInvoice+"'");
       //     JOptionPane.showMessageDialog(null, "NDAHAGEZE");
          
      //    Thread.sleep(1000);
          
         System.out.println("update app.invoice set export='"+data+"' where KEY_INVOICE='"+keyIdInvoice+"'");
     }
     catch(SQLException e)
     {
         insertError(e+" ","updateEJInvoice");
     }     
}
public void updateExternalCopyInvoice(int id,String data)
{  
    try
     {
         STATEMENT.execute("update app.invoice set copy_data='"+data+"'  where id_invoice="+id+"");
     }
     catch(SQLException e)
     {
         insertError(e+" ","external data invoice");
     }     
}
public void updateExternalCopyAnnule(int id,String data)
{  
    try
     {
        STATEMENT.execute("update app.refund set copy_data='"+data+"' where   id_refund="+id+"");
     }
    catch(SQLException e)
    {
        insertError(e+" ","external data annule");
    }     
}
public void updateheureCopyInvoice(int id )
{  
    try
     {
         STATEMENT.execute("update app.invoice set  heure_copy=current timestamp where id_invoice="+id+"");
     }
     catch(SQLException e)
     {
         insertError(e+" ","heure data invoice");
     }     
}
public void updateheureCopyAnnule(int id )
{  
    try
     {
        STATEMENT.execute("update app.refund set  heure_copy=current timestamp where   id_refund="+id+"");
     }
    catch(SQLException e)
    {
        insertError(e+" ","external data annule");
    }     
}

RRA_INVOICE insertInvoice3(String keyIdInvoice)
{    
try
     { 
         psInsert = conn.prepareStatement("insert into APP.INVOICE "
                 + "(KEY_INVOICE )  values (? )");
         
         psInsert.setString(1,keyIdInvoice); 
         psInsert.executeUpdate();
         psInsert.close(); 
          
         return getIdFacture(keyIdInvoice);
}  
 catch (Throwable e)  
 { 
     insertError(e+"","doFinishVentein3");
 } 
 insertError(keyIdInvoice+"","insertinvoice3");
 return null;
}
RRA_INVOICE insertProforma3(String keyIdInvoice )
{    
try
     { 
         psInsert = conn.prepareStatement("insert into APP.proforma "
                 + "(KEY_INVOICE )  values (? )");
         
         psInsert.setString(1,keyIdInvoice); 
         psInsert.executeUpdate();
         psInsert.close();
         
         
          return  getIdProfo(keyIdInvoice);
 
}  
 catch (Throwable e)  
 { 
     insertError(e+"","insertProforma");
 } 
insertError(keyIdInvoice+"","insertProformasdc");
 return null;
} 

RRA_INVOICE insertAnnule3(  String key_invoice)
{
    
try
     { 
         psInsert = conn.prepareStatement("insert into APP.ANNULE "
                 + "(  numero_affilie,id_invoice)  values ( ?,?)");
          
          psInsert.setString(1," ");
          psInsert.setInt(2,-1);
          
         psInsert.executeUpdate();
         psInsert.close();
         
         
         psInsert = conn.prepareStatement("insert into APP.REFUND "
                 + "(  numero_affilie,key_invoice,id_invoice)  values (  ?,?,?)");
          
         psInsert.setString(1," ");
         psInsert.setString(2,key_invoice);
         psInsert.setInt(3,-1);
         psInsert.executeUpdate();
         psInsert.close();
         System.out.println("insert into APP.REFUND  (  numero_affilie,key_invoice)  values (  ?,?)"+key_invoice);
         return  getIdRefund(key_invoice);
}  
 catch (Throwable e)  
 { 
     insertError(e+"","doFinishVente");
 } 
 return null;
} 
 public RRA_INVOICE  getIdFacture(String keyInvoice)
  { 
      try
      {
          outResult = STATEMENT.executeQuery("select  ID_INVOICE,heure from APP.INVOICE WHERE KEY_INVOICE='"+keyInvoice+"'");
          while (outResult.next())
          {
            return(new RRA_INVOICE(outResult.getInt(1),outResult.getString(2),keyInvoice));
          }  outResult.close();
      }
      catch (Throwable e)       
      {
        insertError(e+"","getIdfacture");
      }
      insertError(keyInvoice+"","getIdfacture");
     return null; 
  }
  public RRA_INVOICE  getIdFactureID(int idInvoice)
  { 
try
{
    System.out.println("select  EXTERNAL_DATA,heure from APP.INVOICE WHERE ID_INVOICE= "+idInvoice+" ");
    outResult = STATEMENT.executeQuery("select  KEY_INVOICE,heure,EXTERNAL_DATA from APP.INVOICE WHERE ID_INVOICE= "+idInvoice+" ");
    while (outResult.next())
    { RRA_INVOICE ri =new RRA_INVOICE(idInvoice,outResult.getString(2),outResult.getString(1));
    ri.data=outResult.getString(3);
      return  ri;
    }  outResult.close();
}
      catch (Throwable e)       
      {
        insertError(e+"","getIdfacture");
      }
     return null; 
  }
    public RRA_INVOICE  getIdREFUNDID(int idInvoice)
  { 
try
{
    System.out.println("select  EXTERNAL_DATA,heure from APP.REFUND WHERE ID_REFUND= "+idInvoice+" ");
    outResult = STATEMENT.executeQuery("select  KEY_INVOICE,heure,EXTERNAL_DATA from APP.REFUND WHERE ID_REFUND= "+idInvoice+" ");
    while (outResult.next())
    { RRA_INVOICE ri =new RRA_INVOICE(idInvoice,outResult.getString(2),outResult.getString(1));
    ri.data=outResult.getString(3);
      return  ri;
    }  outResult.close();
}
      catch (Throwable e)       
      {
        insertError(e+"","getIdfacture");
      }
     return null; 
  }
 public RRA_INVOICE  getIdProfo(String keyInvoice)
  { 
      try
      {
          outResult = STATEMENT.executeQuery("select  ID_PROFORMA,heure from APP.PROFORMA"
                  + " WHERE KEY_INVOICE='"+keyInvoice+"'");
          while (outResult.next())
          {
            return(new RRA_INVOICE(outResult.getInt(1),outResult.getString(2),keyInvoice));
          }  outResult.close();
      }
      catch (Throwable e)       
      {
        insertError(e+"","getIdfacture");
      }
      insertError(keyInvoice+"","getIdProfo sdc");
     return null; 
  }
  public RRA_INVOICE  getIdRefund(String keyInvoice)
  { 
      try
      {
          outResult = STATEMENT.executeQuery("select  ID_REFUND,heure,id_invoice from APP.REFUND WHERE KEY_INVOICE='"+keyInvoice+"'");
          while (outResult.next())
          {
            return(new RRA_INVOICE(outResult.getInt(1),outResult.getString(2),keyInvoice,outResult.getInt(3)));
          }  outResult.close();
      }
      catch (Throwable e)       
      {
        insertError(e+"","getIdfacture");
      }
     return null; 
  }
 public RRA_INVOICE  getIdheureInvoice (int ID_INVOICE)
  { 
      try
      {
          outResult = STATEMENT.executeQuery("select  ID_INVOICE,heure_copy from APP.INVOICE WHERE ID_INVOICE= "
                  +ID_INVOICE+" ");
          while (outResult.next())
          {
            return(new RRA_INVOICE(outResult.getInt(1),outResult.getString(2),""));
          }  outResult.close();
      }
      catch (Throwable e)       
      {
        insertError(e+"","getIdfacture");
      }
     return null; 
  }
 public RRA_INVOICE  getIdheureRefund (int ID_REFUND)
  { 
      try
      {
          outResult = STATEMENT.executeQuery("select  ID_REFUND,heure_copy from APP.REFUND WHERE ID_REFUND= "
                  +ID_REFUND+" ");
          while (outResult.next())
          {
            return(new RRA_INVOICE(outResult.getInt(1),outResult.getString(2),""));
          }  outResult.close();
      }
      catch (Throwable e)       
      {
        insertError(e+"","getIdfacture");
      }
     return null; 
  }
 public double getTvaRate( String rate) {
        
        try {
outResult = STATEMENT.executeQuery("select  * from APP.VARIABLES "
        + "where APP.variables.famille_variable= 'RRA' AND NOM_VARIABLE LIKE '%taxrate%' "
        + "AND VALUE_VARIABLE='"+rate+"'");
     while (outResult.next()) { 
               return Double.parseDouble(outResult.getString(4))/100.0;
            }
        } catch (Throwable e) {
            insertError(e + "", "getparamFam");
        }
        return 0;
    }
  public LinkedList<RRA_PRODUCT>  getUrutonde( String keyInvoice)
  { 
      LinkedList<RRA_PRODUCT> ur=new LinkedList<RRA_PRODUCT> ();
      try
      {
         outResult = STATEMENT.executeQuery("select * from APP.SDC_URUTONDE  where "
                 + "   key_invoice='"+keyInvoice+"' ");
         
         while(outResult.next())
         { 
            ur.add(new RRA_PRODUCT(outResult.getString("code_uni"),outResult.getString("NAME_PRODUCT")
                     ,outResult.getDouble("price"),outResult.getDouble("tva"),outResult.getInt("QUANTITE") ));
         }
         outResult.close();
      }
      catch (Throwable e)       
      {
        insertError(e+"","getIdfacture");
      }
     return ur; 
  }

public LinkedList<RRA_PRODUCT>  getListREFUND( String key)
  { 
      LinkedList<RRA_PRODUCT> ur=new LinkedList<RRA_PRODUCT> ();
      try
      {
         outResult = STATEMENT.executeQuery("select * from APP.REFUND_LIST  where "
                 + "   key_invoice='"+key+"' ");
         System.out.println("select * from APP.REFUND_LIST  where "
                 + "   key_invoice='"+key+"' ");
         while(outResult.next())
         { 
            ur.add(new RRA_PRODUCT(outResult.getString("code_uni"),outResult.getString("NAME_PRODUCT")
                     , outResult.getDouble("price"),outResult.getDouble("tva"),outResult.getInt("QUANTITE") ));
         }
         outResult.close();
      }
      catch (Throwable e)       
      {
        insertError(e+"","getIdfacture");
      }
     return ur; 
  }
 
String getV( String nom)
{ 
try{ 
outResult = STATEMENT.executeQuery("select  * from APP.VARIABLES where APP.variables.nom_variable= '"+nom+"RRA'   ");
while (outResult.next())
{  return outResult.getString(4);  }
}
catch (Throwable e){ insertError(e+"","getparm autoout"); }
return     "";
}
String getString(String ss)
{
try
{
outResult = STATEMENT.executeQuery("select  * from APP.VARIABLES where APP.variables.nom_variable= '"+ss+"MAIN'  and APP.variables.famille_variable= 'RUN' ");
//System.out.println("select  * from APP.VARIABLES where APP.variables.nom_variable= '"+nom+"'  and APP.variables.famille_variable= '"+famille+"' ");

while (outResult.next())
{
  //  System.out.println(outResult.getString(2));
        String num =outResult.getString(1);
        String value =outResult.getString(2);
        String fam =outResult.getString(3);
        String value2 =outResult.getString(4); 
        return value2;
}}
catch (Throwable e){
      insertError(e+"","getparm autoout");
}
    return "";
} 
 
 int count(String table,String col,String contain,String periode)
 { String sql="select  count("+col+") from APP."+table+" where "
            + " "+contain+ " and "+periode;
 try
 {       
     outResult = STATEMENT.executeQuery(sql);
      while (outResult.next())
        { return outResult.getInt(1);   } 
 }
 catch(SQLException e)
 {System.err.println(sql); 
    insertError("count", " "+e);
 }
 return 0;
 }
LinkedList  <RRA_PRODUCT> getPLU(String hera, String geza,double [] taxes)
{ 
LinkedList  <RRA_PRODUCT> prod=new LinkedList();
String sql = "select min (id_invoice),max (id_invoice) from APP.INVOICE where"
        + " invoice.HEURE >'"+hera+"' AND invoice.HEURE <'"+geza+"'   "; 
try
 {   
int min_id_invoice=0;
int max_id_invoice=0;
System.err.println(sql); 
outResult = STATEMENT.executeQuery(sql);
while (outResult.next())
    { 
    min_id_invoice=outResult.getInt(1)-1;
    max_id_invoice=outResult.getInt(2)+1; 
    }

sql ="select  code_uni,SUM(quantite) as quantite,list.tva,price,name_product from APP.LIST,APP.PRODUCT  "
         + " WHERE ID_INVOICE>"+min_id_invoice+" and  ID_INVOICE<"+max_id_invoice+" "
         + " and code_uni=code GROUP BY  name_product,CODE_UNI,PRICE,list.tva order by name_product " ;

outResult = STATEMENT.executeQuery(sql); 
 RRA_PRODUCT rp=null;
while (outResult.next())
{  
String cat="A";
double tva=outResult.getDouble(3);
if(tva==taxes[0])
    cat="A-EX";
else if(tva==taxes[1])
    cat="B";
 else if(tva==taxes[2])
    cat="C";
else if(tva==taxes[3])
    cat="D";
rp=new RRA_PRODUCT(outResult.getString(1) ,outResult.getString(5) ,outResult.getDouble(4),
tva,outResult.getInt(2),cat,0 ) ;
prod.add(rp);

}
for(int i=0;i < prod.size();i++)
{
    
RRA_PRODUCT rpp=prod.get(i);    
sql ="select SUM(qty_live)   from APP.LOT  "
   + " WHERE   code_lot='"+rpp.productCode+"'   " ;

outResult = STATEMENT.executeQuery(sql); 
while (outResult.next())
{   
prod.get(i).onStock=outResult.getInt(1);
} 
}    
}
 catch(SQLException e)
 {System.err.println(sql); 
    insertError("PLU", " "+e);
 }
 return prod;
 }
    double sum(String table,String col,String contain,String periode)
 {  String sql="select  sum("+col+") from APP."+table+" where "
            + " "+contain+ " and "+periode;
  System.out.println(sql);
 try
 {       
     outResult = STATEMENT.executeQuery(sql);
      while (outResult.next())
        { return outResult.getDouble(1);   } 
 }
 catch(SQLException e)
 {System.err.println(sql); 
    insertError("sum", " "+e);
 }
 return 0;
 }
LinkedList<String> clients ()
{ 
    LinkedList<String> clien  =new LinkedList();
    try
    { 
    outResult = STATEMENT.executeQuery("select NOM_VARIABLE from APP.VARIABLES WHERE FAMILLE_VARIABLE='CLIENT' and NOM_VARIABLE!='CASHNORM' order by NOM_VARIABLE ");
 
    while (outResult.next())
    {
      clien.add(outResult.getString(1));
    } 
    //  Close the resultSet
outResult.close();

      }  catch (Throwable e)  {

   insertError(e+""," V getlot");
    }
 
return clien;

}
 String getRRA(  String nom)
{ 
try
{
outResult = STATEMENT.executeQuery("select  * from APP.VARIABLES where APP.variables.nom_variable= '"+nom+"RRA'   ");
 while (outResult.next())
{
 return outResult.getString(4); 
}}
catch (Throwable e){
      insertError(e+"","getparm autoout");
 }
return  "";
}
 public RRA_MRC getRRA_MRC(String  SQL)
{
    String sdcip="";
 if(SQL!=null && !SQL.equals("NEW") && !SQL.equals("SELECT"))
 {  try
    { 
outResult = STATEMENT.executeQuery("select  * from APP.SDC_MRC  "+SQL+"   ");
 while (outResult.next())
{ 
    sdcip=""+outResult.getString("SDC_IP");
return new RRA_MRC (   outResult.getString("MRC"),outResult.getString("MAC_ADDRESS"),    outResult.getString("MACHINE_NAME"), 
        outResult.getString("COMPANY_NAME"),   outResult.getString("MAIN_USER"), 
        outResult.getString("DPT"),sdcip);
}
outResult.close();
 }  catch (Throwable e)  {
    System.out.println(e); 
 }
 }
 else if(SQL!=null &&  SQL.equals("SELECT"))
 {
 try
    { 
outResult = STATEMENT.executeQuery("select  * from APP.SDC_MRC   ");
LinkedList <RRA_MRC> LIST= new LinkedList();
 while (outResult.next())
{ 
    sdcip=""+outResult.getString("SDC_IP");
LIST.add(new RRA_MRC (   outResult.getString("MRC"),outResult.getString("MAC_ADDRESS"),    outResult.getString("MACHINE_NAME"), 
        outResult.getString("COMPANY_NAME"),   outResult.getString("MAIN_USER"), 
        outResult.getString("DPT"),sdcip ) );
}
 outResult.close();
 RRA_MRC  M=  (RRA_MRC) JOptionPane.showInputDialog(null, "SELECT", " ",
JOptionPane.QUESTION_MESSAGE, null, LIST.toArray(), " ");
 return  M; 

 }  catch (Throwable e)  {
    System.out.println(e); 
 }
 }
  else
    {
         String name="";   
        try {
            String [] sp=(""+InetAddress.getLocalHost()).split("/");
            name=sp[0];
        } catch (Exception ex) {
            Logger.getLogger(RRA_DB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new RRA_MRC ( getNextRRA_MRC(),RRA_MRC.getMac(),name ,"","","",sdcip );
    }  
   return new RRA_MRC ( getNextRRA_MRC(),RRA_MRC.getMac(),"" ,"","","",sdcip ); 
 }
 
  public String getNextRRA_MRC()
{
   try
    { 
 RRA_MRC last=null;
outResult = STATEMENT.executeQuery("select  * from APP.SDC_MRC order by mrc  ");
 while (outResult.next())
{ 
last = new RRA_MRC (   outResult.getString("MRC"),outResult.getString("MAC_ADDRESS"),    outResult.getString("MACHINE_NAME"), 
        outResult.getString("COMPANY_NAME"),   outResult.getString("MAIN_USER"), 
        outResult.getString("DPT"),outResult.getString("SDC_IP") );
}
outResult.close();
if(last==null)
{return "";}
else
{
   try
   {   //ALG01000001 
       //BBBCCNNNNNN
       //01234567890        .
       //"hamburger".substring(4, 8) returns "urge"
       // 0123456789
       System.out.println(last.MRC);
       System.out.println(last.MRC.substring(5, 10));
   int getLastSerialNumber=Integer.parseInt(last.MRC.substring(5, 11))+1;
   System.out.println(getLastSerialNumber);
   String d=""+getLastSerialNumber;
   String debut=last.MRC.substring(0, 5);
   String zeros="";
   int zerotoadd=6-d.length();
   
   for(int i=0;i<zerotoadd;i++)
   {zeros+="0";}
   
   return debut+zeros+d;
   }
     catch (Throwable e)  {
         JOptionPane.showMessageDialog(null, last.MRC+" HAS AN ERROR IN INCREMENTATION");
    System.out.println(e); 
    return "" ;
 }
}

}  catch (Throwable e)  {
    System.out.println(e); 
 }
  return "" ; 
 }
 
 public void insertRRA_MRC(RRA_MRC m)
{
     try
    { 
psInsert = conn.prepareStatement("insert into APP.SDC_MRC"
        + " (MRC,MACHINE_NAME,COMPANY_NAME ,MAIN_USER,DPT,MAC_ADDRESS)"
        + " values (?,?,?,?,?,?)"); 
psInsert.setString(2,m.MACHINE_NAME);
psInsert.setString(1,m.MRC);
psInsert.setString(3,m.COMPANY_NAME);
psInsert.setString(4,m.MAIN_USER); 
psInsert.setString(5,m.DPT );
psInsert.setString(6,m.MAC ); 
psInsert.executeUpdate();
psInsert.close();
    JOptionPane.showMessageDialog(null," SDC_MRC ADDED SUCCESSFULLY"," MURAKOZE ",JOptionPane.PLAIN_MESSAGE);
 }  catch (Throwable e)  {
 JOptionPane.showMessageDialog(null," SDC_MRC ishobore kuba arimo  "+e," DUPLICAGE ",JOptionPane.ERROR_MESSAGE);
  }
 }
 public void upDateRRA_MRC(RRA_MRC m)
 {
try 
{ 
    Statement sU = conn.createStatement();
    String createString1 = " update SDC_MRC set"
            + " MACHINE_NAME='" + m.MACHINE_NAME + "',"
             + " MAIN_USER='" + m.MAIN_USER + "',"
             + " COMPANY_NAME='" + m.COMPANY_NAME + "',"
            + " MAC_ADDRESS='" + m.MAC + "',"
            + " DPT='" + m.DPT + "', "
            + " SDC_IP='" + m.SDC_IPL + "' "
            + " where MRC='"+m.MRC+"'";
    sU.execute(createString1);
    JOptionPane.showMessageDialog(null," sdc_MRC UPDATED SUCCESSFULLY "," MURAKOZE ",JOptionPane.PLAIN_MESSAGE);

} catch (SQLException ex) {

    JOptionPane.showMessageDialog(null,"  "+ex," UpDate RRA_MRC Fail " ,JOptionPane.ERROR_MESSAGE);
insertError(ex+"","update");     } 
}
public void deleteRRA_MRC(String mrc)
{
try 
{ 
int n=JOptionPane.showConfirmDialog(null, "DO YOU WANT TO DELETE "+ mrc,
 "INVOICE TO SYNC", JOptionPane.WARNING_MESSAGE);
    
if(n==0)
{
    Statement sU = conn.createStatement();
    String createString1 = " delete from SDC_MRC  where MRC='"+mrc+"'";
    sU.execute(createString1);
    JOptionPane.showMessageDialog(null," sdc_MRC DELETED SUCCESSFULLY "," MURAKOZE ",JOptionPane.PLAIN_MESSAGE);
}
} catch (SQLException ex) {

    JOptionPane.showMessageDialog(null,"  "+ex," UpDate RRA_MRC Fail " ,JOptionPane.ERROR_MESSAGE);
insertError(ex+"","update");     } 
}

LinkedList<RRA_RECORD> getRecs (String sql)
{ 
LinkedList <RRA_RECORD>recs = new LinkedList();
 try
 { 
     System.out.println(sql);
 outResult = STATEMENT.executeQuery( sql);
 
    while (outResult.next())
    {
       
       String EXT=outResult.getString("EXTERNAL_DATA");
       String COPY=outResult.getString("COPY_DATA");  
          String COPY_HEURE="";
       
          if(!sql.contains("PROFORMA"))
          {
          COPY_HEURE=outResult.getString("HEURE_COPY");
          }
       
       String RTYPE=getRtype(  EXT,  COPY);
recs.add(new RRA_RECORD
(outResult.getInt(1) ,RTYPE,outResult.getString("NUM_CLIENT") , outResult.getString("EMPLOYE") ,
outResult.getString("HEURE") , EXT , COPY   ,outResult.getString("MRC") ,outResult.getString("TIN") ,
 outResult.getDouble("TOTAL") , outResult.getDouble("TVA")   , outResult.getDouble("TOTAL_A")  , 
 outResult.getDouble("TOTAL_B")  ,  outResult.getDouble("TOTAL_C")  , outResult.getDouble("TOTAL_D")   ,
 outResult.getDouble("TAXE_A") , outResult.getDouble("TAXE_B") , outResult.getDouble("TAXE_C")   , 
 outResult.getDouble("TAXE_D"), COPY_HEURE) );
    } 
    //  Close the resultSet
outResult.close(); 
 }  catch (Throwable e)  {
 insertError(e+""," V getlot");
 }
 
return recs; 
}
LinkedList <RRA_TRACK > getTRACK (String sql)
{ 
LinkedList <RRA_TRACK>recs = new LinkedList();
 try
 { 
     System.out.println(sql);
 outResult = STATEMENT.executeQuery( sql);
 
    while (outResult.next())
    {  
recs.add(new RRA_TRACK
(outResult.getInt("ID_TRACKING") , outResult.getString("HEURE_TRACKING") , 
 outResult.getString("SENT_TRACKING") ,outResult.getString("RECEIVED_TRACKING") , 
 outResult.getString("KEY_INVOICE") ,outResult.getString("EMPLOYE") ) );
    } 
    //  Close the resultSet
outResult.close(); 
 }  catch (Throwable e)  {
 insertError(e+""," V getlot");
 }
 
return recs; 
}
String getRtype(String EXT,String COPY)
{ 
    String RTYPE="";
     if(EXT.contains("NS"))
       {
       if(COPY.contains("CS"))
           RTYPE="NS + CS";
       else
           RTYPE="NS";
       }
       else if(EXT.contains("NR"))
       {
       if(COPY.contains("CR"))
           RTYPE="NR + CR";
       else
           RTYPE="NR";
       } 
       else if(EXT.contains("TS"))
       {RTYPE="TS";}
       else if(EXT.contains("TR"))
       {RTYPE="TR";}
       else if(EXT.contains("PS"))
       {RTYPE="PS";}  
     
return RTYPE;

}
public void photoCopyR(RRA_RECORD rec ) 
{
 LinkedList <RRA_PRODUCT> prod2=new LinkedList();
   
  String external_data="external_data"; 
  String time="";
 String tin="";
  if(rec.RTYPE.contains("C"))
      external_data="COPY_data";
  
String sql="select  num_client, code_uni ,quantite,refund_list.price,refund_list.original_price,"
        + "name_product,refund_list.tva, "+external_data+", heure,tin from "
        + " APP.REFUND ,APP.refund_list  where       APP.REFUND.id_REFUND = " +
        rec.ID+" and APP.REFUND.id_REFUND = APP.refund_list.id_REFUND   order by  name_product";
  System.out.println(sql);
try {
outResult = STATEMENT.executeQuery(sql);
       
 
while (outResult.next()) {
String    client = outResult.getString(1); 
String   code = outResult.getString(2); 
int   quantite = outResult.getInt(3);
double  price = outResult.getDouble(4);
double  orig = outResult.getDouble(5);
String     desi = outResult.getString(6); 
if(price!=orig)
desi += "# Discount -"+(int)(100-Math.abs((price*100)/orig))+"%";
//
//System.out.println(desi+"      "+price+" **************************************** "+orig+"    "+Math.abs((price*100)/orig)+"      "+(100-Math.abs((price*100)/orig)));
double  tva = outResult.getDouble(7); 
external_data=outResult.getString(8);
time=outResult.getString(9); 
tin=outResult.getString("tin");
RRA_PRODUCT pro=new RRA_PRODUCT( desi, code, quantite, price,orig, tva );
  
prod2.add(pro);
} 

outResult.close();  

 } catch (SQLException ex) {
System.out.println(ex);
}
LinkedList<String> infoClient=Leclient(getClient(rec.ID));
String Rtype=rec.RTYPE.substring(0, 1), Ttype=rec.RTYPE.substring(1, 2);  
RRA_PACK rp = getRpack4(rec, prod2 , Rtype, Ttype,getNowTime2(time));  
 if(rp!=null)
 {
     rp.ClientTin=tin;
     printRRA(external_data,Rtype, Ttype, rp.getProductList, rp, rec.ID,0,infoClient);
 }   
 else
 {JOptionPane.showMessageDialog(null,"ERROR IN BUILDING PACK TO PRINT");
 }  
}
public void photoCopyS(RRA_RECORD rec ) 
{
 LinkedList < RRA_PRODUCT> prod2=new LinkedList();
   
  String external_data="external_data"; 
  String time=""; 
 String tin="";
  if(rec.RTYPE.contains("C"))
      external_data="COPY_data";
  
String sql=""
+ "select  num_client, code_uni ,quantite,list.price,list.original_price,"
+ "name_product,list.tva, "+external_data+", heure,tin "
+ "from APP.PRODUCT,APP.INVOICE ,APP.LIST "
+ " where     "
+ "  APP.INVOICE.id_invoice = " + rec.ID
+ " and APP.INVOICE.id_invoice = APP.LIST.id_invoice  and product.code=code_uni  order by  name_product";
 System.out.println(sql);
try {
outResult = STATEMENT.executeQuery(sql); 
 
while (outResult.next()) {
String    client = outResult.getString(1); 
String   code = outResult.getString(2); 
int   quantite = outResult.getInt(3);
double  price = outResult.getDouble(4);
double  orig = outResult.getDouble(5);
String     desi = outResult.getString(6); 
if(price!=orig)
desi += "# Discount -"+(int)(100-Math.abs((price*100)/orig))+"%";
double  tva = outResult.getDouble(7); 
external_data=outResult.getString(8);
time=outResult.getString(9); 
tin=outResult.getString("tin");
RRA_PRODUCT pro=new RRA_PRODUCT( desi, code, quantite, price,orig, tva ); 
prod2.add(pro);
} 
outResult.close();  

 } catch (SQLException ex) {
System.out.println(ex);
}
LinkedList<String> infoClient=Leclient(getClient(rec.ID));
String Rtype=rec.RTYPE.substring(0, 1), Ttype=rec.RTYPE.substring(1, 2);  
RRA_PACK rp = getRpack4(rec, prod2,  Rtype, Ttype,getNowTime2(time));  
 if(rp!=null)
 {
     rp.ClientTin=tin;
     printRRA(external_data,Rtype, Ttype, rp.getProductList, rp, rec.ID,0,infoClient);
 }   
 else
 {JOptionPane.showMessageDialog(null,"ERROR IN BUILDING PACK TO PRINT");
 }
 
}
static public String getNowTime2(String heure)
{
    //2013-12-18 09:33:50.263
    //0123456789012345678 
    if(heure!=null && heure.length()>20)
    {String heureInvoice=heure.substring(8, 10)+"/"+heure.substring(5, 7)+"/"+heure.substring(0, 4)
            +" "+heure.substring(11, 13)+":"+heure.substring(14, 16)+":"+heure.substring(17, 19);
return heureInvoice;
    }
    else
        return "NOT A TIME"+heure;
}
  RRA_PACK getRpack4(RRA_RECORD rec, LinkedList <RRA_PRODUCT> getProductList,
        String time, String rtype, String ttype)
{
double totalpriceA, totalpriceB, totalpriceC, totalpriceD,
          tvaPriceA, tvaPriceB, tvaPriceC, tvaPriceD;
     double tvaRateA, tvaRateB, tvaRateC, tvaRateD;
     totalpriceA=0; totalpriceB=0; totalpriceC=0; totalpriceD=0;
          tvaPriceA=0; tvaPriceB=0; tvaPriceC=0; tvaPriceD=0;
       tvaRateA=0;tvaRateB=0; tvaRateC=0; tvaRateD=0;
tvaRateA= getTvaRate("A");
tvaRateB= getTvaRate("B");
tvaRateC= getTvaRate("C");
tvaRateD= getTvaRate("D");  
   
   for (int i = 0; i < getProductList.size(); i++) {

                RRA_PRODUCT p2 =  getProductList.get(i); 
                
                double taxable =((p2.salePrice()) / (1 + p2.tva));
//                if(taxable<0)
//                    arr=-0.5;
//                else
//                    arr=0.5;
                 
                if(p2.tva==tvaRateA)
                {tvaPriceA+=(taxable * p2.tva);
                totalpriceA+=(p2.salePrice());
                getProductList.get(i).classe="A-EX";
                }
                else if(p2.tva==tvaRateD)
                {tvaPriceD+=(taxable * p2.tva);
                totalpriceD+=(p2.salePrice());
                getProductList.get(i).classe="D";
                }
                else if(p2.tva==tvaRateB)
                {tvaPriceB+=((taxable * p2.tva));
                totalpriceB+=(p2.salePrice());
                getProductList.get(i).classe="B";
                }
                else if(p2.tva==tvaRateC)
                {tvaPriceC+=(taxable * p2.tva);
                totalpriceC+=(p2.salePrice());
                getProductList.get(i).classe="C";
                }
                else
                {
                 //   return null;
                }
}
if(rec.MRC==null)
 {
 JOptionPane.showMessageDialog(null, "NO MRC FOUND ","ATTENTION",JOptionPane.ERROR_MESSAGE);  
 return null;
 }
   
  return new RRA_PACK(rec.ID, 
          totalpriceA, totalpriceB, totalpriceC, totalpriceD,
          tvaPriceA, tvaPriceB, tvaPriceC, tvaPriceD,
          tvaRateA*100, tvaRateB*100, tvaRateC*100, tvaRateD*100,
          rtype,  ttype,rec.MRC,rec.TIN,getProductList, time);
}
  String getRate(double tva)
{  
                if( tva==tvaRateA)
                { 
                 return "A-EX";
                }
                else if( tva==tvaRateD)
                { 
                 return "D";
                }
                else if( tva==tvaRateB)
                { 
                return "B";
                }
                else if( tva==tvaRateC)
                { 
                return "C";
                }
                else
                {
                    return "";
                }
  
}
  boolean printRRA(String data,String rtype,String ttype,LinkedList< RRA_PRODUCT> prod2,
          RRA_PACK rp,int id_invoice,int id_ref,LinkedList<String>infoClient)
{
    int neg=1;
    if(ttype.equals("R"))
        neg=-1;
    data = data.replaceAll(",,", ",#,#");
    String[] sp = data.split(",");
    if(sp.length!=7)
    {
        return false;        
    }
    else
    {
 new RRA_PRINT3(getRRA("company"), getRRA("bp"), "KIGALI", getRRA("tin"), ""+rp.ClientTin,
(rp.getTotal()),
(rp.getTotalA()), (rp.getTotalB()),
rp.getTotalTaxes(), rp.getTotalTaxes(), rp.getTotal(),  sp[0], sp[1] + "/" + sp[2] + "  " + sp[3],
sp[6], sp[5], id_invoice, getRRA("mrc"), prod2,rtype, ttype,getRRA("version"),neg,id_ref,
         infoClient,rp.Mrctime, sp[4]);
 
        return true;
    }
}
  RRA_CLIENT getClient2(String key )
{
    RRA_CLIENT infoClient=null;
    String client="";
    String   num_affiliation="";
    String  nom_client="";  
   // String   numero_quitance  ="";
    int percentage=0;
    String prenom_client="";
    String employeur="";
    try
{
// Clients Societe
    
    String sql="" +
"select num_client,client.num_affiliation,'',client.percentage,nom_client,prenom_client,"
         + "SDC_FAGITURE.key_invoice from  APP.SDC_FAGITURE,client "
+ "where client.num_affiliation=SDC_FAGITURE.num_client and  key_invoice='"+key+"' " ;
    System.out.println(sql  );
 
 outResult = STATEMENT.executeQuery( sql );
 
       
    while (outResult.next())
    { 
        client=outResult.getString("num_client");
        num_affiliation=outResult.getString(2);
        nom_client=outResult.getString("nom_client"); 
       // numero_quitance  =outResult.getString("reference"); 
        percentage=outResult.getInt("percentage");
        prenom_client=outResult.getString("prenom_client");
        infoClient=new RRA_CLIENT(num_affiliation, nom_client, prenom_client,percentage, 0, ""+client, "", "", "", ""+num_affiliation);
        return infoClient;
    }
    outResult.close();
     
    }
    catch (Throwable e)  
    {
        insertError(e+"","retour ");
    }
    return infoClient;
}  

RRA_CLIENT getClient(int id_invoice)
{
    RRA_CLIENT infoClient=null;
    String client="";
    String   num_affiliation="";
    String  nom_client="";  
    String   numero_quitance  ="";
    int percentage=0;
    String prenom_client="";
    String employeur="";
    try
{
// Clients Societe
    System.out.println("" +
             "select num_client,invoice.num_affiliation,reference,client.percentage,nom_client,prenom_client,invoice.key_invoice from  APP.INVOICE,client "
         + "where client.num_affiliation=invoice.num_affiliation and  APP.INVOICE.id_invoice = "+ id_invoice  );
 
 outResult = STATEMENT.executeQuery("" +
             "select num_client,invoice.num_affiliation,reference,client.percentage,nom_client,prenom_client,invoice.key_invoice from  APP.INVOICE,client "
         + "where client.num_affiliation=invoice.num_affiliation and  APP.INVOICE.id_invoice = "+ id_invoice  );
 
       
    while (outResult.next())
    { 
        client=outResult.getString("num_client");
        num_affiliation=outResult.getString(2);
        nom_client=outResult.getString("nom_client"); 
        numero_quitance  =outResult.getString("reference"); 
        percentage=outResult.getInt("percentage");
        prenom_client=outResult.getString("prenom_client");
           JOptionPane.showMessageDialog(null, "nom_client "+nom_client);
        infoClient=new RRA_CLIENT(num_affiliation, nom_client, prenom_client,percentage, 0, ""+client, "", "", "", ""+num_affiliation);
        return infoClient;
    }
    outResult.close();
     
    }
    catch (Throwable e)  
    {
        insertError(e+"","retour ");
    }
    return infoClient;
}

RRA_CLIENT getClientRef(int id_invoice)
{
    RRA_CLIENT infoClient=null;
    String client="";
    String   num_affiliation="";
    String  nom_client="";  
    String   numero_quitance  ="";
    int percentage=0;
    String prenom_client="";
    String employeur="";
    try
{
// Clients Societe
    System.out.println("" +
             "select num_client,num_affiliation,reference,client.percentage,nom_client,prenom_client,invoice.key_invoice from  APP.INVOICE,client "
         + "where client.num_affiliation=invoice.num_affiliation and  APP.INVOICE.id_invoice = "+ id_invoice  );
 
 outResult = STATEMENT.executeQuery("" +
             "select num_client,invoice.num_affiliation,reference,client.percentage,nom_client,prenom_client,invoice.key_invoice from  APP.INVOICE,client "
         + "where client.num_affiliation=invoice.num_affiliation and  APP.INVOICE.id_invoice = "+ id_invoice  );
 
       
    while (outResult.next())
    { 
        client=outResult.getString("num_client");
        num_affiliation=outResult.getString(2);
        nom_client=outResult.getString("nom_client"); 
        numero_quitance  =outResult.getString("reference"); 
        percentage=outResult.getInt("percentage");
        prenom_client=outResult.getString("prenom_client");
        infoClient=new RRA_CLIENT(num_affiliation, nom_client, prenom_client,percentage, 0, ""+client, "", "", "", ""+num_affiliation);
        return infoClient;
    }
    outResult.close(); 
    }
    catch (Throwable e)  
    {
        insertError(e+"","retour ");
    }
    return infoClient;
}
public LinkedList<String> Leclient(RRA_CLIENT infoClient)
{
    LinkedList<String> Clientinfo=new LinkedList<String>();
    if(infoClient!=null)
    {
        Clientinfo.add(infoClient.NUM_AFFILIATION);
        Clientinfo.add(infoClient.NOM_CLIENT);
        Clientinfo.add(infoClient.PRENOM_CLIENT);
        Clientinfo.add(infoClient.ASSURANCE);
        Clientinfo.add(""+infoClient.PERCENTAGE);
        Clientinfo.add(infoClient.EMPLOYEUR); 
        return Clientinfo;
    }
          
    return null;
} 
 LinkedList<RRA_INVOICE> getRRA_INVOICE(String debut, String fin)
 {
      LinkedList<RRA_INVOICE>vars=new LinkedList();
 try
 {
    
    outResult = STATEMENT.executeQuery("select  * from APP.invoice where  "
            + "APP.invoice.heure<'"+fin+"'  and app.invoice.heure>'"+debut+"' and external_data like '%,NS,%' "); 
        while (outResult.next())
        {
          //  System.out.println(outResult.getString(2));
                String heure =outResult.getString("heure");
                int id_invoice =outResult.getInt("id_invoice");
                String external_data =outResult.getString("external_data");
                double amount =outResult.getDouble("total"); 
   //SDC002000934,193,256,NS,17/04/2014 07:37:59,3N3XQKZRF5JFJVTY,QEVKRVF66GBX5PA35DL5JRGHL4
                String [] sp=external_data.split(",");
              //  System.out.println(external_data);
                if(sp!=null && sp.length>5)
                {
                id_invoice=Integer.parseInt(sp[1]);
                external_data=sp[0]; 
                }
                RRA_INVOICE ri=new RRA_INVOICE(  id_invoice, external_data,  amount);
          //      System.out.println(external_data+"---------     -------  "+ri.toString2());
                vars.add(ri);
        }
        
      
         
 }
 catch(SQLException e)
 {
     System.out.println(e);
 }
 return vars;
 }
int getIdInvoiceFailed(String seq1, String seq2)
 { 
     int id_invoice1 =0;
    int id_invoice2 =0;
 try
 { 
    
    outResult = STATEMENT.executeQuery("select  * from APP.invoice where  "
            + "  external_data like '%"+seq1+"%' ");  
//    System.out.println("select  * from APP.invoice where  "
//            + "  external_data like '%"+seq1+"%' ");
//    System.out.println("select  * from APP.invoice where  "
//            + "  external_data like '%"+seq2+"%' ");
        while (outResult.next())
        {
         id_invoice1 =outResult.getInt("id_invoice"); 
   //SDC002000934,193,256,NS,17/04/2014 07:37:59,3N3XQKZRF5JFJVTY,QEVKRVF66GBX5PA35DL5JRGHL4 
        }
        outResult = STATEMENT.executeQuery("select  * from APP.invoice where  "
            + "  external_data like '%"+seq2+"%' ");  
        while (outResult.next())
        {
         id_invoice2 =outResult.getInt("id_invoice"); 
   //SDC002000934,193,256,NS,17/04/2014 07:37:59,3N3XQKZRF5JFJVTY,QEVKRVF66GBX5PA35DL5JRGHL4 
        }    
      if(id_invoice2-id_invoice1==2)
      {
      return id_invoice2-1;
      }
        
         
 }
 catch(SQLException e)
 {
     System.out.println(e);
 }
 return id_invoice1+1;
 }
RRA_SDC_FAGITURE getSDCFagiture(String key_invoice)
{ 
    
    try
  {
     String sql="select * from APP.SDC_FAGITURE WHERE KEY_INVOICE='"+key_invoice+"' "; 
//String sql="select *  from APP.INVOICE where num_client is null order by  app.invoice.id_invoice";
System.out.println(sql);
outResult = STATEMENT.executeQuery(sql);


   while (outResult.next())
    {
        String num_client=""+outResult.getString("NUM_CLIENT");
        String date=""+outResult.getString("DATE");
        double tot=outResult.getDouble("TOTAL");
        String employe=""+outResult.getString("EMPLOYE");
        double tva=outResult.getDouble("TVA"); 
        double cash=outResult.getDouble("SOLDE"); 
        double credit=outResult.getDouble("PAYED");
        String tosend=""+outResult.getString("TOSEND");
        
        return (new RRA_SDC_FAGITURE(num_client, date, tot, employe, tva, cash, credit, key_invoice,tosend));
    } 
    outResult.close();  
    }  catch (Throwable e)  
    {
   insertError(e+"","employe");
    }
//JOptionPane.showMessageDialog(null, toSyncro,
//        "INVOICE TO SYNC", JOptionPane.WARNING_MESSAGE);
 
return null;
}
public void updateFailedInvoice(RRA_SDC_FAGITURE fag)
{  
    try
     {
         STATEMENT.execute("update app.invoice set total="+fag.TOTAL+", tva="+fag.TVA+",comptabilise='FAILED'"
                 + " ,TOTAL_A="+fag.TOTAL_A+",TOTAL_B="+fag.TOTAL_B+",TOTAL_C="+fag.TOTAL_C+",TOTAL_D="+fag.TOTAL_C+""
                 + " ,TAXE_A="+fag.TAX_A+",TAXE_B="+fag.TAX_B+",TAXE_C="+fag.TAX_C+",TAXE_D="+fag.TAX_D+""
                 + " ,MRC='"+fag.MRC+"', TIN='"+fag.TIN+"' "
                 + " where KEY_INVOICE='"+fag.KEY_INVOICE+"' AND ID_INVOICE="+fag.ID_INVOICE+"");
         //s.execute("delete from app.sdc_fagiture where KEY_INVOICE='"+keyIdInvoice+"'  ");
     }
     catch(SQLException e)
     {
         insertError(e+" ","update failedinvoice");
     }     
}

 
LinkedList <Integer> getIdInvoiceFailedList()
 { 
LinkedList <Integer> list= new LinkedList ();  
 try
 { 
    outResult = STATEMENT.executeQuery("select  * from APP.invoice where  "
            + "  comptabilise ='FAILED' "); 
    while (outResult.next())
        {
        list.add(outResult.getInt("id_invoice"));
        }
        
 }
 catch(SQLException e)
 {
     System.out.println(e);
 }
 return list;
 }
public RRA_PACK getSaleToRefund(int id_invoice) 
{ 
String st=null; 
String listUpdateSql = " SELECT  ID_INVOICE,DATE, TOTAL , EMPLOYE,TVA , REDUCTION, TOTAL_A, TOTAL_B , TOTAL_C , TOTAL_D "
        + " , TAXE_A, TAXE_B, TAXE_C, TAXE_D, MRC, TIN, CASH, CREDIT  FROM APP.INVOICE where id_invoice="+id_invoice+"";
   System.out.println(listUpdateSql);
try
 { 
    outResult = STATEMENT.executeQuery( listUpdateSql); 
        while (outResult.next())
        {
          return new  RRA_PACK( outResult.getInt("ID_INVOICE"),
            outResult.getDouble("TOTAL_A"), outResult.getDouble("TOTAL_B"), outResult.getDouble("TOTAL_C")
                    ,outResult.getDouble("TOTAL_D"),
           outResult.getDouble("TAXE_A"),outResult.getDouble("TAXE_B"), outResult.getDouble("TAXE_C")
                    , outResult.getDouble("TAXE_D"),
              tvaRateA, (tvaRateB*100),   tvaRateC,   tvaRateD, 
              "N", "R", outResult.getString("MRC") ,RRA_SDC.TIN,
            null,"");

     }  
 }
 catch(SQLException e)
 {
     System.err.println(e);
 }
return null;
}

public void updateExternalAnnuleFAILED(String keyIdInvoice,String data, RRA_PACK invoicePack,String emp, int fact)
{  
    try
     {
   String sqlUpdate = " update APP.refund  SET ID_INVOICE="+invoicePack.id_invoice+", external_data='"+data+"'   "
        + " , TOTAL="+RRA_PRINT2.setSansVirgule(invoicePack.getTotal())+" , EMPLOYE='"+emp+"'"
        + " ,TVA="+RRA_PRINT2.setSansVirgule(invoicePack.getTotalTaxes())+"  "
        + ", TOTAL_A="+RRA_PRINT2.setSansVirgule(invoicePack.getTotalA())+", TOTAL_B="+RRA_PRINT2.setSansVirgule(invoicePack.getTotalB())+""
        + " , TOTAL_C="+RRA_PRINT2.setSansVirgule(invoicePack.getTotalD())+", TOTAL_D="+RRA_PRINT2.setSansVirgule(invoicePack.getTotalD())+""
        + " , TAXE_A="+RRA_PRINT2.setSansVirgule(invoicePack.tvaPriceA)+", TAXE_B="+RRA_PRINT2.setSansVirgule(invoicePack.tvaPriceB)+""
        + " , TAXE_C="+RRA_PRINT2.setSansVirgule(invoicePack.tvaPriceC)+", TAXE_D="+RRA_PRINT2.setSansVirgule(invoicePack.tvaPriceD)+""
        + " , MRC='"+invoicePack.MRC+"', TIN='"+invoicePack.ClientTin+"'    where  key_invoice= '"+keyIdInvoice+"' and id_refund="+fact+"";
  
        STATEMENT.execute(sqlUpdate);
        System.out.println(sqlUpdate);
        
     }
    catch(SQLException e)
    {
        insertError(e+" ","external data annule");
    }     
}

public void updateExternalInvoiceFAILED(int keyIdInvoice )
{  
    try
     {
         STATEMENT.execute("update app.invoice set comptabilise='ANNULE', RETOUR='ANNULE' where ID_INVOICE= "+keyIdInvoice+" ");
         //s.execute("delete from app.sdc_fagiture where KEY_INVOICE='"+keyIdInvoice+"'  ");
     }
     catch(SQLException e)
     {
         insertError(e+" ","external data invoice");
     }     
}
RRA_COMMAND    getLastNotOfficial(String invoice)
{ 
String toSyncro="";
try
  {  
String sql="select  * from APP.INVOICE WHERE EXTERNAL_DATA IS NULL and ID_INVOICE="+invoice; 

System.out.println(sql); 


outResult = STATEMENT.executeQuery(sql); 

   while (outResult.next())
    {
         RRA_COMMAND D=new RRA_COMMAND( "", "", outResult.getString("EMPLOYE"), "",  null);
        D.KEY_INVOICE=outResult.getString("KEY_INVOICE");
        D.ID_INVOICE =outResult.getInt(1); 
        return (D ); 
    }
    //  Close the resultSet
    outResult.close(); 
    //  Beginning of the primary catch block: uses errorPrint method
    }  catch (Throwable e)  {
   insertError(e+"","employe");
    }
JOptionPane.showMessageDialog(null, toSyncro,
        "INVOICE TO SYNC", JOptionPane.WARNING_MESSAGE);
 
return null;
}
public void updateFailedInvoice(String invoice,String EXTERNAL_DATA)
{  
    try
     {
         STATEMENT.execute("update app.invoice set  EXTERNAL_DATA='"+EXTERNAL_DATA+"',COPY_DATA='EMPTY' " 
                 + " where   ID_INVOICE="+invoice+"");
   
     
     }
     catch(SQLException e)
     {
         insertError(e+" ","update failedinvoice");
     }     
}



public void updatePUSHEDReceipt(String invoice,String table)
{  
    try
     {  
         STATEMENT.execute("update APP."+table+" set  DOCUMENT=5  where   ID_"+table+"="+invoice+""); 
     }
     catch(SQLException e)
     {
         insertError(e+" ","update failed"+table+"");
     }     
}

LinkedList <Integer> getIdInvoiceUnPushedReceipt(String table)
 { 
LinkedList <Integer> list= new LinkedList ();  
 try
 {  
     String sql = "select  * from APP."+table+" where  "
                + "  document !=5  and NUM_CLIENT IS NOT NULL"; 
     
//      String sql = "select  * from APP."+table+" where  "
//            + "  ID_INVOICE =16190  and NUM_CLIENT IS NOT NULL";

     System.out.println(sql);
    outResult = STATEMENT.executeQuery(sql); 
    while (outResult.next())
        {
        list.add(outResult.getInt("ID_"+table));
        }
        
 }
 catch(SQLException e)
 {
     System.out.println(e);
 }
 return list;
 }
}
 