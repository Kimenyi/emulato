/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ebm.sdc.rra;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.StringTokenizer;

/**
 *
 * @author Kimenyi
 */
public class RRA_PACK {
  public double totalpriceA, totalpriceB, totalpriceC, totalpriceD,
          tvaPriceA, tvaPriceB, tvaPriceC, tvaPriceD;
     double tvaRateA, tvaRateB, tvaRateC, tvaRateD;
String Mrctime,  rtype,  ttype,MRC,TIN ;
int id_invoice;
 LinkedList < RRA_PRODUCT> getProductList;
 String ClientTin="";
    public RRA_PACK( 
            double totalpriceA, double totalpriceB, double totalpriceC,double totalpriceD,
            double tvaPriceA, double tvaPriceB, double tvaPriceC, double tvaPriceD,
            double tvaRateA, double tvaRateB, double tvaRateC, double tvaRateD  )
    {
         
        this.totalpriceA = getArrondi(totalpriceA);
        this.totalpriceB = getArrondi(totalpriceB);
        this.totalpriceC = getArrondi(totalpriceC);
        this.totalpriceD = getArrondi(totalpriceD);
        this.tvaPriceA =getArrondi( tvaPriceA);
        this.tvaPriceB =getArrondi( tvaPriceB);
        this.tvaPriceC = getArrondi(tvaPriceC);
        this.tvaPriceD =getArrondi( tvaPriceD);
        this.tvaRateA = tvaRateA;
        this.tvaRateB = tvaRateB;
        this.tvaRateC = tvaRateC;
        this.tvaRateD = tvaRateD; 
    }
     public RRA_PACK(int id_invoice,
            double totalpriceA, double totalpriceB, double totalpriceC, double totalpriceD,
            double tvaPriceA, double tvaPriceB, double tvaPriceC, double tvaPriceD,
            double tvaRateA, double tvaRateB, double tvaRateC, double tvaRateD, 
             String rtype, String ttype,String MRC ,String TIN,
            LinkedList < RRA_PRODUCT> getProductList,String Mrctime)
    {
        this.id_invoice = id_invoice;
        this.totalpriceA = getArrondi(totalpriceA);
        this.totalpriceB = getArrondi(totalpriceB);
        this.totalpriceC = getArrondi(totalpriceC);
        this.totalpriceD = getArrondi(totalpriceD);
        this.tvaPriceA = getArrondi(tvaPriceA);
        this.tvaPriceB = getArrondi(tvaPriceB);
        this.tvaPriceC = getArrondi(tvaPriceC);
        this.tvaPriceD = getArrondi(tvaPriceD);
        this.tvaRateA = tvaRateA;
        this.tvaRateB = tvaRateB;
        this.tvaRateC = tvaRateC;
        this.tvaRateD = tvaRateD;
        this.Mrctime = Mrctime;
        this.rtype = rtype;
        this.ttype = ttype;
        this.MRC=MRC;
        this.TIN=TIN;
        this.getProductList=getProductList;
    }
 public double getTotalTaxes()
 {
     return tvaPriceA+tvaPriceB+tvaPriceC+tvaPriceD;
 }
 public double getTotal()
 {
     return totalpriceA+totalpriceB+totalpriceC+totalpriceD;
 }
 
  public double getTotalA()
 {
     return totalpriceA;
 }
  public double getTotalB()
 {  
     return totalpriceB;
 }
  public double getTotalC()
 {
     return totalpriceC;
 }
  public double getTotalD()
 {
     return totalpriceD;
 }
   
    public final double getArrondi(double value)
  {
//      System.out.println("avant:"+value);
      double arr=0.005;
      if(value<0)
          arr=-0.005;
      String v=setArrondi(value+arr);
//      
      try
      {
          double val=Double.parseDouble(v);
//          System.out.println("apres:"+value);
          return val;
      }
      catch(NumberFormatException e)
      {
          System.out.println("eRROR:"+e);
          return value;
      } 
  }
  
  static String setArrondi(double frw)
    {
        String setString="";
        if(frw>99999)
        {
            BigDecimal big=new BigDecimal(frw);
            int value=big.intValue();
            float dec=(float)(frw-value);
            System.out.println(dec);
            StringTokenizer st1=new StringTokenizer (""+dec);
            st1.nextToken(".");
            
            String decimal=st1.nextToken("").replace(".", "");
            
            if(decimal.length()==1 && decimal.equals("0") )
                decimal=".00";
            else if (decimal.length()==1 && !decimal.equals("0") )
                decimal="."+decimal+"0";
            else
                decimal="."+decimal.substring(0, 2);
            
            setString = ""+value;   
            return setString+decimal;
        }
        else
        {
            StringTokenizer st1=new StringTokenizer (""+frw);
            String entier =st1.nextToken(".");
            //System.out.println(frw);
            String decimal=st1.nextToken("").replace(".", "");
            
            if(decimal.length()==1 && decimal.equals("0") )    
                decimal=".00";
                    
            else if (decimal.length()==1 && !decimal.equals("0") )
                decimal="."+decimal+"0";
            else
                decimal="."+decimal.substring(0, 2);
            
            return entier+decimal; 
        }
         
}
}
