/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ebm.sdc.rra;

import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Kimenyi
 */
public class RRA_COMMAND {
    
String CMD,DATA,MSG,SENDER ;  
String KEY_INVOICE,TIME,TIN,R_TYPE,T_TYPE;
int ID_INVOICE;
Socket socket ;

 
    public RRA_COMMAND(String CMD, String DATA, String SENDER, String MSG,  Socket socket) {
        this.CMD = CMD;
        this.DATA = DATA;
        this.MSG = MSG;
        this.SENDER = SENDER; 
        this.socket=socket;
    }
    public void executeCmd()
    {
        
    } 
    public void replyClient(String answer)
    {
        PrintStream out = null;
        try {
            out = new PrintStream(socket.getOutputStream());
            out.println(answer);
        } catch (IOException ex) {
            Logger.getLogger(RRA_COMMAND.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            out.close();
        }
    }
    
}
