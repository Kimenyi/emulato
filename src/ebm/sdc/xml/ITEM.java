/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ebm.sdc.xml;

/**
 *
 * @author ishyiga
 */
public class ITEM {
   public String  CODE,NAME_PRODUCT,CODE_BAR,CODE_CLASSEMENT,TVA,ITEMS_CLASS;
   public int ID_PRODUCT;
    public ITEM(int ID_PRODUCT,String CODE, String NAME_PRODUCT, String CODE_BAR,
            String CODE_CLASSEMENT, double TVA_VALUE,String ITEMS_CLASS) {
        this.ID_PRODUCT = ID_PRODUCT;
        this.CODE = CODE;
        this.NAME_PRODUCT = NAME_PRODUCT;
        this.CODE_BAR = CODE_BAR;
        this.CODE_CLASSEMENT = CODE_CLASSEMENT;
        this.ITEMS_CLASS = ITEMS_CLASS;
        if(TVA_VALUE==0)
        { this.TVA = "A";}
        else if(TVA_VALUE==0.18)
        { this.TVA = "B";}
        else  
        { this.TVA = "C";}
    }
  
}
