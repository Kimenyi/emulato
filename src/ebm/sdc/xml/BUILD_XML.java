/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 package ebm.sdc.xml;

import ebm.sdc.rra2.RRA_RECEIVED_PACKAGE;
import ebm.sdc.rra2.RRA_SDC_SOFT;
import ebm.sdc.rra2.RraSoft;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement; 
import java.util.Date;
import javax.swing.JOptionPane;

/**
 *
 * @author CORNEILLE/ KIMENYI
 */
public class BUILD_XML {
    
      
    Statement stat;
    String connectionURL;
    Connection conn;
    int totItemNum;
   public static ITEM [] lesItems= new ITEM [20000];
    
BUILD_XML(String server, String dbName) { 
connectionURL = "jdbc:derby://" + server + ":1527/" + dbName+";user=sa;password=ish"; 
 read();
       }
private void read() {

    try {
  conn = DriverManager.getConnection(connectionURL);
  conn.setSchema("APP"); 
  stat = conn.createStatement(); 
  System.out.println(conn.getSchema()+"   Connected database successfully.. voir db ."+connectionURL);
        } catch (SQLException e) {
            System.err.println(e+" echec Read");
        } 
    }
static double round(double to_round)
{return ((int)((to_round+0.005)*100))/100.0;}
public static RECEIPT receipt(int idInvoice,Statement stat){
   RECEIPT toInsert =null;
     try{
         
           String sql="select * from APP.INVOICE    " 
                   + " WHERE ID_INVOICE = "+idInvoice; 
                System.out.println(sql);
ResultSet                outResult2 = stat.executeQuery(sql);

                while (outResult2.next()) { 
                    String sente = "RWF";//outResult2.getString("MONAIE_INVOICE");
                    
   toInsert = new RECEIPT(outResult2.getInt("ID_INVOICE"), outResult2.getString("DATE"),
outResult2.getDouble("TOTAL"), outResult2.getString("EMPLOYE"), "" + outResult2.getTimestamp("HEURE"),
outResult2.getDouble("TVA"), outResult2.getInt("DOCUMENT"), ' ',
outResult2.getString("MODE"), outResult2.getString("NUM_AFFILIATION"), "" + outResult2.getTimestamp("SERVED"),
outResult2.getString("RETOUR"), outResult2.getString("NUM_FACT"), "FACTURE", outResult2.getString("REFERENCE"),
outResult2.getString("COMPTABILISE"), "" + outResult2.getTimestamp("IMPRIME"), outResult2.getString("TARIF"),
1, sente, outResult2.getInt("REDUCTION"), "",
outResult2.getString("PROFIT_CENTER"), outResult2.getDouble("CASH"), outResult2.getDouble("CREDIT"), null);
                 
 toInsert.clientName=outResult2.getString("NUM_CLIENT");
         toInsert.clientAdress=outResult2.getString("NAME_CLIENT");
        toInsert.clientTin=outResult2.getString("TIN");
        toInsert.extData=outResult2.getString("EXTERNAL_DATA");
        toInsert.totA=outResult2.getDouble("TOTAL_A");
          toInsert.totB=outResult2.getDouble("TOTAL_B");
          toInsert.taxB=outResult2.getDouble("TAXE_B");
              toInsert.export=outResult2.getString("EXPORT");  
              toInsert.MRC=outResult2.getString("MRC");
                }
                //  Close the resultSetde
                outResult2.close();
      
         String q="select * from app.LIST WHERE ID_INVOICE = "+idInvoice+" ";
         //System.out.println(q); 
          outResult2 = stat.executeQuery(q); 
         int  itemSeq=1;
         while (outResult2.next()) {
       
        String taxTy="B";
        int taxRate=18;
        
        if(outResult2.getDouble("TVA")==0)
        { taxTy="A";
          taxRate=0;}
        
         
         double taxablSplpc=0;
         
         double totAmt=round(outResult2.getDouble("PRICE")*outResult2.getInt("QUANTITE"));
         double taxablAmt=round(totAmt/(1+outResult2.getDouble("TVA")));
         double vatAmt= round(taxablAmt*outResult2.getDouble("TVA"));
  
         String name=" ";
         String code=" ";
         
try{     
     name=lesItems[ outResult2.getInt("LIST_ID_PRODUCT")].NAME_PRODUCT;        
     code=lesItems[ outResult2.getInt("LIST_ID_PRODUCT")].CODE_CLASSEMENT; 

}catch (Throwable e)  {     
    JOptionPane.showConfirmDialog(null, outResult2.getInt("LIST_ID_PRODUCT")+" receipt "+e);
System.out.println(" ... exception throw  :"+e);
}

RECEIPT_ITEM ri= new   RECEIPT_ITEM(  itemSeq,
name, 
  taxTy, code,
        outResult2.getString("CODE_UNI"), 
round(outResult2.getDouble("PRICE")),// untpc, 
round(outResult2.getDouble("PRICE")),// splpc, 
  taxablSplpc,  vatAmt,   totAmt, 
outResult2.getInt("QUANTITE")  ,  taxRate,idInvoice,taxablAmt) ; 
toInsert.productList.add(ri);
itemSeq++;


     } 
        
toInsert.rcptSeq=itemSeq;

}catch (SQLException e)  { 
System.out.println(" ... exception throw  :"+e);
}

    return toInsert;
}

public static RECEIPT receiptRefund(int idRefund,Statement stat){
   RECEIPT toInsert =null;
     try{
         
           String sql="select * from APP.REFUND    " 
                   + " WHERE ID_REFUND = "+idRefund; 
                System.out.println(sql);
ResultSet                outResult2 = stat.executeQuery(sql);

                while (outResult2.next()) { 
                    String sente = "RWF";// outResult2.getString("MONAIE_INVOICE");
                    
   toInsert = new RECEIPT(outResult2.getInt("ID_REFUND"), outResult2.getString("DATE"),
outResult2.getDouble("TOTAL"), outResult2.getString("EMPLOYE"), "" + outResult2.getTimestamp("HEURE"),
outResult2.getDouble("TVA"), outResult2.getInt("DOCUMENT"), ' ',
"", outResult2.getString("NUM_AFFILIATION"), ""  ,
"", outResult2.getString("NUM_FACT"), "FACTURE", outResult2.getString("RAISON"),
outResult2.getString("COMPTABILISE"), ""  , "",
1, sente, outResult2.getInt("ID_REFUND"), "",
"", outResult2.getDouble("CASH"), outResult2.getDouble("CREDIT"), null);
                 
 toInsert.clientName=outResult2.getString("NUM_CLIENT");
         toInsert.clientAdress=outResult2.getString("NAME_CLIENT");
        toInsert.clientTin=outResult2.getString("TIN");
        toInsert.extData=outResult2.getString("EXTERNAL_DATA");
        toInsert.totA=outResult2.getDouble("TOTAL_A");
          toInsert.totB=outResult2.getDouble("TOTAL_B");
              toInsert.export=outResult2.getString("EXPORT");  
              toInsert.MRC=outResult2.getString("MRC");
                }
                //  Close the resultSet
                outResult2.close();
      
         String q="select * from app.REFUND_LIST WHERE ID_REFUND = "+idRefund+" ";
          System.out.println(q); 
          outResult2 = stat.executeQuery(q); 
         int  itemSeq=1;
         while (outResult2.next()) {
       
        String taxTy="B";
        int taxRate=18;
        String classement="0";
        if(outResult2.getDouble("TVA")==0)
        { taxTy="A";
          taxRate=0;}
        
        if(lesItems[ outResult2.getInt("LIST_ID_PRODUCT")]!=null)
        { classement=lesItems[ outResult2.getInt("LIST_ID_PRODUCT")].CODE_CLASSEMENT;}
         double taxablSplpc=0;
         double vatAmt=0;
         double totAmt=0;
RECEIPT_ITEM ri= new   RECEIPT_ITEM(  itemSeq,
      outResult2.getString("NAME_PRODUCT")   , 
  taxTy, 
      classement,
        outResult2.getString("CODE_UNI"), 
outResult2.getDouble("PRICE"),// untpc, 
outResult2.getDouble("PRICE"),// splpc, 
  taxablSplpc,  vatAmt,   totAmt, 
outResult2.getInt("QUANTITE")  ,  taxRate,idRefund,0) ; 
toInsert.productList.add(ri);
itemSeq++;
     } 
        
toInsert.rcptSeq=itemSeq;

}catch (SQLException e)  { 
System.out.println(" ... exception throw  :"+e);
}

    return toInsert;
}

public static  String doXmlReceipt(RECEIPT inv,String refId,String rcptTyCd,String reference ){
    String invoice="";
    String ln = "";
     //0 SDC001000638,
    //1 465,
    //2 743,
    //3 NS,
    //4 17/12/2013 18:02:39,
    //5 AVR6PYZDNGFEICTD,
    //6 2NPRWVP2X7YCR2GQ5NOA2G7W7A
    if(inv== null)
    {
      System.out.println(" ... inv  is null :" );  
    }
  else
    {System.out.println(" ... inv.extData  :"+inv.extData);
      String[] sp = inv.extData.split(",");
      
      
      String sdcId= sp[0];  
      String rcptSeq= sp[1] ;
      String totRcptSeq= sp[2]  ;
      String rcptSign= sp[5];
      String internalData= sp[6];
      
      if(internalData!=null && internalData.length()>26)
      {internalData=internalData.substring(0, 26);}
      
      System.out.println(sdcId+"  sdcId  :"+sdcId.length());
      
        String invoiceDate=makeDate(inv.HEURE);
                invoice =  
                          "<row>" + ln
                        + "<table>TRNRECEIPT</table>" + ln
                        + "<actionCd>ACT</actionCd>" + ln      
                        + "<invId>"+inv.ID_INVOICE+"</invId>" + ln 
                        + "<bhfId>00</bhfId>" + ln
                        + "<tin>"+RRA_SDC_SOFT.TIN+"</tin>" + ln
                        + "<bcncId>"+inv.clientTin+"</bcncId> " + ln  // CUSTOMER TIN
                        + "<bcncNm>"+inv.clientName+"</bcncNm> " + ln  // CUSTOMER TIN
                        + "<refId>"+refId+"</refId> " + ln  // ORIGINAL INVOICE
                        + "<validDt>"+invoiceDate+"</validDt> " + ln  // CUSTOMER TIN
                        + "<taxRateA>0</taxRateA>" + ln
                        + "<totTaxablAmtA>"+inv.totA+"</totTaxablAmtA>" + ln
                        + "<totTaxA>0</totTaxA>" + ln
                        + "<taxRateB>18</taxRateB>" + ln
                        + "<totTaxablAmtB>"+inv.totB+"</totTaxablAmtB>" + ln
                        + "<totTaxB>"+inv.taxB+"</totTaxB>" + ln
                        + "<taxRateC>0</taxRateC>" + ln
                        + "<totTaxablAmtC>0</totTaxablAmtC>" + ln
                        + "<totTaxC>0</totTaxC>" + ln
                        + "<taxRateD>0</taxRateD>" + ln
                        + "<totTaxablAmtD>0</totTaxablAmtD>" + ln
                        + "<totTaxD>0</totTaxD>" + ln
                        + "<transTyCd>N</transTyCd>" + ln
                        + "<rcptTyCd>"+rcptTyCd+"</rcptTyCd>" + ln  
                        + "<sdcId>"+
                        sdcId.substring((sdcId.length() - 12 ) , 
                        (sdcId.length())) +"</sdcId>" + ln
                        + "<mrcNo>"+inv.MRC+"</mrcNo>" + ln
                        + "<rcptDt>"+makeDateRcpt(inv.HEURE)+"</rcptDt>" + ln
                        + "<sdcRcptNo>"+rcptSeq+"</sdcRcptNo>" + ln
                        + "<totSdcRcptNo>"+totRcptSeq+"</totSdcRcptNo>" + ln 
                        + "<totItemNum>"+inv.rcptSeq+"</totItemNum>" + ln
                        + "<internalData>"+internalData+"</internalData>" + ln
                        + "<signature>"+rcptSign+"</signature>" + ln 
                        + "<topMsg></topMsg>" + ln
                        + "<bottomMsg></bottomMsg>" + ln
                        + "<totTax>"+inv.taxB+"</totTax>" + ln
                        + "<totAmt>"+inv.TOTAL4+"</totAmt>" + ln
                        + "<payTyCd>"+inv.paymentMode()+"</payTyCd>" + ln
                        + "<regusrId>"+inv.EMPLOYE+"</regusrId>  " + ln /// CIS USER LOGIN
                         + "<regusrNm>"+reference.substring(0, Math.min(59, reference.length()))+"</regusrNm>  " + ln /// put refun reason
                         + "<rptNo>XXX</rptNo>  " + ln /// CIS USER LOGIN
                         + "<regDt>"+invoiceDate+"</regDt>  " + ln /// CIS USER LOGIN
                        + "<journal>"+inv.export+"</journal> " + ln //EJ WITHOUT B AND E
                        + "</row>"  ;
       System.out.println("XML size::"+invoice.length()); 
        System.out.println(invoice);
    }
     return invoice;
}
public static  String doXmlReceiptItem(RECEIPT inv){
    String items="";
    String ln = ""; 
      if(inv== null)
    {
      System.out.println(" ... inv list is null :" );  
    }
  else   
    for(int i=0;i< inv.productList.size();i++)
    {     
        RECEIPT_ITEM ri = inv.productList.get(i);
        items +=  
                          "<row>" + ln
  
                        + "<table>TRNRECEIPTITEM</table>" + ln
                        + "<actionCd>ACT</actionCd>" + ln      
                        + "<invId>"+inv.ID_INVOICE+"</invId>" + ln 
                        + "<bhfId>00</bhfId>" + ln 
                        + "<itemSeq>"+ri.itemSeq+"</itemSeq>" + ln
                        + "<itemNm>"+ri.itemNm+"</itemNm> " + ln  // CUSTOMER TIN
                        + "<untpc>"+ri.untpc+"</untpc>" + ln
                        + "<qty>"+ri.qty+"</qty>" + ln
                        + "<splpc>"+ri.splpc+"</splpc>" + ln
                        + "<dcRate>0</dcRate>" + ln
                        + "<dcAmt>0</dcAmt>" + ln
                + "<bcncId>"+inv.clientTin+"</bcncId>" + ln
                        + "<pkgQty>0</pkgQty>" + ln
                + "<pkgUnitCd>0</pkgUnitCd>" + ln
                        + "<qtyUnitCd>U</qtyUnitCd>" + ln
                        + "<taxablSplpc>"+ri.taxablSplpc+"</taxablSplpc>" + ln
                        + "<taxTyCd>"+ri.taxTy+"</taxTyCd>" + ln
                        + "<tax>"+ri.vatAmt+"</tax>" + ln
                + "<taxablAmt>"+ri.taxablAmt+"</taxablAmt>" + ln
                        + "<totAmt>"+ri.totAmt+"</totAmt>" + ln
                        + "<itemClsCd>"+ri.itemClsCd+"</itemClsCd>" + ln
                        + "<itemCd>"+ri.itemCd+"</itemCd>" + ln 
                        + "</row>"  ; 
         
        
    }
   // System.out.println(items);
     return items;
}


public static String getDateTime() {
           Date d = new Date();

        String day, mm, year;

        if (d.getDate() < 10) {
            day = "0" + d.getDate();
        } else {
            day = "" + d.getDate();
        }
        if ((d.getMonth() + 1) < 10) {
            mm = "0" + (d.getMonth() + 1);
        } else {
            mm = "" + (d.getMonth() + 1);
        }
        year=""+((d.getYear() - 100) + 2000);
//        if ((d.getYear() - 100) < 10) {
//            year = "0" + (d.getYear() - 100);
//        } else {
//            year = "" + (d.getYear() - 100);
//        }

        return year + mm + day+d.getHours()+d.getMinutes()+d.getSeconds(); 
    }
 public static String makeDate(String heure) {
      
        String day, mm, year,h,m,s;
//2017-01-01 00:00:00
//0123456789012345678
        
        day = heure.substring(8, 10); 
        mm = heure.substring(5, 7);  
        year=heure.substring(0, 4); 
        
        h = heure.substring(11, 13); 
        m = heure.substring(14, 16);  
        s = heure.substring(17, 19);

        return year + mm + day+h+m+s; 
    }
 
  public static String makeDateRcpt(String heure) {
      
        String day, mm, year,h,m,s;
//2017-01-01 00:00:00
//0123456789012345678
        
        day = heure.substring(8, 10); 
        mm = heure.substring(5, 7);  
        year=heure.substring(0, 4); 
        
        h = heure.substring(11, 13); 
        m = heure.substring(14, 16);  
        s = heure.substring(17, 19);

        return day  + mm +year +h+m+s; 
    }
 
  public static boolean buildStock(Statement stat, String user,int port){
       String lotLine="";
       String code;
       String qtyLive;
        try{
String sql="select CODE_LOT,sum(QTY_LIVE) as total from APP.LOT,APP.PRODUCT WHERE app.lot.code_lot=app.product.code  group by CODE_LOT";
System.out.println(sql); 
ResultSet outResult = stat.executeQuery(sql); 
int i=0;
            while (outResult.next()) { 
                code=outResult.getString(1);
                int qte=outResult.getInt(2);
                qtyLive=""+qte;
                if(qte>0)
                {lotLine=code+";"+qtyLive+"#";  
                RRA_RECEIVED_PACKAGE recItem=RRA_SDC_SOFT.sendToSDCSOFT
        ("STOCK",i+">>"+doXmlStock(lotLine) ,user,port,RraSoft.softIP );
                
                if(recItem!=null && recItem.data!=null && !recItem.data.equals("true"))
                {
                    JOptionPane.showMessageDialog(null, " DUMPING STOCK FAIL \n LINE "+lotLine);
                     
                    return false;
                }
                i++;}
                
              }
            //System.out.println(lotLine);
            outResult.close();
            
        }catch (SQLException e)  { 
    System.out.println(" ... exception  :"+e);
    } 
      return true;   
    }
  
  public static void buildItem(Statement stat){
       
        try{
String sql="SELECT ID_PRODUCT,CODE,NAME_PRODUCT,CODE_BAR,CODE_CLASSEMENT,TVA,ITEMS_CLASS"
        + " FROM APP.PRODUCT ORDER BY  ID_PRODUCT";
System.out.println(sql); 
ResultSet outResult = stat.executeQuery(sql); 

            while (outResult.next()) { 
   int ID_PRODUCT=outResult.getInt(1);             
   String CODE=outResult.getString(2);
   String NAME_PRODUCT=outResult.getString(3);
   String CODE_BAR=outResult.getString(4);
   String CODE_CLASSEMENT=outResult.getString(5); 
   double TVA_VALUE=outResult.getDouble(6);
   String ITEMS_CLASS=outResult.getString(7);
   
   lesItems[ID_PRODUCT] =new ITEM(ID_PRODUCT,  CODE,   NAME_PRODUCT,   CODE_BAR,
              CODE_CLASSEMENT,   TVA_VALUE,ITEMS_CLASS);  
              }
            //System.out.println(lotLine);
            outResult.close();
            
        }catch (SQLException e)  { 
    System.out.println(" ... exception  :"+e);
    }  
    }
  
  
  public static String doXmlStock(String lot){
      String  row="";
      //ZYRTE01;63#
      String [] t = lot.split("#");
      String [][] line = new String [t.length][2];
      for(int i=0;i<line.length;i++)
       {
       line [i] = t[i].split(";");
       }
      String ln = "";

        for (String[] line1 : line) {
            row += "<row>" +ln
                    + "<table>STCINVENTORY</table>" +ln
                    + "<actionCd>ACT</actionCd>" +ln
                      + "<tin>"+RRA_SDC_SOFT.TIN+"</tin>" + ln
                    + "<bhfId>00</bhfId>" +ln // SDC INFO...
                    + "<itemClsCd>5020220101</itemClsCd>" +ln
                    + "<itemCd>" + line1[0] + "</itemCd>" + ln + "<qty>" + line1[1] + "</qty>" + ln + "<updDt>" + getDateTime() + "</updDt>" + ln + "</row>" + ln;
        }
     
       System.out.println("xml::"+row);
      return row;
  }
     
   public static String doXmlItem(ITEM lesItem){
    String ln = "";
    
    String  row  = " <row>" +ln
+ "<itemClsCd>"+lesItem.CODE_CLASSEMENT+"</itemClsCd>" +ln
+ "<itemCd>"+lesItem.CODE+"</itemCd>" +ln
+ "<itemNm>"+lesItem.NAME_PRODUCT+"</itemNm>" +ln
+ "<itemTyCd>"+lesItem.ITEMS_CLASS+"</itemTyCd> " +ln    //raw material 1; produit fini 2; service 3
+ "<itemStd></itemStd>" +ln
+ "<orgplceCd>RW</orgplceCd>" +ln // code zibihugu 
+ "<pkgUnitCd>CT</pkgUnitCd> " +ln//db to be given by danny (shared table)
+ "<qtyUnitCd>KG</qtyUnitCd>" +ln //kg...
+ "<taxTyCd>"+lesItem.TVA+"</taxTyCd> " +ln   ///	ABCD ARIKO RRA HAS TO VALIDATE 
+ "<useYn>Y</useYn>" +ln // YES/NO WAVUYE KWISOKO
+ "<regusrId>admin</regusrId>" +ln
+ "<regDt>" + getDateTime() + "</regDt>" +ln
+ "<useBarcode>N</useBarcode>" +ln
+ "<changeYn>N</changeYn> " +ln// IYO ITEM IHINDUSTE
+ "</row>";
 
     
       System.out.println("xml::"+row);
      return row;
  }
  
  
    public static void main(String[] arghs) {
 
       BUILD_XML m = new BUILD_XML("localhost", "HOTELgalaxy"); 
     //  m.receipt(98947);
       
    }
}
