/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ebm.sdc.xml;

/**
 *
 * @author ishyiga
 */
public class RECEIPT_ITEM {
String itemNm,taxTy,itemClsCd,itemCd;
double untpc,splpc, taxablSplpc, vatAmt,totAmt,taxablAmt;
int qty,dcRate,idInvoice,itemSeq; 

    public RECEIPT_ITEM(int itemSeq, String itemNm, String taxTy, String itemClsCd,
            String itemCd, double untpc, double splpc, double taxablSplpc, double vatAmt,
            double totAmt, int qty, int dcRate, int idInvoice,double taxablAmt) {
        this.taxablAmt=taxablAmt;
        this.itemSeq = itemSeq;
        this.itemNm = itemNm;
        this.taxTy = taxTy;
        this.itemClsCd = itemClsCd;
        this.itemCd = itemCd;
        this.untpc = untpc;
        this.splpc = splpc;
        this.taxablSplpc = taxablSplpc;
        this.vatAmt = vatAmt;
        this.totAmt = totAmt;
        this.qty = qty;
        this.dcRate = dcRate;
        this.idInvoice = idInvoice;
    }

   
    

}
